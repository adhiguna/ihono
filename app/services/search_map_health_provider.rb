# frozen_string_literal: true

class SearchMapHealthProvider < Struct.new(:category, :distance, :latitude, :longitude)
  include ActiveModel::Model

  class << self
    def build_search(hash)
      obj = self.new
      hash.each { |key,value| obj.send("#{key}=", value) }
      obj
    end

    def parse(hash)
      pre_data = { category: nil, distance: nil, latitude: nil, longitude: nil }

      pre_data[:category] = hash[:category] if category_options.first[1].to_s != hash[:category].to_s
      pre_data[:distance] = hash[:distance] if distance_options.last[1].to_i != hash[:distance].to_i
      pre_data[:latitude] = hash[:latitude] if hash[:latitude].present?
      pre_data[:longitude] = hash[:longitude] if hash[:longitude].present?

      pre_data
    end

    def category_options
      [['All Medical Health Providers', 0]] + HealthProvider::SERVICE_LIST.map{ |data| [data, data] }
    end

    def distance_options
      [['Within 1km', 1], ['Within 10km', 10], ['Within 50km', 50], ['50km or more', 51]]
    end

    def extract_data(type = 'marker', params)
      raise "no #{type} for extract data" unless ['marker', 'list'].include?(type)
      
      latitude = params[:latitude]
      longitude = params[:longitude]
      distance = params[:distance]
      category = params[:category]

      results = send("extract_data_#{type}_info", HealthProvider.extract_data_and_filters(latitude, longitude, category, distance))
   
      results
    end

    def extract_data_marker_info(data)
      data = data.map do |f| 
        obj = HealthProviderPresenter.new(f)

        { 
          type: obj.info_map_type, 
          type_name: obj.info_map_type_name, 
          icon_symbol: obj.info_map_icon_symbol, 
          link: obj.info_map_link_generate, 
          name: obj.name, 
          latitude: obj.latitude, 
          longitude: obj.longitude, 
          short_abstract: obj.short_abstract 
        }
      end

      data.flatten
    end

    def extract_data_list_info(data)
      data = data.map do |f| 
        obj = HealthProviderPresenter.new(f) 

        { 
          region: obj.region, 
          name: obj.name, 
          short_abstract: obj.short_abstract,
          link_to_map: obj.link_to_map
        }
      end

      data.flatten.group_by{ |f| f[:region] }
    end

    # default value for filters
    def init_filters
      { category: category_options.first[1], distance: distance_options.last[1] }
    end
  end
end
