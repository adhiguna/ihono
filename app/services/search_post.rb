# frozen_string_literal: true

class SearchPost < Struct.new(:category, :region)
  include ActiveModel::Model
  
  class << self
    def build_search(hash)
      obj = self.new
      hash.each { |key,value| obj.send("#{key}=", value) }
      obj
    end

    def category_filter
      ['All'] + Post::TYPE_LIST['covid-19']
    end

    def region_filter
      [['All', 0]] + Region.pluck(:name, :id)
    end

    def parse(hash)
      pre_data = { category: nil, region: nil }

      pre_data[:category] = hash[:category] if category_filter.first != hash[:category]
      pre_data[:region] = hash[:region] if region_filter.first.last != hash[:region].to_i
      
      pre_data
    end

    # default value for filters
    def init_filters
      { category: category_filter.first, region: region_filter.first.last }
    end
  end
end
