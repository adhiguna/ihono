# frozen_string_literal: true

class SearchMap < Struct.new(:category, :distance, :latitude, :longitude) 
  include ActiveModel::Model

  class << self
    def build_search(hash)
      obj = self.new
      hash.each { |key,value| obj.send("#{key}=", value) }
      obj
    end
    
    def category_options(user_present: true)
      # only allowed search user when user are present
      if user_present
        ['People', 'Marae'] + Organisation::TYPE_LIST
      else
        ['Marae'] + Organisation::TYPE_LIST
      end
    end

    def distance_options
      [['Within 1km', 1], ['Within 10km', 10], ['Within 50km', 50], ['50km or more', 51]]
    end

    def data_maps(params = {})
      results = []
      category = params[:category] ? params[:category] : category_options
      latitude = params[:latitude]
      longitude = params[:longitude]
      distance = params[:distance]

      results += extract_data_marker_info(
        Marae.extract_data(latitude, longitude, distance), 'marae'
      ) if category.include?('Marae')

      results += extract_data_marker_info(
        User.extract_data(latitude, longitude, distance), 'user'
      ) if category.include?('People')

      category_organisation = get_category_organisation(category)

      results += extract_data_marker_info(
        Organisation.where(organisation_type: category_organisation).extract_data(latitude, longitude, distance),
        'organisation'
      ) if category_organisation.present?

      results
    end

    def extract_data_marker_info(data, class_name)
      locations = if class_name == 'organisation'
                    data.uniq.map do |org|
                      obj = class_presenter(org)
                      org.locations.latitude_longitude_present.map do |location|
                        { type: obj.info_map_type, type_name: obj.info_map_type_name, icon_symbol: obj.info_map_icon_symbol, link: obj.info_map_link_generate, name: obj.name, latitude: location.latitude, longitude: location.longitude, short_abstract: obj.short_abstract }
                      end
                    end
                  else
                    data.map do |f| 
                      obj = class_presenter(f)
                      { type: obj.info_map_type, type_name: obj.info_map_type_name, icon_symbol: obj.info_map_icon_symbol, link: obj.info_map_link_generate, name: obj.name, latitude: obj.latitude, longitude: obj.longitude, short_abstract: obj.short_abstract }
                    end
                  end

      locations.flatten
    end

    def class_presenter(data)
      "#{data.class.name}Presenter".constantize.new(data)
    end

    # removing other category except for organisation
    def get_category_organisation(category)
      category.delete('Marae')
      category.delete('User')

      category
    end

    # default value for filters
    def init_filters
      { category: category_options, distance: distance_options.last[1] }
    end
  end
end
