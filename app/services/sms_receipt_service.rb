class SmsReceiptService < ApplicationService
  attr_accessor :sms_receipt, :params

  def initialize(params)
    @sms_receipt = SmsReceipt.find_or_initialize_by(message_id: params['messageId'])
    @params = params
  end

  def call
    return if @sms_receipt.blank?
    
    transform_params
    sms_receipt.assign_attributes(params)
    sms_receipt.save
  end

  private

  def transform_params
    params.delete 'messageId'
    params[:network_code] = params.delete 'network-code' if params['network-code']
    params[:error_code] = params.delete 'err-code' if params['err-code']
    params[:api_key] = params.delete 'api-key' if params['api-key']
    params[:message_timestamp] = params.delete 'message-timestamp' if params['message-timestamp']
  end
end