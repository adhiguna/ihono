class UserInvitationSignupService < ApplicationService
  include Rails.application.routes.url_helpers 
  attr_accessor :user_invitation_signup, :short_url

  def initialize(user_invitation_signup)
    @user_invitation_signup = user_invitation_signup.is_a?(UserInvitationSignup) ? user_invitation_signup : UserInvitationSignup.find(user_invitation_signup)
  end

  def call
    send_email
    send_sms
  end

  def send_email
    UserInvitationSignupMailer.notification(user_invitation_signup).deliver_now if user_invitation_signup.email.present?
  end

  def send_sms
    SmsWorker.perform_async(user_invitation_signup.phone, sms_message) if user_invitation_signup.phone.present?
  end

  def owner_invited_name
    user_invitation_signup.owner_invited.full_name
  end

  def sms_message
    "#{owner_invited_name} just invited you to join īHono : #{short_url}" 
  end

  def short_url
    @short_url ||= ShortUrl.call(get_url)
  end

  def get_url
    new_registration_url(user_invitation_signup_token: user_invitation_signup.token, host: ENV['DOMAIN_NAME'])
  end
end