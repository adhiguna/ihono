# frozen_string_literal: true

class SearchGroup < Struct.new(:category, :distance, :latitude, :longitude)
  include ActiveModel::Model
  LIST_URL_GROUPS = {
    'people' => 'People',
    'user' => 'User',
    'post' => 'Post',
    'marae' => 'Marae',
    'iwi-hapu' => 'Iwi / Hapū',
    'iwi-organisation-or-trust' => 'Iwi Organisation or Trust',
    'maori-business' => 'Maori Business',
    'community-organisation' => 'Community Organisation',
    'education-organisation' => 'Education Organisation',
    'health-organisation' => 'Health Organisation',
    'central-or-local-government' => 'Central or Local Government'
  }

  class << self
    def data_count_groups(params: {}, user_present: true)
      results = []
      keyword = params[:keyword]

      if keyword.present?
        results = PgSearch.multisearch(keyword).where.not(searchable_type: ['Wharenui', 'Rohe', 'Region'])
        results = results.where.not(searchable_type: 'User') unless user_present
        results = grouping_category(results)
        results = count_icon_and_link_group_category(results, keyword)
      end

      results
    end

    ##
    # list of grouping
    # 'Post', 'Marae', 'Iwi / Hapū', 'Iwi Organisations or Trust', 'Maori Business', 
    # 'Community Organisation', 'Education Organisation', 'Health Organisation'
    def grouping_category(results)
      results.group_by do |result|
        case result.searchable_type
        when 'User'
          'People'
        when 'Iwi'
          'Iwi / Hapū'
        when 'Hapu'
          'Iwi / Hapū'
        when 'Organisation'
          result.searchable.organisation_type
        else
          result.searchable_type
        end
      end
    end

    def count_icon_and_link_group_category(results, keyword)
      results.map do |group, data|
        { group_type: group, size: data.size, icon: get_icon(group), link: get_group_link(group, keyword) }
      end
    end

    def get_group_link(group, keyword)
      base_route.view_results_search_index_path(group.parameterize, keyword, redirect_back_to: base_route.root_path(anchor: 'search-section') )
    end

    def base_route
      Rails.application.routes.url_helpers
    end

    def get_icon(data)
      case data
      when 'People'
        'ihono-icon-people'
      when 'User'
        'ihono-icon-people'
      when 'Post'
        'ihono-icon-post'
      when 'Iwi / Hapū'
        'ihono-icon-iwi-hapu'
      when 'Marae'
        'ihono-icon-marae'
      when 'Iwi Organisation or Trust'
        'ihono-icon-iwi-organisation-or-trust'
      when 'Māori Business'
        'ihono-icon-māori-business'
      when 'Community Organisation'
        'ihono-icon-community-organisation'
      when 'Education Organisation'
        'ihono-icon-education-organisation'
      when 'Health Organisation'
        'ihono-icon-health-organisation'
      when 'Central or Local Government'
        'ihono-icon-central-or-local-government'
      end
    end

    def data_view_results(params: {}, user_present: true)
      return {} if params.blank?
      return {} if params[:keyword].blank?
      return {} if params[:group].blank?
      return {} if !SearchGroup::LIST_URL_GROUPS.keys.include?(params[:group])
      return {} if !user_present and params[:group] == 'maori-business'

      results = []
      keyword = params[:keyword]
      group = params[:group]

      results = PgSearch.multisearch(keyword).reorder('')
      results = filter_data_view_results(results, group)

      { group: SearchGroup::LIST_URL_GROUPS[params[:group]], icon_group: get_icon(SearchGroup::LIST_URL_GROUPS[group]), keyword: keyword, results: results }
    end

    def filter_data_view_results(results, group)
      results =   case group
                  when 'people'
                    results.where(searchable_type: 'User')
                  when 'user'
                    results.where(searchable_type: 'User')
                  when 'post'
                    results.where(searchable_type: 'Post')
                  when 'marae'
                    results.where(searchable_type: 'Marae')
                  when 'iwi-hapu'
                    results.where("searchable_type = 'Iwi' OR searchable_type = 'Hapu'")
                  when 'iwi-organisation-or-trust'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Iwi Organisation or Trust'")
                  when 'community-organisation'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Community Organisation'")
                  when 'education-organisation'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Education Organisation'")
                  when 'health-organisation'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Health Organisation'")
                  when 'maori-business'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Māori Business'")
                  when 'central-or-local-government'
                    results.where(searchable_type: 'Organisation').joins("INNER JOIN organisations ON organisations.id = searchable_id AND organisations.organisation_type = 'Central or Local Government'")
                  end
      
      results.order('content').map do |result|
        { name: get_name(result), link: get_link(result) }
      end
    end

    def get_name(data)
      case data.searchable_type
      when 'Post'
        data.searchable.title
      else
        data.searchable.name
      end
    end

    def get_link(data)
      front_slug =  case data.searchable_type
                    when 'User'
                      'users'
                    when 'Organisation'
                      'organisations'
                    when 'Post'
                      'posts'
                    else
                      data.searchable_type.downcase
                    end

      "/#{front_slug}/#{data.searchable.slug}"
    end

    # default value for filters
    def init_filters 
      { keyword: '' }
    end
  end
end
