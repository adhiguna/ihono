# frozen_string_literal: true

class SearchAppointment < Struct.new(:health_provider_id, :name, :appointment_date, :service_worker, :contacted, :symptoms, :pre_exist_health_condition, :age, :contacts, :sort)
  include ActiveModel::Model
  
  class << self
    def build_search(hash)
      obj = self.new
      hash.each { |key,value| obj.send("#{key}=", value) }
      obj
    end

    def sort_filter
      ['booking_date desc', 'booking_date asc']
    end

    def region_filter
      [['All', 0]] + Region.pluck(:name, :id)
    end

    def service_worker_filter
      [
        ['Yes', true], 
        ['No', false], 
      ]
    end

    def contact_covid19_filter
      [
        ['Yes', true], 
        ['No', false], 
      ]
    end

    def symptoms_filter
      FEELS_LIST
    end

    def pre_exist_health_condition_filter
      HEALTH_CONDITION_LIST
    end

    def age_filter
      AGE_LIST
    end

    def contact_filter
      [
        ['home phone', 'home_phone'], 
        ['email', 'email'],
        ['cell phone', 'cell_phone']
      ]
    end

    def health_provider_filter
      HealthProvider.all.pluck(:name, :id)
    end

    def parse(hash)
      pre_data = { health_provider_id: nil, name: nil, service_worker: nil, contacted: nil, appointment_date: nil, symptoms: [], pre_exist_health_condition: [], age: [], contact_details: nil, sort: nil }

      pre_data[:health_provider_id] = hash[:health_provider_id]
      pre_data[:keyword] = hash[:keyword]
      pre_data[:name] = hash[:name]
      pre_data[:service_worker] = hash[:service_worker] if hash[:service_worker].present? && (hash[:service_worker] == 'true' || hash[:service_worker] == true)
      pre_data[:contacted] = hash[:contacted] if hash[:contacted].present? && (hash[:contacted] == 'true' || hash[:contacted] == true)
      pre_data[:symptoms] = hash[:symptoms].reject(&:blank?) if hash[:symptoms]&.reject(&:blank?)&.present?
      pre_data[:pre_exist_health_condition] = hash[:pre_exist_health_condition].reject(&:blank?) if hash[:pre_exist_health_condition]&.reject(&:blank?)&.present?
      pre_data[:age] = hash[:age].reject(&:blank?) if hash[:age]&.reject(&:blank?)&.present?
      pre_data[:contacts] = hash[:contacts].reject(&:blank?) if hash[:contacts]&.reject(&:blank?)&.present?
      pre_data[:appointment_date] = hash[:appointment_date]
      pre_data[:sort] = hash[:sort]

      { params: pre_data }
    end

    def open_toggle?(hash)
      return false unless hash[:search_appointment].present?
      
      hash = hash[:search_appointment]

      result =  hash[:service_worker]&.reject(&:blank?)&.present? ||
                hash[:contacted]&.reject(&:blank?)&.present? ||
                hash[:symptoms]&.reject(&:blank?)&.present? ||
                hash[:pre_exist_health_condition]&.reject(&:blank?)&.present? ||
                hash[:age]&.reject(&:blank?)&.present? ||
                hash[:contacts]&.reject(&:blank?)&.present? ||
                hash[:appointment_date]&.reject(&:blank?)&.present?
      
      result
    end

    # default value for filters
    def init_filters
      { sort: sort_filter.first }
    end

    def sort_active(hash, sort_type = 'booking_date')
      return false unless hash[:search_appointment].present?
      
      hash = hash[:search_appointment]

      if hash[:sort].present?
        'active' if hash[:sort].split(' ').first == sort_type
      else
        'active' if sort_type == sort_filter.first.split(' ').first
      end
    end

    def sort_position(hash, sort = 'desc')
      return sort unless hash[:search_appointment].present?

      hash = hash[:search_appointment]

      if hash[:sort].present?
        hash[:sort].split(' ').last 
      else
        sort
      end
    end
  end
end
