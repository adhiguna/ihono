class CreateAppointmentNotificationService < ApplicationService
  include Rails.application.routes.url_helpers 
  attr_accessor :appointment, :health_provider, :short_url_health_admin, :short_url_patient

  def initialize(appointment)
    @appointment = appointment.is_a?(Appointment) ? appointment : Appointment.find(appointment)
    @health_provider = @appointment.health_provider
  end

  def call
    call_health_admin_notification
    call_patient_notification
  end

  def call_health_admin_notification
    send_health_admin_email
    send_sms_health_admin
  end

  def call_patient_notification
    send_email_patient
    send_sms_patient
  end

  def send_health_admin_email
    user_health_admin_emails.each do |email|
      AppointmentMailer.create_health_admin_notification(appointment, email).deliver_now
    end
  end

  def send_email_patient
    patient_emails.each do |email|
      AppointmentMailer.create_patient_notification(appointment, email).deliver_now
    end
  end

  def send_sms_health_admin
    user_health_admin_phones.each do |phone|
      SmsWorker.perform_async(phone, appointment_sms_message('health_admin'))
    end
  end

  def send_sms_patient
    patient_phones.each do |phone|
      SmsWorker.perform_async(phone, appointment_sms_message('patient'))
    end
  end

  def appointment_sms_message(type = 'health_admin')
    return unless ['health_admin', 'patient'].include?(type)

    if type == 'health_admin'
      "New appointment for #{health_provider.name} : #{short_url_health_admin}"
    elsif type == 'patient'
      "Your appointment at #{health_provider.name} : #{short_url_patient}" 
    end
  end

  def short_url_health_admin
    @short_url_health_admin ||= ShortUrl.call(get_health_admin_appointment_url)
  end

  def get_health_admin_appointment_url
    health_admin_appointment_url(@appointment, host: ENV['DOMAIN_NAME'])
  end

  def short_url_patient
    @short_url_patient ||= ShortUrl.call(get_patient_appointment_url)
  end

  def get_patient_appointment_url
    corona_appointment_url(@appointment, host: ENV['DOMAIN_NAME'])
  end

  private

  def patient_emails
    [appointment.user&.email, appointment.email].reject(&:blank?).uniq
  end

  def patient_phones
    [appointment.user&.phone, appointment.cell_phone].reject(&:blank?).uniq
  end

  def user_health_admins
    User.health_admin
  end

  def user_health_admin_emails
    user_health_admins.pluck(:email).reject(&:blank?)
  end

  def user_health_admin_phones
    user_health_admins.pluck(:phone).reject(&:blank?)
  end
end