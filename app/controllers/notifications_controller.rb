class NotificationsController < ApplicationController
  before_action :authenticate_user!

  def index
    @notifications = current_user.receiver_notifications.undeleted.joins(notification: :sender)
    current_user.read_all_notification
  end
end
