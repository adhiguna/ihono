class WharenuisController < ApplicationController
  before_action :set_wharenui

  def show; end

  private

  def set_wharenui
    @wharenui = Wharenui.friendly.find(params[:id])
  end
end
