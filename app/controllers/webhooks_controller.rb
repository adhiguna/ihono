class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def sms_receipt
    SmsReceiptWorker.perform_async(sms_receipt_worker_params)
  end

  private

  def sms_receipt_worker_params
    params.permit(
      'msisdn', 'to', 'network-code', 'messageId',
      'price', 'status', 'scts', 'err-code', 'api-key', 'message-timestamp'
    )
  end
end
