class IwisController < ApplicationController
  before_action :set_iwi, except: [:get_hapu]

  def show; end

  def get_hapu
    iwi = Iwi.find(params[:id])
    respond_to do |format|
      format.json { render json: iwi.hapus.pluck(:name, :id).to_json }
    end
  end

  private

  def set_iwi
    @iwi = Iwi.friendly.find(params[:id])
  end
end
