class RohesController < ApplicationController
  before_action :set_rohe

  def show; end

  private

  def set_rohe
    @rohe = Rohe.friendly.find(params[:id])
  end
end
