class Admin::RegionsController < Admin::ApplicationController
  before_action :set_region, only: [:show, :edit, :update, :destroy]
  def index
    @regions = Region.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @region = Region.new
  end

  def show; end

  def create
    @region = Region.new(region_params)

    if @region.save
      redirect_to admin_regions_path, flash: { success: 'Successful create regions' }
    else
      render :new
    end
  end

  def edit; end

  def update
    if @region.update(region_params)
      redirect_to admin_regions_path, flash: { success: 'Successful create regions' }
    else
      render :edit
    end
  end

  def destroy
    if @region.destroy
      redirect_to admin_regions_path, flash: { success: 'Successful delete region' }
    else
      redirect_to admin_regions_path, flash: { error: 'Cannot delete region' }
    end
  end

  private

  def set_region
    @region = Region.friendly.find(params[:id])
  end
  
  def region_params
    params.require(:region).permit(:name, :abstract, :contact, rohe_ids: [])
  end
end
