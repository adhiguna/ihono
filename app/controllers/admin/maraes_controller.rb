class Admin::MaraesController < Admin::ApplicationController
  before_action :set_marae, only: [:show, :edit, :update, :destroy]

  def index
    @maraes = Marae.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @marae = Marae.new
  end

  def edit; end

  def create
    @marae = Marae.new(marae_params)

    if @marae.save
      redirect_to admin_maraes_path, notice: 'Marae was successfully created.'
    else
      render :new
    end
  end

  def update
    if @marae.update(marae_params)
      redirect_to admin_maraes_path, notice: 'Marae was successfully update.'
    else
      render :edit
    end
  end

  def destroy
    if @marae.destroy
      redirect_to admin_maraes_path, flash: { success: 'Successful delete marae' }
    else
      redirect_to admin_maraes_path, flash: { error: 'Cannot delete marae' }
    end
  end

  private
  
  def set_marae
    @marae = Marae.friendly.find(params[:id])
  end

  def marae_params
    params.require(:marae).permit(:name, :address, :suburb, :country,
      :postcode, :latitude, :longitude, :contact, :abstract, 
      iwi_ids: [], hapu_ids: [], wharenui_ids: [])
    .merge(turn_off_geocoder: true)
  end
end
