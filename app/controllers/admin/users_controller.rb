class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  
  def index
    @users = User.search_filters(params: filter_params).page(params[:page]) 
  end

  def show; end

  def edit; end

  def update
    if @user.update(user_params)
      redirect_to admin_users_path, flash: { success: 'Successful create user' }
    else
      render :edit
    end
  end

  def destroy
    if @user.discard 
      redirect_to admin_users_path, flash: { success: 'Successful delete user' }
    else
      redirect_to admin_users_path, flash: { error: 'Cannot delete user' }
    end
  end

  private

  def set_user
    @user = User.friendly.find(params[:id])
  end
  
  def user_params
    params.require(:user).permit(:first_name, :last_name, :role)
  end
end
