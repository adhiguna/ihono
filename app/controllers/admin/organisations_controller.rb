class Admin::OrganisationsController < Admin::ApplicationController
  before_action :set_organisation, only: [:edit, :update, :destroy]

  def index
    @organisations = Organisation.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @organisation = Organisation.new
  end

  def edit; end

  def create
    @organisation = Organisation.new(organisation_params)
    @organisation.user = current_user

    if @organisation.save
      redirect_to admin_organisations_path, notice: 'Organisation was successfully created.'
    else
      render :new
    end
  end

  def update
    if @organisation.update(organisation_params)
      redirect_to admin_organisations_path, notice: 'Organisation was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    if @organisation.destroy
      redirect_to admin_organisations_path, notice: 'Organisation was successfully destroyed.'
    end
  end

  private
  
  def set_organisation
    @organisation = Organisation.friendly.find(params[:id])
  end

  def organisation_params
    params.require(:organisation).permit(
      :name, :nzbn, :organisation_type, :hero_image,:abstract,
      :hero_image_crop_x, :hero_image_crop_y, :hero_image_crop_h, :hero_image_crop_w,
      region_ids: [],
      rohe_ids: [], 
      iwi_ids: []
    )
  end
end
