class Admin::WharenuisController < Admin::ApplicationController
  before_action :set_wharenui, only: [:show, :edit, :update, :destroy]
  def index
    @wharenuis = Wharenui.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @wharenui = Wharenui.new
  end

  def show; end

  def create
    @wharenui = Wharenui.new(wharenui_params)

    if @wharenui.save
      redirect_to admin_wharenuis_path, flash: { success: 'Successful create wharenuis' }
    else
      render :new
    end
  end

  def edit; end

  def update
    if @wharenui.update(wharenui_params)
      redirect_to admin_wharenuis_path, flash: { success: 'Successful create wharenuis' }
    else
      render :edit
    end
  end

  def destroy
    if @wharenui.destroy
      redirect_to admin_wharenuis_path, flash: { success: 'Successful delete wharenui' }
    else
      redirect_to admin_wharenuis_path, flash: { error: 'Cannot delete wharenui' }
    end
  end

  private

  def set_wharenui
    @wharenui = Wharenui.friendly.find(params[:id])
  end
  
  def wharenui_params
    params.require(:wharenui).permit(:name, :abstract, :contact, marae_ids: [])
  end
end
