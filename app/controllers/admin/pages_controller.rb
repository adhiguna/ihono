class Admin::PagesController < Admin::ApplicationController
  before_action :load_page, only: [:edit, :update]

  def index 
    @pages = Page.all
  end

  def edit; end

  def update
    @page.update(page_params)
    redirect_to admin_pages_path, flash: { success: "Success for update #{@page.title}" }
  end

  private
  def load_page
    @page = Page.friendly.find(params[:id])
  end

  def page_params
    params.require(:page).permit(:description, :hero_image)
  end
end
