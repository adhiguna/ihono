class Admin::AppointmentsController < Admin::ApplicationController
  respond_to :html

  def index
    @appointments = Appointment.search_filters(SearchAppointment.parse(appointment_params)).page(params[:page])
    @search = SearchAppointment.build_search(appointment_params)
  end

  def show
    @appointment = Appointment.find(params[:id])
    @back_link = admin_appointments_path
  end

  private
  def appointment_params
    params[:search_appointment] ||= SearchAppointment.init_filters

    params.require(:search_appointment).permit(
      :health_provider_id, :keyword, :sort, :service_worker, :contacted, :name, :appointment_date, contacts: [],
      symptoms: [], pre_exist_health_condition: [], age: []
    )
  end

end
