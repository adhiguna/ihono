class Admin::ApplicationController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_user_are_super_admin

  private

  def checking_user_are_super_admin
    authorize current_user, :super_admin?
  end
end
