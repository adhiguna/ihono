class Admin::HapusController < Admin::ApplicationController
  before_action :set_hapu, only: [:show, :edit, :update, :destroy]
  def index
    @hapus = Hapu.search_filters(params: filter_params).page(params[:page])
  end 

  def new
    @hapu = Hapu.new
  end

  def show; end

  def create
    @hapu = Hapu.new(hapu_params)

    if @hapu.save
      redirect_to admin_hapus_path, flash: { success: 'Successful create hapu' }
    else
      render :new
    end
  end

  def edit; end

  def update
    if @hapu.update(hapu_params)
      redirect_to admin_hapus_path, flash: { success: 'Successful create hapu' }
    else
      render :edit
    end
  end

  def destroy
    if @hapu.destroy
      redirect_to admin_hapus_path, flash: { success: 'Successful delete hapu' }
    else
      redirect_to admin_hapus_path, flash: { error: 'Cannot delete hapu' }
    end
  end

  private

  def set_hapu
    @hapu = Hapu.friendly.find(params[:id])
  end
  
  def hapu_params
    params.require(:hapu).permit(:name, :contact, :abstract, iwi_ids: [], marae_ids: [])
  end
end
