class Admin::RohesController < Admin::ApplicationController
  before_action :set_rohe, only: [:show, :edit, :update, :destroy]
  def index
    @rohes = Rohe.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @rohe = Rohe.new
  end

  def show; end

  def create
    @rohe = Rohe.new(rohe_params)

    if @rohe.save
      redirect_to admin_rohes_path, flash: { success: 'Successful create rohe' }
    else
      render :new
    end
  end

  def edit; end

  def update
    if @rohe.update(rohe_params)
      redirect_to admin_rohes_path, flash: { success: 'Successful create rohe' }
    else
      render :edit
    end
  end

  def destroy
    if @rohe.destroy
      redirect_to admin_rohes_path, flash: { success: 'Successful delete rohe' }
    else
      redirect_to admin_rohes_path, flash: { error: 'Cannot delete rohe' }
    end
  end

  private

  def set_rohe
    @rohe = Rohe.friendly.find(params[:id])
  end
  
  def rohe_params
    params.require(:rohe).permit(:name, :abstract, :contact, region_ids: [], iwi_ids: [])
  end
end
