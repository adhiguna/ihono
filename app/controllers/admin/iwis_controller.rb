class Admin::IwisController < Admin::ApplicationController
  before_action :set_iwi, only: [:show, :edit, :update, :destroy]
  def index
    @iwis = Iwi.search_filters(params: filter_params).page(params[:page])
  end

  def new
    @iwi = Iwi.new
  end

  def show; end

  def create
    @iwi = Iwi.new(iwi_params)

    if @iwi.save
      redirect_to admin_iwis_path, flash: { success: 'Successful create iwi' }
    else
      render :new
    end
  end

  def edit; end

  def update
    if @iwi.update(iwi_params)
      redirect_to admin_iwis_path, flash: { success: 'Successful create iwi' }
    else
      render :edit
    end
  end

  def destroy
    if @iwi.destroy
      redirect_to admin_iwis_path, flash: { success: 'Successful delete iwi' }
    else
      redirect_to admin_iwis_path, flash: { error: 'Cannot delete iwi' }
    end
  end

  private

  def set_iwi
    @iwi = Iwi.friendly.find(params[:id])
  end
  
  def iwi_params
    params.require(:iwi).permit(:name, :contact, :abstract, rohe_ids: [], hapu_ids: [])
  end
end
