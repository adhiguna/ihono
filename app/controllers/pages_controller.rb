class PagesController < ApplicationController
  def index
    @hide_header = true
    @posts = Post.search_filters(params[:filters]).search_order(params[:order]).page(params[:page]).per(5)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @page = Page.find_by_slug(params[:page])
    redirect_to root_path unless @page.present? 
  end
end
