class HapusController < ApplicationController
  before_action :set_hapu

  def show; end

  private

  def set_hapu
    @hapu = Hapu.friendly.find(params[:id])
  end
end
