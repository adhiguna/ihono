class ApplicationController < ActionController::Base
  protect_from_forgery
  include Pundit
  
  # before_action :basic_authenticate
  before_action :checking_if_user_has_password

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def filter_params
    params.permit(:keyword)
  end

  def basic_authenticate
    return if !Rails.env.production? || !ENV['HTTP_BASIC_USERNAME'].present?
    
    authenticate_or_request_with_http_basic do |username, password|
      username == ENV['HTTP_BASIC_USERNAME'] && password == ENV['HTTP_BASIC_PASSWORD']
    end
  end

  # to make it path correct redirect button
  def set_back_link(fallback_url, force = false)
    return fallback_url if force
    return params[:redirect_back_to] if params[:redirect_back_to].present?

    source_path = Rails.application.routes.recognize_path(request.env["HTTP_REFERER"])
    current_path = Rails.application.routes.recognize_path(request.env["PATH_INFO"])

    if source_path == current_path
      fallback_url
    else
      request.env["HTTP_REFERER"]
    end
  end

  private

  def checking_if_user_has_password
    if current_user && current_user.encrypted_password.nil? && controller_name != 'setup_account' && controller_name != 'two_factor_authentication'
      redirect_to users_setup_account_index_path
    end
  end

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(request.referrer || root_path)
  end
end
