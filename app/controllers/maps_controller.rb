class MapsController < ApplicationController
  def index
    @id_class = 'map-body'
    @results = SearchMap.data_maps(search_params)
    @search = SearchMap.build_search(search_params)
    @back_link = root_path

    respond_to do |format|
      format.html
      format.json { 
        render json: @results.to_json
      }
      format.js
    end
  end

  private

  def search_params
    params[:search_map] = params[:search_map] || SearchMap.init_filters

    # only allowed search user when user are present
    params[:search_map][:category].delete('People') unless current_user

    params.require(:search_map).permit(:distance, :latitude, :longitude, category: [])
  end
end
