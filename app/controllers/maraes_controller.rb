class MaraesController < ApplicationController
  before_action :set_marae

  def show; end

  private

  def set_marae
    @marae = Marae.friendly.find(params[:id])
  end
end
