class Corona::HealthProvidersController < Corona::ApplicationController
  def index
    @id_class = 'map-body'
    @back_link = corona_medical_checkup_path
    @results = SearchMapHealthProvider.extract_data('marker', SearchMapHealthProvider.parse(search_map_health_provider_params))
    @search = SearchMapHealthProvider.build_search(search_map_health_provider_params)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def index_list
    @back_link = corona_medical_checkup_path
    @results = SearchMapHealthProvider.extract_data('list', SearchMapHealthProvider.parse(search_map_health_provider_params))
    @search = SearchMapHealthProvider.build_search(search_map_health_provider_params)
  end

  private

  def search_map_health_provider_params
    params[:search_map_health_provider] = params[:search_map_health_provider] || SearchMapHealthProvider.init_filters
    params.require(:search_map_health_provider).permit(:distance, :latitude, :longitude, :category)
  end 
end
