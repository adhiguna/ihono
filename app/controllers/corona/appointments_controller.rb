class Corona::AppointmentsController < Corona::ApplicationController
  before_action :authenticate_user!
  before_action :load_health_provider, only: [:new, :create, :success]

  def index
    @appointments = current_user.appointments.booking_date_sort('desc').search_filters(params: keyword_params).page(params[:page])
  end

  def show
    @appointment = current_user.appointments.find(params[:id])
    @back_link = corona_appointments_path
  end
  
  def new
    @appointment = @health_provider.appointments.new
    @back_link = set_back_link(corona_health_provider_list_path)

    if current_user.present?
      @appointment.name = current_user.full_name
      @appointment.email = current_user.email
      @appointment.address = current_user.address
      @appointment.cell_phone = current_user.phone
    end
  end

  def create
    @appointment = @health_provider.appointments.new(appointment_params)
    @appointment.user = current_user if current_user.present?
    @back_link = corona_health_provider_appointment_path(@health_provider)

    if @appointment.save
      redirect_to corona_health_provider_appointment_success_path(@health_provider), flash: { success: 'Successful' }
    else
      render :new
    end
  end

  def success; end

  private

  def keyword_params
    params.permit(:keyword)
  end

  def appointment_params
    params.require(:appointment).permit(
      :address, :age, :appointment_date, :appointment_time, :email, :name, :home_phone,
      :contact_with_corona, :travel_overseas, :contact_with_overseas_traveller, 
      :health_care_worker, :essential_services_worker,
      :cell_phone, feels: [], pre_exist_health_condition: []
    )
  end
  
  def load_health_provider
    @health_provider = HealthProvider.friendly.find(params[:health_provider_id])
    authorize @health_provider, :can_booking_appointment?
  end
end
