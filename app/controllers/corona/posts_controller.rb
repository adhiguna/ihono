class Corona::PostsController < Corona::ApplicationController
  before_action :set_post, only: [:show]
  respond_to :html

  def index
    @posts = Post.corona_post.search_filters(SearchPost.parse(search_post_params))
                  .search_order(params[:order])
                  .page(params[:page])
                  .per(5)
                  
    @search = SearchPost.build_search(search_post_params)
    @back_link = corona_root_path

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @back_link = corona_posts_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.corona_post.friendly.find(params[:id])
    end

    def search_post_params
      params[:search_post] = params[:search_post] || SearchPost.init_filters
      params.require(:search_post).permit(:category, :region)
    end
end
