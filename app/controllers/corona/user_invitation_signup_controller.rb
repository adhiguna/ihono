class Corona::UserInvitationSignupController < Corona::ApplicationController
  before_action :authenticate_user!
  before_action :set_user
  respond_to :html

  def index
    @user.send_invitations.build if @user.send_invitations.blank?
  end
  
  def create
    @user.assign_attributes(user_invitation_signup_params) if params[:user].present?

    if @user.save
      redirect_to corona_health_status_invitation_signup_path, flash: { success: 'Successful send invitation message' }
    else
      render :new
    end
  end

  private

  def user_invitation_signup_params
    params.require(:user).permit(send_invitations_attributes: [:name, :email, :phone])
  end

  def set_user
    @user = current_user
  end
end
