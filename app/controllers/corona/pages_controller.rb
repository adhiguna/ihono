class Corona::PagesController < Corona::ApplicationController
  def index
    @back_link = root_path
  end

  def medical_checkup
    @back_link = root_path
  end

  def health_status; end
end
