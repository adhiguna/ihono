class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :set_post, only: [:show, :edit, :update, :destroy, :toggle_favorite, :toggle_like]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.public_post.search_filters(params[:filters]).search_order(params[:order]).page(params[:page]).per(5)
    @back_link = posts_path

    respond_to do |format|
      format.html
      format.js
    end
  end

  def my_posts
    @posts = current_user.posts.search_filters(params[:filters]).search_order(params[:order]).page(params[:page])
  end

  def favorites
    @posts = current_user.post_favorites.search_filters(params[:filters]).search_order(params[:order]).page(params[:page])
  end

  def toggle_favorite
    @favorite = current_user.toggle_favorite_post!(@post)
    respond_to :js
  end

  def toggle_like
    @like = current_user.toggle_like_post!(@post)
    @total_likes = @post.user_likes.size

    respond_to :js
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @back_link = posts_path
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    authorize @post, :update?
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        format.html { redirect_to redirection_path, notice: 'Post was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /posts/1
  def update
    authorize @post, :update?
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to redirection_path, notice: 'Post was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /posts/1
  def destroy
    authorize @post, :destroy?
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :hero_image, :x, :y, :h, :w, :content, :post_type, :region_id)
    end

    def redirection_path
      if @post.corona_post?
        corona_post_path(@post)
      else
        post_path(@post)
      end
    end
end
