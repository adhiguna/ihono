class Users::UserInvitationFriendshipsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user
  before_action :load_user_friend, only: [:create, :update, :destroy, :unfriend]

  def index
    @user_invitation_friendships = @user.invitation_friendships.request_not_by(@user)
  end

  def create
    @user.invite_friend(@user_friend)
    respond_to :js
  end

  def destroy
    @user.uninvite_friend(@user_friend)
    respond_to :js
  end

  def update
    @user.send("#{params[:status]}_invite_friend", @user_friend)
    respond_to do |format|
      format.js
      format.html { redirect_to redirect_path, flash: { success: "Successful #{params[:status]} invitation" } }
    end
  end

  def unfriend
    @user.remove_friend(@user_friend)
    
    respond_to do |format|
      format.js
      format.html { redirect_to redirect_path, flash: { success: "Successful #{params[:status]} invitation" } }
    end
  end

  private

  def load_user
    @user = current_user
  end

  def load_user_friend
    @user_friend = User.friendly.find(params[:user_friend_id])
  end

  def redirect_path
    params[:redirect_to] || root_path
  end
end
