class Users::SetupAccountController < ApplicationController
  before_action :authenticate_user!
  include Wicked::Wizard

  steps :set_profile, :set_password, :finished

  def show
    @user = current_user
    render_wizard
  end

  def update
    @user = current_user
    case step
    when :set_profile
      @user.update(set_profile_params)
    when :set_password
      if @user.update(set_password_params)
        bypass_sign_in(@user) # needed for devise
      end
    end
    render_wizard @user
  end

  private

  def set_profile_params
    params.require(:user).permit(:first_name, :last_name)
  end

  def set_password_params
    params.require(:user).permit(:username, :password)
  end
end