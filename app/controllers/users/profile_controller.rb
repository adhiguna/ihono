class Users::ProfileController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user, only: [:edit, :update, :friend_invitations, :update_location]
  before_action :get_list_iwis, only: [:edit, :update]

  def index; end

  def edit; end

  def update
    @user.update(user_params)
    redirect_to users_profile_index_path, flash: { success: 'Success for update profile' }
  end

  def update_location
    result = @user.update(update_location_params)

    respond_to do |format|
      format.json { render json: { success: result } }
    end
  end

  private

  def get_list_iwis
    @iwis = Iwi.all.pluck(:name, :id)
  end

  def load_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(
      :first_name, :last_name, :abstract, :suburb, :city, :postcode, :contact, :competency, :avatar,
      :avatar_crop_x, :avatar_crop_y, :avatar_crop_h, :avatar_crop_w, :interest, :work, :phone,
      social_media_attributes: [:id, :link, :name, :social_media_type, :_destroy],
      lineages_attributes: [:id, :iwi_id, :hapu_id, :whanau, :_destroy]
    )
  end

  def update_location_params
    params.permit(:visibility, :latitude, :longitude)
  end
end
