# class for override library https://github.com/Houdini/two_factor_authentication
class Users::TwoFactorAuthenticationController < Devise::TwoFactorAuthenticationController
  def show
    @hide_flash_message = true
  end

  private

  def after_two_factor_fail_for(resource)
    set_flash_message :alert, :attempt_failed, now: true
    render :show
  end

  def prepare_and_validate
    redirect_to :root && return if resource.nil?
  end
end