class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation
  before_action :set_location, only: %w[edit update destroy]

  def index
    @locations = @organisation.locations
  end

  def new
    @location = @organisation.locations.new
  end

  def edit; end

  def create
    @location = @organisation.locations.new(location_params)

    if @location.save
      redirect_to organisation_locations_path(@organisation), notice: 'Location was successfully created.'
    else
      @back_link = organisation_locations_path(@organisation)
      render :new
    end
  end

  def update
    if @location.update(location_params)
      redirect_to organisation_locations_path(@organisation), notice: 'Location was successfully updated.'
    else
      @back_link = organisation_locations_path(@organisation)
      render :edit
    end
  end

  def destroy
    if @location.destroy
      redirect_to organisation_locations_path(@organisation), notice: 'Location was successfully destroyed.'
    end
  end

  private
  def set_location
    @location = @organisation.locations.find(params[:id])
  end
  
  def set_organisation
    @organisation = Organisation.friendly.find(params[:organisation_id])
    authorize @organisation, :access_location?
  end

  def location_params
    params.require(:location).permit(:address, :suburb, :country,
      :postcode, :latitude, :longitude, :contact)
    .merge(turn_off_geocoder: true)
  end
end
