class RegionsController < ApplicationController
  before_action :set_region

  def show; end

  private

  def set_region
    @region = Region.friendly.find(params[:id])
  end
end
