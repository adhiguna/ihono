class SearchController < ApplicationController
  def index
    @results = SearchGroup.data_count_groups(params: search_params, user_present: current_user.present?)

    respond_to do |format|
      format.html
      format.json { 
        render json: @results.to_json
      }
    end
  end

  def view_results
    @view_results = SearchGroup.data_view_results(params: data_view_results_params, user_present: true)
    if @view_results.present?
      respond_to :html
    else
      redirect_to root_path
    end
  end

  private
  
  def search_params
    params[:search_group] = params[:search_group] || SearchGroup.init_filters
    params.require(:search_group).permit(:keyword)
  end

  def data_view_results_params
    params.permit(:group, :keyword)
  end
end
