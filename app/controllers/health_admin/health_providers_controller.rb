class HealthAdmin::HealthProvidersController < HealthAdmin::ApplicationController
  before_action :set_health_provider, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @health_providers = HealthProvider.all.page(params[:page])
  end

  def new
    @health_provider = HealthProvider.new
  end

  def edit; end

  def create
    @health_provider = HealthProvider.new(health_provider_params)

    respond_to do |format|
      if @health_provider.save
        format.html { redirect_to health_admin_health_providers_url, notice: 'Health provider was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @health_provider.update(health_provider_params)
        format.html { redirect_to health_admin_health_providers_url, notice: 'Health provider was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @health_provider.destroy
    respond_to do |format|
      format.html { redirect_to health_admin_health_providers_url, notice: 'Health provider was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_health_provider
      @health_provider = HealthProvider.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def health_provider_params
      params.require(:health_provider).permit(
        :name, :postcode, :slug, :suburb, :address, :region, :service,
        :contact, :country, :latitude, :longitude, :abstract,
        :open, :close, :allow_appointment
      )
    end
end
