class HealthAdmin::ApplicationController < ApplicationController
  before_action :authenticate_user!
  before_action :checking_user_are_health_admin

  private

  def checking_user_are_health_admin
    authorize current_user, :health_admin?
  end
end
