class SendCreateUserInvitationFriendshipWorker
  include Sidekiq::Worker

  def perform(user_invitation_friendship_id)
    u = UserInvitationFriendship.find(user_invitation_friendship_id)

    UserInvitationMailer.create(u).deliver
  end
end