##
# this class for add shrine to background job
class ShrineUploadWorker
  include Sidekiq::Worker

  def perform(data)
    Shrine::Attacher.promote(data)
  end
end
