##
# this class for add shrine to background job
class ShrineDeleteWorker
  include Sidekiq::Worker

  def perform(data)
    Shrine::Attacher.delete(data)
  end
end
