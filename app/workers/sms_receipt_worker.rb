class SmsReceiptWorker
  include Sidekiq::Worker

  def perform(params)
    SmsReceiptService.call(eval(params))
  end
end