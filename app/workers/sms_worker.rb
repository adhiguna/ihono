class SmsWorker
  include Sidekiq::Worker

  def perform(phone_number, message)
    Message::Sms.call(phone_number, message)
  end
end