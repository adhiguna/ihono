class SendUserInvitationSignupWorker
  include Sidekiq::Worker

  def perform(user_invitation_signup_id)
    user_invitation_signup = UserInvitationSignup.find(user_invitation_signup_id)

    UserInvitationSignupService.call(user_invitation_signup)
  end
end