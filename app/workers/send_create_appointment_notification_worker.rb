class SendCreateAppointmentNotificationWorker
  include Sidekiq::Worker

  def perform(appointment_id)
    appointment = Appointment.find(appointment_id)

    CreateAppointmentNotificationService.call(appointment)
  end
end