# frozen_string_literal: true

class ReceiverNotification < ApplicationRecord 
  belongs_to :notification
  belongs_to :user
  counter_culture :user, 
                  column_name: 'receiver_notifications_count',
                  column_names: {
                    ["receiver_notifications.is_read = false AND receiver_notifications.is_show = true"] => 'receiver_notifications_count'
                  }

  scope :undeleted, -> { joins(:notification).where(notifications: { is_show: true }, is_show: true) }
  scope :unread, -> { where(is_read: false) }


  delegate :sender_user, to: :notification, prefix: false
  delegate :notifiable_status, to: :notification, prefix: false
  delegate :notifiable, to: :notification, prefix: false
  delegate :notifiable_type, to: :notification, prefix: false
  delegate :notifiable_id, to: :notification, prefix: false

  alias_attribute :receiver, :user
  alias_attribute :sender, :sender_user
  alias_attribute :status, :notifiable_status
end

# == Schema Information
#
# Table name: receiver_notifications
#
#  id              :bigint           not null, primary key
#  is_read         :boolean          default(FALSE), not null
#  is_show         :boolean          default(TRUE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  notification_id :bigint
#  user_id         :bigint
#
# Indexes
#
#  index_receiver_notifications_on_notification_id  (notification_id)
#  index_receiver_notifications_on_user_id          (user_id)
#
