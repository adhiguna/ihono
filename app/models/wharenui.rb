# frozen_string_literal: true

class Wharenui < ApplicationRecord
  include InfoData

  has_and_belongs_to_many :maraes, :join_table => :maraes_wharenuis
  has_and_belongs_to_many :hapus, :join_table => :hapus_wharenuis
  has_and_belongs_to_many :iwis, :join_table => :iwis_wharenuis
  has_and_belongs_to_many :rohes, :join_table => :rohes_wharenuis
  has_many :organisations, through: :iwis
  has_many :regions, through: :rohes

  validates :name, presence: true
end

# == Schema Information
#
# Table name: wharenuis
#
#  id       :bigint           not null, primary key
#  abstract :string
#  contact  :string
#  name     :string           not null
#  slug     :string           not null
#
