# frozen_string_literal: true

class WorkExperience < ApplicationRecord
end

# == Schema Information
#
# Table name: work_experiences
#
#  id          :bigint           not null, primary key
#  company     :string           not null
#  description :text
#  end_date    :date             not null
#  location    :string
#  start_date  :date             not null
#  title       :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
