class Page < ApplicationRecord
  extend FriendlyId
  include HeroImageUploader::Attachment.new(:hero_image)

  PAGE_TYPES = ['About', 'Terms and Privacy', 'Contact Us']
  friendly_id :title, use: :slugged

  validates :page_type, inclusion: { in: Page::PAGE_TYPES }, uniqueness: true, presence: true
  validates :title, presence: true
end

# == Schema Information
#
# Table name: pages
#
#  id                  :bigint           not null, primary key
#  description         :text
#  hero_image_data     :text
#  hero_image_filename :string
#  page_type           :string
#  slug                :string           not null
#  title               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
