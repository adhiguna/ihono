# frozen_string_literal: true

class Iwi < ApplicationRecord
  include InfoData

  has_and_belongs_to_many :rohes, :join_table => :rohes_iwis
  has_and_belongs_to_many :hapus, :join_table => :iwis_hapus
  has_and_belongs_to_many :maraes, :join_table => :iwis_maraes
  has_and_belongs_to_many :wharenuis, :join_table => :iwis_wharenuis
  has_and_belongs_to_many :organisations, :join_table => :iwis_organisations
  has_many :regions, through: :rohes

  validates :name, presence: true
end

# == Schema Information
#
# Table name: iwis
#
#  id       :bigint           not null, primary key
#  abstract :text
#  contact  :text
#  name     :string           not null
#  slug     :string           not null
#
