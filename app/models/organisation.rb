# frozen_string_literal: true

class Organisation < ApplicationRecord
  include OrganisationHeroImageUploader::Attachment.new(:hero_image)
  include InfoMap
  include InfoData 

  acts_as_geolocated through: :locations

  multisearchable against: [:name, :abstract, :contacts]
  
  TYPE_LIST = ['Iwi Organisation or Trust', 'Māori Business', 'Community Organisation', 'Education Organisation', 'Health Organisation', 'Central or Local Government']

  attr_accessor :hero_image_crop_x, :hero_image_crop_y, :hero_image_crop_h, :hero_image_crop_w

  has_and_belongs_to_many :regions, :join_table => :regions_organisations
  has_and_belongs_to_many :rohes, :join_table => :rohes_organisations
  has_and_belongs_to_many :iwis, :join_table => :iwis_organisations
  has_many :locations, as: :locationable
  belongs_to :user, optional: true

  accepts_nested_attributes_for :locations, reject_if: proc{ |attributes| attributes['address'].blank? }, allow_destroy: true

  validates :name, presence: true
  validates :organisation_type, :inclusion => { :in => Organisation::TYPE_LIST }

  before_validation :set_cropping_hero_image, if: Proc.new { |attribute| attribute.hero_image_crop_x.present? && attribute.hero_image_crop_y.present? && attribute.hero_image_crop_w.present? && attribute.hero_image_crop_h.present? }

  private

  def contacts
    locations.pluck(:address, :contact).flatten.join(' ')
  end

  def set_cropping_hero_image
    data = JSON.parse(hero_image_data)

    data["metadata"]["cropping"] = {
      'x' => hero_image_crop_x,
      'y' => hero_image_crop_y,
      'h' => hero_image_crop_h,
      'w' => hero_image_crop_w,
    }

    self.hero_image = data.to_json
  end
end

# == Schema Information
#
# Table name: organisations
#
#  id                  :bigint           not null, primary key
#  abstract            :text
#  hero_image_data     :text
#  hero_image_filename :string
#  name                :string           not null
#  nzbn                :string
#  organisation_type   :string           not null
#  slug                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  user_id             :bigint
#
# Indexes
#
#  index_organisations_on_user_id  (user_id)
#
