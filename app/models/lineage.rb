class Lineage < ApplicationRecord
  belongs_to :user, touch: true
  belongs_to :iwi
  belongs_to :hapu, optional: true
end

# == Schema Information
#
# Table name: lineages
#
#  id         :bigint           not null, primary key
#  whanau     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  hapu_id    :bigint
#  iwi_id     :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_lineages_on_hapu_id  (hapu_id)
#  index_lineages_on_iwi_id   (iwi_id)
#  index_lineages_on_user_id  (user_id)
#
