# frozen_string_literal: true

class Notification < ApplicationRecord

  NOTIFIABLE_STATUS = {
    'UserInvitationFriendship' => ['create']
  }

  has_many :receivers, class_name: 'ReceiverNotification', dependent: :destroy
  belongs_to :sender, class_name: 'SenderNotification', foreign_key: 'sender_notification_id', dependent: :destroy

  accepts_nested_attributes_for :receivers, :sender

  belongs_to :notifiable, polymorphic: true

  delegate :user, to: :sender, prefix: true

  ##
  # instant create notification
  def self.create_notification(user_sender, user_receivers, notifiable, notifiable_status)
    if checking_notifiable_status(notifiable, notifiable_status)
      data = new(notifiable_status: notifiable_status, notifiable: notifiable)

      # build user_sender
      user_sender = get_user(user_sender)
      data.build_sender(user: user_sender)

      # build user_receivers
      user_receivers = get_user_receivers_data(user_receivers)

      user_receivers.map do |user_receiver|
        data.receivers.build(user: user_receiver)
      end

      data.save
    end
  end

  ##
  # set user friend by object not by id
  def self.get_user(user_data)
    user_data = User.find(user_data) if user_data.class != User
    return user_data
  end

  ##
  # build user receipt if array and just return array for easy build
  def self.get_user_receivers_data(user_receivers)
    if user_receivers.is_a?(Array)
      user_receivers = user_receivers.map {|user_receiver| get_user(user_receiver) }
    else
      user_receivers = get_user(user_receivers)
    end

    return user_receivers.is_a?(Array) ? user_receivers : [user_receivers]
  end

  ##
  # checking if notification status match with list, otherwise raise error
  def self.checking_notifiable_status(notifiable, notifiable_status)
    check_notification = Notification::NOTIFIABLE_STATUS[notifiable.class.to_s]

    if check_notification.present? && check_notification.include?(notifiable_status)
      return true
    else
      raise "can't find #{notifiable.class} with status #{notifiable_status}"
    end
  end
end

# == Schema Information
#
# Table name: notifications
#
#  id                     :bigint           not null, primary key
#  is_show                :boolean          default(TRUE), not null
#  notifiable_status      :string           not null
#  notifiable_type        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  notifiable_id          :bigint
#  sender_notification_id :bigint
#
# Indexes
#
#  index_notifications_on_notifiable_type_and_notifiable_id  (notifiable_type,notifiable_id)
#  index_notifications_on_sender_notification_id             (sender_notification_id)
#
