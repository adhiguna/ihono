# frozen_string_literal: true

class SocialMedia < ApplicationRecord
  SOCIAL_MEDIA_TYPE_LIST = ['Facebook', 'Twitter', 'Linkedin', 'Instagram']

  belongs_to :mediable, polymorphic: true

  validates :link, presence: true
  validates :name, presence: true
  validates :social_media_type, :inclusion => { :in => SocialMedia::SOCIAL_MEDIA_TYPE_LIST }
end

# == Schema Information
#
# Table name: social_media
#
#  id                :bigint           not null, primary key
#  link              :string           not null
#  mediable_type     :string
#  name              :string           not null
#  social_media_type :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  mediable_id       :bigint
#
# Indexes
#
#  index_social_media_on_mediable_type_and_mediable_id  (mediable_type,mediable_id)
#
