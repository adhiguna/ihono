# frozen_string_literal: true

class SenderNotification < ApplicationRecord
  belongs_to :user
  has_one :notification, :foreign_key => 'sender_notification_id'
end

# == Schema Information
#
# Table name: sender_notifications
#
#  id         :bigint           not null, primary key
#  is_show    :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_sender_notifications_on_user_id  (user_id)
#
