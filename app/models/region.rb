# frozen_string_literal: true

class Region < ApplicationRecord
  include InfoData

  has_and_belongs_to_many :rohes, :join_table => :regions_rohes
  has_and_belongs_to_many :organisations, :join_table => :regions_organisations
  has_many :iwis, through: :rohes
  has_many :hapus, through: :iwis
  has_many :maraes, through: :hapus
  has_many :wharenuis, through: :maraes

  validates :name, presence: true

  scope :name_first, -> { order('name ASC') }
end

# == Schema Information
#
# Table name: regions
#
#  id       :bigint           not null, primary key
#  abstract :text
#  contact  :text
#  name     :string           not null
#  slug     :string           not null
#
