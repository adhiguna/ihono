# frozen_string_literal: true

class User < ApplicationRecord
  include Discard::Model
  include Users::InviteFriendModule
  include Users::PostModule
  include Users::SlugGeneratorModule
  include Users::DeviseExtModule
  include Users::AvatarModule
  include Users::LocationModule
  include Users::InvitationSignupModule
  include InfoMap
  include InfoData
  include Users::PgSearchExtModule

  COMPETENCY_LIST = ['Novice', 'Intermediate', 'Advanced']
  enum role: [:user, :health_admin, :super_admin]

  validates :username, uniqueness: true, 
                       allow_blank: true, 
                       allow_nil: true, 
                       format: { with: /^[a-zA-Z0-9_\.]*$/, multiline: true, message: 'Only valid for alphanumeric' }

  has_many :posts, dependent: :nullify
  has_many :organisations, dependent: :nullify
  has_many :sender_notifications, dependent: :destroy
  has_many :receiver_notifications, -> { order('receiver_notifications.created_at DESC') }, dependent: :destroy
  has_many :receiver_notify, through: :receiver_notifications, class_name: 'Notification'
  has_and_belongs_to_many :post_favorites, :join_table => :users_favorite_posts, class_name: 'Post'
  has_and_belongs_to_many :post_likes, :join_table => :users_like_posts, class_name: 'Post'
  has_many :social_media, -> { order('created_at asc') }, as: :mediable, class_name: 'SocialMedia', dependent: :destroy
  has_many :lineages, dependent: :destroy
  has_many :appointments, dependent: :nullify
  has_many :send_invitations, class_name: 'UserInvitationSignup'
  has_many :user_invites, through: :send_invitations, :source => :user_invited
  has_one :receive_invitation, class_name: 'UserInvitationSignup', primary_key: :id, foreign_key: :user_invited_id
  has_one :owner_invite, through: :receive_invitation, :source => :owner_invited

  accepts_nested_attributes_for :social_media, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :lineages, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :send_invitations#, reject_if: Proc.new{ |attribute| attribute['name'].blank? && (attribute['email'].blank? || attribute['phone'].blank?) }, update_only: true, allow_destroy: true

  default_scope -> { kept }
  scope :can_found, -> { where(visibility: true) }
  
  ##
  # set display name
  def full_name 
    case
    when first_name.present? && last_name.present?
      "#{first_name} #{last_name}"
    when first_name.present?
      first_name
    when last_name.present?
      last_name
    else
      email
    end
  end

  def name
    full_name
  end

  def read_all_notification
    receiver_notifications.update(is_read: true)
    update_column(:receiver_notifications_count, 0)
  end

  protected

  ##
  # making registration no need password
  def password_required?
    false
  end

  private

  ##
  # sending code authentification when signin
  def send_two_factor_authentication_code(code)
    # take a look to devise.rb
    TwoFactorAuthenticationMailer.send_to_user(self, code).deliver unless second_factor_attempts_count == 3
  end
end

# == Schema Information
#
# Table name: users
#
#  id                            :bigint           not null, primary key
#  abstract                      :text
#  address                       :text
#  avatar_data                   :text
#  avatar_filename               :string
#  city                          :string
#  competency                    :string
#  contact                       :text
#  direct_otp                    :string
#  direct_otp_sent_at            :datetime
#  discarded_at                  :datetime
#  email                         :string           default(""), not null
#  encrypted_otp_secret_key      :string
#  encrypted_otp_secret_key_iv   :string
#  encrypted_otp_secret_key_salt :string
#  encrypted_password            :string
#  first_name                    :string
#  interest                      :text
#  last_name                     :string
#  latitude                      :float
#  longitude                     :float
#  phone                         :string
#  postcode                      :string
#  receiver_notifications_count  :integer          default(0), not null
#  remember_created_at           :datetime
#  reset_password_sent_at        :datetime
#  reset_password_token          :string
#  role                          :integer          default("user"), not null
#  second_factor_attempts_count  :integer          default(0)
#  slug                          :string           not null
#  suburb                        :string
#  totp_timestamp                :datetime
#  username                      :string
#  visibility                    :boolean          default(TRUE), not null
#  work                          :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
# Indexes
#
#  index_users_on_discarded_at              (discarded_at)
#  index_users_on_email                     (email) UNIQUE
#  index_users_on_encrypted_otp_secret_key  (encrypted_otp_secret_key) UNIQUE
#  index_users_on_reset_password_token      (reset_password_token) UNIQUE
#
