# frozen_string_literal: true

class UserInvitationFriendship < ApplicationRecord
  belongs_to :user
  belongs_to :user_friend, class_name: 'User'
  belongs_to :request_by, class_name: 'User'
  has_many :notifications, as: :notifiable

  scope :pending, -> { where(invitation_status: 'pending') }
  scope :accepted, -> { where(invitation_status: 'accepted') }
  scope :request_by, -> (user) { where(request_by: user) }
  scope :request_not_by, -> (user) { where.not(request_by: user) }
  scope :with_friend,  -> (user) { where('user_id = :user_id OR user_friend_id = :user_id', user_id: user.id) }

  after_create :send_email_invitation, :create_notification
  before_update :set_by_status, if: Proc.new { |f| f.invitation_status_changed? }

  def accept!
    update(invitation_status: 'accepted')
  end

  def reject!
    update(invitation_status: 'rejected')
  end

  private

  def create_notification
    Notification.create_notification(user, user_friend, self, 'create')
  end

  def send_email_invitation
    SendCreateUserInvitationFriendshipWorker.perform_async(self.id)
  end

  def set_by_status
    if invitation_status == 'accepted'
      user.add_friend(user_friend)
    elsif invitation_status == 'rejected'
      self.destroy
    end
  end
end

# == Schema Information
#
# Table name: user_invitation_friendships
#
#  id                :bigint           not null, primary key
#  invitation_status :string           default("pending"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  request_by_id     :bigint
#  user_friend_id    :bigint
#  user_id           :bigint
#
# Indexes
#
#  index_user_invitation_friendships_on_request_by_id               (request_by_id)
#  index_user_invitation_friendships_on_user_friend_id              (user_friend_id)
#  index_user_invitation_friendships_on_user_id                     (user_id)
#  index_user_invitation_friendships_on_user_id_and_user_friend_id  (user_id,user_friend_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (request_by_id => users.id)
#  fk_rails_...  (user_friend_id => users.id)
#
