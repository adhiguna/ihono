# frozen_string_literal: true

class Rohe < ApplicationRecord
  include InfoData
  
  has_and_belongs_to_many :regions, :join_table => :regions_rohes
  has_and_belongs_to_many :iwis, :join_table => :rohes_iwis
  has_and_belongs_to_many :hapus, :join_table => :rohes_hapus
  has_and_belongs_to_many :maraes, :join_table => :rohes_maraes
  has_and_belongs_to_many :wharenuis, :join_table => :rohes_wharenuis
  has_and_belongs_to_many :organisations, :join_table => :rohes_organisations

  validates :name, presence: true
end

# == Schema Information
#
# Table name: rohes
#
#  id       :bigint           not null, primary key
#  abstract :text
#  contact  :text
#  name     :string           not null
#  slug     :string           not null
#
