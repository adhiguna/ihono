# frozen_string_literal: true

class PostImage < ApplicationRecord
end

# == Schema Information
#
# Table name: post_images
#
#  id             :bigint           not null, primary key
#  image_data     :text
#  image_filename :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
