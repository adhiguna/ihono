# frozen_string_literal: true

class Location < ApplicationRecord
  include LocationGeocoder

  acts_as_geolocated

  validates :address, presence: true

  belongs_to :locationable, polymorphic: true, touch: true

  scope :latitude_longitude_present, -> { where.not(latitude: nil).where.not(longitude: nil) }
end

# == Schema Information
#
# Table name: locations
#
#  id                :bigint           not null, primary key
#  address           :text             not null
#  contact           :text
#  country           :string
#  latitude          :float
#  locationable_type :string
#  longitude         :float
#  postcode          :string
#  suburb            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  locationable_id   :bigint
#
# Indexes
#
#  index_locations_on_locationable_type_and_locationable_id  (locationable_type,locationable_id)
#
