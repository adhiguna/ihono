# frozen_string_literal: true

class Hapu < ApplicationRecord
  include InfoData

  has_and_belongs_to_many :iwis, :join_table => :iwis_hapus
  has_and_belongs_to_many :rohes, :join_table => :rohes_hapus
  has_and_belongs_to_many :maraes, :join_table => :hapus_maraes
  has_and_belongs_to_many :wharenuis, :join_table => :hapus_wharenuis
  has_many :regions, through: :rohes

  validates :name, presence: true
end

# == Schema Information
#
# Table name: hapus
#
#  id       :bigint           not null, primary key
#  abstract :text
#  contact  :text
#  name     :string           not null
#  slug     :string           not null
#
