module Appointments::SortModule
  def self.included(base)
    base.send :scope, :booking_date_sort, ->(direction) { order({ appointment_date: direction, appointment_time: (direction == :asc ? :desc : :asc) }) }
    base.send :scope, :health_provider_sort, ->(direction) { joins(:health_provider).order('health_providers.name' => direction ) }
    base.send :scope, :name_sort, ->(direction) { order({ name: direction }) }
    base.send :scope, :feels_sort, ->(direction) { order("array_length(appointments.feels, 1) #{direction}") }
    base.send :scope, :pre_exist_health_condition_sort, ->(direction) { order("array_length(appointments.pre_exist_health_condition, 1) #{direction}") }
  
    base.instance_eval do
      def service_worker_sort(direction)
        select('
          appointments.*, 
          (
            CASE
            WHEN appointments.health_care_worker IS TRUE AND appointments.essential_services_worker IS TRUE THEN
              2
            WHEN appointments.health_care_worker IS TRUE OR appointments.essential_services_worker IS TRUE THEN
              1
            ELSE
              0
            END
          ) as service_worker
        ').order(service_worker: direction)
      end
  
      def contacted_sort(direction)
        select('
          appointments.*, 
          (
            CASE
            WHEN appointments.contact_with_corona IS TRUE AND appointments.contact_with_overseas_traveller IS TRUE AND appointments.travel_overseas IS TRUE THEN
              2
            WHEN appointments.contact_with_corona IS TRUE OR appointments.contact_with_overseas_traveller IS TRUE OR appointments.travel_overseas IS TRUE THEN
              1
            ELSE
              0
            END
          ) as contacted
        ').order(contacted: direction)
      end

      def age_sort(direction)
        age_list = AGE_LIST.enum_for(:each_with_index).map { |age, index| "WHEN appointments.age = '#{age}' THEN #{index}"}.join(' ')
        
        select("
          appointments.*, 
          (
            CASE
            #{age_list}
            END
          ) as age_order
          ").order(age_order: direction)
      end

      def contacts_sort(direction)
        select('
          appointments.*, 
          (
            CASE
            WHEN appointments.email IS NOT NULL AND appointments.cell_phone IS NOT NULL AND appointments.home_phone IS NOT NULL THEN
              2
            WHEN appointments.email IS NOT NULL OR appointments.cell_phone IS NOT NULL OR appointments.home_phone IS NOT NULL THEN
              1
            ELSE
              0
            END
          ) as contacts
        ').order(contacts: direction)
      end
    end
  end
end