module Appointments::FilterModule
  def self.included(base)
    base.send :scope, :search_by_health_provider_id, -> (health_provider_id) { where(health_provider_id: health_provider_id) }
    base.send :scope, :search_by_service_worker, -> { where("appointments.health_care_worker = 'true' OR appointments.essential_services_worker = 'true'") }
    base.send :scope, :search_by_contacted, ->  { where("appointments.contact_with_corona = 'true' OR appointments.contact_with_overseas_traveller = 'true' OR appointments.travel_overseas = 'true'") }
    base.send :scope, :search_by_contacts, -> (contacts) { where("#{contacts.map{ |f| "(appointments.#{f} IS NOT NULL AND appointments.#{f} != '')"}.join(' OR ')}") }
    base.send :scope, :search_by_name, -> (name) { where("REGEXP_REPLACE(unaccent(appointments.name), '(\\W+)', ' ', 'g') ILIKE ?", "%#{name.downcase.gsub(/(\W+)/, ' ')}%") }
    base.send :pg_search_scope, :search_by_keyword, against: [:name, :address, :email, :cell_phone, :home_phone], using: { trigram: { word_similarity: true } }, ignoring: :accents
    base.send :scope, :search_by_appointment_date, -> (first_date, end_date) { where(appointment_date: first_date..end_date) }

    base.instance_eval do
      def search_by_symptoms(symptoms)
        symptoms = symptoms.split(' ') unless symptoms.is_a?(Array)
  
        where('appointments.feels && array[:symptoms]::varchar[]', symptoms: symptoms)
      end
  
      def search_by_pre_exist_health_condition(pre_exist_health_condition)
        pre_exist_health_condition = pre_exist_health_condition.split(' ') unless pre_exist_health_condition.is_a?(Array)
  
        where('appointments.pre_exist_health_condition && array[:pre_exist_health_condition]::varchar[]', pre_exist_health_condition: pre_exist_health_condition)
      end
  
      def search_by_age(age)
        age = age.split(' ') unless age.is_a?(Array)
        age = age.map{ |f| "%#{f.gsub(/(\W+\-)/, ' ')}%" }
  
        where('appointments.age ILIKE any (array[:age])', age: age)
      end

      def search_filters(params: {})
        data = where(false)
  
        data = data.search_by_health_provider_id(params[:health_provider_id]) if params[:health_provider_id].present?
        data = data.search_by_service_worker if params[:service_worker].present?
        data = data.search_by_contacted if params[:contacted].present?
        data = data.search_by_symptoms(params[:symptoms]) if params[:symptoms].present?
        data = data.search_by_pre_exist_health_condition(params[:pre_exist_health_condition]) if params[:pre_exist_health_condition].present?
        data = data.search_by_age(params[:age]) if params[:age].present?
        data = data.search_by_contacts(params[:contacts]) if params[:contacts].present?
        data = data.search_by_name(params[:name]) if params[:name].present?
        data = data.search_by_keyword(params[:keyword]) if params[:keyword].present?
  
        if params[:appointment_date].present?
          date_range = params[:appointment_date].split(' - ')
  
          data = data.search_by_appointment_date(date_range.first, date_range.last)
        end
  
        # sort
        if params[:sort].present?
          data = data.send("#{params[:sort].split(' ').first}_sort", params[:sort].split(' ').last.to_sym)
        else
          data = data.booking_date_sort(:desc)
        end
  
        data
      end
    end
  end
end