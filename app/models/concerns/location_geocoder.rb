module LocationGeocoder
  # Class Methods
  def self.included(base)
    base.send :attr_accessor, :turn_off_geocoder
    base.send :before_save, :set_location_info, if: Proc.new{ |f| f.address_changed? && !turn_off_geocoder }
  end

  private

  def set_location_info
    results = Geocoder.search(address)
    
    if results.present?
      self.latitude = results.first.coordinates[0]
      self.longitude = results.first.coordinates[1]
      self.postcode = results.first.postal_code
      self.suburb = results.first.city
      self.country = results.first.country
    end
  end
end