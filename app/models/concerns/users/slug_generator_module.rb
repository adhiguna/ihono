module Users::SlugGeneratorModule
  def self.included(base)
    base.send :extend, FriendlyId
    base.send :friendly_id, :slug_candidates, use: :slugged
  end

  # START for slug not too random ex :
  # latteria-mozzarella
  # latteria-mozzarella-2
  def slug_candidates
    [:username, :slug_sequence]
  end

  def slug_sequence
    select_as_slug = email
    normalize_friendly_id(select_as_slug)
  end

  def should_generate_new_friendly_id?
    username_changed? || email_changed?
  end
  # END for slug not too random
end