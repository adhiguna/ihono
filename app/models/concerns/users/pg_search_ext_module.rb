module Users::PgSearchExtModule
  def self.included(base)
    base.send :include, PgSearch::Model
    base.send :multisearchable, against: [:first_name, :last_name, :email, :abstract, :contact, :username, :set_lineages]
    base.send :pg_search_scope, :search_by_name, against: [:first_name, :last_name, :email, :abstract, :contact, :username], using: { trigram: { word_similarity: true } }, ignoring: :accents
  end

  def set_lineages
    lineages.map{ |f| [f.whanau, f.iwi.try(:name), f.hapu.try(:name)].reject(&:blank?) }.flatten.join(' ')
  end
end