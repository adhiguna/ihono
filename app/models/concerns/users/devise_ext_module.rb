module Users::DeviseExtModule
  def self.included(base)
    base.send :attr_writer, :login
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :two_factor_authenticatable
    base.send :devise, :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, authentication_keys: [:login]
    # turn off OTP
    # base.send :has_one_time_password
    # base.send :after_create, :send_new_otp

    base.instance_eval do
      def find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
          where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        elsif conditions.has_key?(:username) || conditions.has_key?(:email)
          where(conditions.to_h).first
        end
      end
    end
  end

  ##
  # allow user can login via username or password
  def login
    @login || self.username || self.email
  end
end