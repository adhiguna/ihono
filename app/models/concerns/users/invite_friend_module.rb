# module for accept and reject friend request
# flow -> 
# - user will send invite request friend using invite_friend
# - user can also uninvite request friend using uninvite_friend
# - friend can already accept_invite_friend or reject_invite_friend 
# - if friend accept then it will automatically add_friend (user_invitation_friendship line 45)
# - else friend will reject_invite_friend (user_invitation_friendship line 45)
# if they become friend they can unfriend by remove_friend. both of them can do it

module Users::InviteFriendModule
  def self.included(base)
    base.send :has_and_belongs_to_many, :friends,
                                        class_name: 'User',
                                        join_table: :users_friendships,
                                        foreign_key: :user_id,
                                        association_foreign_key: :user_friend_id,
                                        uniq: true
  end

  def invitation_friendships
    UserInvitationFriendship.with_friend(self)
  end

  # set user friend by object not by id
  def get_user_friend(user_friend)
    user_friend = User.find(user_friend) if user_friend.class != User
    return user_friend
  end

  ##
  # add friend
  def add_friend(user_friend)
    user_friend = get_user_friend(user_friend)
    return false if friends.include?(user_friend)

    friends << user_friend
    user_friend.friends << self
    true
  end

  ##
  # remove friend
  def remove_friend(user_friend)
    user_friend = get_user_friend(user_friend)
    return false unless friends.include?(user_friend)

    friends.delete(user_friend)
    user_friend.friends.delete(self)

    # deleting so user can re-invite in future
    invitation_friendships.accepted.with_friend(user_friend).take.try(:destroy)

    true
  end

  ##
  # checking if alreadt friend
  def already_friend?(user_friend)
    user_friend = get_user_friend(user_friend)

    friends.include?(user_friend)
  end

  def invite_friend(user_friend)
    user_friend = get_user_friend(user_friend)
    return false if already_has_invite_friend?(user_friend)

    UserInvitationFriendship.create(user: self, user_friend: user_friend, request_by: self)
  end

  def uninvite_friend(user_friend)
    user_friend = get_user_friend(user_friend)

    invitation_friendships.with_friend(user_friend).take.try(:destroy)
  end

  def accept_invite_friend(user_friend)
    invitation_friendships.with_friend(user_friend).take.try(:accept!)
  end

  def reject_invite_friend(user_friend)
    invitation_friendships.with_friend(user_friend).take.try(:reject!)
  end

  def get_invite_friend(user_friend)
    invitation_friendships.with_friend(user_friend).take
  end

  def already_has_invite_friend?(user_friend)
    user_friend = get_user_friend(user_friend)

    get_invite_friend(user_friend).present?
  end
end
