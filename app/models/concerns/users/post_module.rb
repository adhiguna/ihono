module Users::PostModule
  ##
  # favorite / unfavorite post
  # false mean unfavorite and true mean favorite
  def toggle_favorite_post!(post)
    post = Post.friendly.find(post) if post.class != Post

    if post_favorites.include?(post)
      post_favorites.delete(post)
      return false
    else
      post_favorites << post
      return true
    end
  end

  ##
  # checking favorite /unfavorite post
  def post_favorite?(post)
    post = Post.friendly.find(post) if post.class != Post

    return post_favorites.include?(post)
  end

  ##
  # favorite / unfavorite post
  # false mean unfavorite and true mean favorite
  def toggle_like_post!(post)
    post = Post.friendly.find(post) if post.class != Post

    if post_likes.include?(post)
      post_likes.delete(post)
      return false
    else
      post_likes << post
      return true
    end
  end

  ##
  # checking favorite /unfavorite post
  def post_like?(post)
    post = Post.friendly.find(post) if post.class != Post

    return post_likes.include?(post)
  end
end