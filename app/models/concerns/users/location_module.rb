module Users::LocationModule
  def self.included(base)
    base.send :before_save, :clear_latitude_longitude, unless: Proc.new { |attribute| attribute.visibility }
  end

  private

  def clear_latitude_longitude
    self.latitude = nil
    self.longitude = nil
  end
end