module Users::AvatarModule
  def self.included(base)
    base.send :include, AvatarUploader::Attachment.new(:avatar)
    base.send :attr_accessor, :avatar_crop_x, :avatar_crop_y, :avatar_crop_h, :avatar_crop_w
    base.send :before_validation, :set_cropping_avatar, if: Proc.new { |attribute| attribute.avatar.present? && attribute.avatar_crop_x.present? && attribute.avatar_crop_y.present? && attribute.avatar_crop_w.present? && attribute.avatar_crop_h.present? }
  end

  private
  
  def set_cropping_avatar
    data = JSON.parse(avatar_data)

    data["metadata"]["cropping"] = {
      'x' => avatar_crop_x,
      'y' => avatar_crop_y,
      'h' => avatar_crop_h,
      'w' => avatar_crop_w,
    }

    self.avatar = data.to_json
  end
end