module Users::InvitationSignupModule
  def self.included(base)
    base.send :attr_accessor, :user_invitation_signup_token
    base.send :after_create, :set_invitation_signup, if: Proc.new { |attribute| attribute.user_invitation_signup_token }
  end

  def set_invitation_signup
    user_invitation = UserInvitationSignup.search_by_no_user_invited.find_by_token(user_invitation_signup_token)
    user_invitation.update_column(:user_invited_id, self.id) if user_invitation.present?
  end
  
  # search and replace existing send invitations
  def send_invitations_attributes=(attributes)
    attributes = attributes.map do |key, value|
      data_exist = self.send_invitations.find_data_by_contact(value['email'], value['phone'])

      if data_exist.present?
        value['id'] = data_exist.id
        value['email'] = data_exist.email unless value['email']
        value['phone'] = data_exist.phone unless value['phone']
      end

      value
    end

    super(attributes)
  end
end