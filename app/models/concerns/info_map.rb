# frozen_string_literal: true

module InfoMap
  # Class Methods
  def self.included(base)
    base.send :acts_as_geolocated

    base.instance_eval do 
      def get_base_data
        if self == User
          can_found.where.not(latitude: nil).where.not(longitude: nil)
        elsif self == Organisation
          joins(:locations).where.not({ locations: { latitude: nil } }).where.not({ locations: { longitude: nil } })
        else
          where.not(latitude: nil).where.not(longitude: nil)
        end
      end

      def extract_data(latitude = nil, longitude = nil, distance = nil)
        data = get_base_data

        # distance 51 mean is more than 50 km. we gonna show all
        if(latitude.present? && longitude.present? && distance.present? && distance.to_i != 51)
          data = data.within_radius(distance.to_i * 1000, latitude.to_f, longitude.to_f)
        end

        return data
      end
    end
  end
end
