# frozen_string_literal: true

module InfoData 
  def self.included(base)
    base.send :extend, FriendlyId
    base.send :friendly_id, :name, use: :slugged
    base.send :include, PgSearch::Model
    base.send :multisearchable, against: [:name, :abstract, :contact]
    base.send :pg_search_scope, :search_by_name, against: :name, using: { trigram: { word_similarity: true } }, ignoring: :accents
    base.send :after_touch, :update_pg_search_document

    base.instance_eval do
      def search_filters(params: {})
        data = all
  
        if params.present?
          data = search_by_name(params[:keyword]) if params[:keyword].present?
        end
  
        data
      end
    end
  end
  
  # Instance Methods
  def short_abstract
    abstract.try(:first, 200) || ''
  end
end
