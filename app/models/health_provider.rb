class HealthProvider < ApplicationRecord
  include InfoData
  include InfoMap
  include LocationGeocoder
  acts_as_geolocated
  audited

  SERVICE_LIST = ['Covid-19 testing only', 'Covid-19 testing & other health checks']
  REGION_LIST = [
                  'Northland', 'Auckland', 'Waikato', 'Bay of Plenty', 'Gisborne', 'Otago', 'Southland',
                  'Hawke\'s Bay', 'Taranaki', 'Manawatū-Whanganui', 'Wellington', 
                  'Tasman', 'Nelson', 'Marlborough', 'West Coast', 'Canterbury'
                ].uniq.freeze.sort

  multisearchable against: [:name, :contact, :suburb, :postcode, :region, :service]

  validates :name, presence: true
  validates :address, presence: true
  validates :abstract, presence: true
  validates :region, presence: true, inclusion: { in: HealthProvider::REGION_LIST, message: '%{value} is not a valid region' }
  validates :service, presence: true
  validates :open, presence: true
  validates :close, presence: true
  validates :contact, presence: true
  validate :check_valid_open_close_time

  has_many :appointments, dependent: :nullify

  accepts_nested_attributes_for :appointments, reject_if: :all_blank, allow_destroy: true

  scope :search_by_service, -> (service) { where(service: service) }

  def self.extract_data_and_filters(latitude = nil, longitude = nil, service = nil, distance = nil)
    data = extract_data(latitude, longitude, distance)

    data = data.search_by_service(service) if service.present?

    data
  end

  private

  def check_valid_open_close_time
    if open > close
      self.errors.add(:open, 'must be more than close time')
    end
  end
end

# == Schema Information
#
# Table name: health_providers
#
#  id                :bigint           not null, primary key
#  abstract          :text
#  address           :text
#  allow_appointment :boolean          default(TRUE), not null
#  close             :time
#  contact           :text
#  country           :string
#  latitude          :float
#  longitude         :float
#  name              :string           not null
#  open              :time
#  postcode          :string
#  region            :string
#  service           :text
#  slug              :string
#  suburb            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
