class SmsReceipt < ApplicationRecord
  validates :message_id, presence: true
  validate :match_api_key

  private

  def match_api_key
    self.errors.add(:api_key, 'API key invalid') unless ENV['NEXMO_ACCOUNT_SID'] == api_key
  end
end

# == Schema Information
#
# Table name: sms_receipts
#
#  id                :bigint           not null, primary key
#  api_key           :string
#  error_code        :string
#  message_timestamp :string
#  msisdn            :string
#  network_code      :string
#  price             :string
#  scts              :string
#  status            :string
#  to                :string
#  message_id        :string           not null
#
