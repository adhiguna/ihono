class Appointment < ApplicationRecord
  audited
  include PgSearch::Model
  include Appointments::FilterModule
  include Appointments::SortModule

  multisearchable against: [:name, :address, :email, :cell_phone, :home_phone] 

  validates :name, presence: true
  validates :appointment_date, presence: true
  validates :appointment_time, presence: true
  validates :address, presence: true
  validates :feels, presence: true
  validates :pre_exist_health_condition, presence: true
  validate :checking_contact
  
  belongs_to :health_provider
  belongs_to :user, optional: true

  before_validation :parse_input
  after_create :create_notification_worker
  
  private

  def create_notification_worker
    SendCreateAppointmentNotificationWorker.perform_in(5, self.id)
  end

  def checking_contact
    self.errors.add(:base, 'Must be input one of contact (email, cell phone, OR home phone)') if email.blank? && cell_phone.blank? && home_phone.blank?
  end

  def parse_input
    self.feels = feels.reject(&:blank?)
    self.pre_exist_health_condition = pre_exist_health_condition.reject(&:blank?)
  end
end

# == Schema Information
#
# Table name: appointments
#
#  id                              :bigint           not null, primary key
#  address                         :text
#  age                             :string
#  appointment_date                :date             not null
#  appointment_time                :time             not null
#  cell_phone                      :string
#  contact_with_corona             :boolean          default(FALSE), not null
#  contact_with_overseas_traveller :boolean          default(FALSE), not null
#  email                           :string
#  essential_services_worker       :boolean          default(FALSE), not null
#  feels                           :string           default([]), not null, is an Array
#  health_care_worker              :boolean          default(FALSE), not null
#  home_phone                      :string
#  name                            :string           not null
#  pre_exist_health_condition      :string           default([]), not null, is an Array
#  travel_overseas                 :boolean          default(FALSE), not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  health_provider_id              :bigint
#  user_id                         :bigint
#
# Indexes
#
#  index_appointments_on_health_provider_id  (health_provider_id)
#  index_appointments_on_user_id             (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (health_provider_id => health_providers.id)
#  fk_rails_...  (user_id => users.id)
#
