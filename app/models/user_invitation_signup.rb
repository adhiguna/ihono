class UserInvitationSignup < ApplicationRecord
  belongs_to :owner_invited, primary_key: :id, foreign_key: :user_id, class_name: 'User'
  belongs_to :user_invited, primary_key: :id, foreign_key: :user_invited_id, class_name: 'User', optional: true

  validates :name, presence: true
  validate :checking_contact

  after_save :sending_invitation_to_contact
  before_save :generate_token, if: Proc.new { |attribute| attribute.email_changed? || attribute.phone_changed? }

  scope :search_by_no_user_invited, -> { where(user_invited_id: nil) }

  def self.find_data_by_contact(data_email, data_phone)
    if data_email.present? && data_phone.present?
      where('user_invitation_signups.email = :email OR user_invitation_signups.phone = :phone', email: data_email, phone: data_phone).take
    elsif data_email.present?
      where('user_invitation_signups.email = :email', email: data_email).take
    elsif data_phone.present?
      where('user_invitation_signups.phone = :phone', phone: data_phone).take
    end
  end

  private
  def checking_contact
    self.errors.add(:base, 'Must be input one of contact (email or phone)') if email.blank? && phone.blank?
  end

  def sending_invitation_to_contact
    SendUserInvitationSignupWorker.perform_async(id)
  end

  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(token: random_token)
    end
  end
end

# == Schema Information
#
# Table name: user_invitation_signups
#
#  id              :bigint           not null, primary key
#  email           :string
#  name            :string
#  phone           :string
#  token           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :bigint
#  user_invited_id :bigint
#
# Indexes
#
#  index_user_invitation_signups_on_user_id          (user_id)
#  index_user_invitation_signups_on_user_invited_id  (user_invited_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_invited_id => users.id)
#
