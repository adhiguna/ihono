# frozen_string_literal: true

class Post < ApplicationRecord
  extend FriendlyId
  include PgSearch::Model
  include HeroImageUploader::Attachment.new(:hero_image)

  TYPE_LIST = {
    'public' => ['News', 'Events', 'Posts'],
    'covid-19' => ['Official government updates', 'Medical Service updates', 'Whānau help and advice']
  }

  multisearchable against: [:title, :content, :author_name]
  friendly_id :title, use: :slugged

  acts_as_taggable_on :tag

  has_and_belongs_to_many :user_favorites, :join_table => :users_favorite_posts, class_name: 'User'
  has_and_belongs_to_many :user_likes, :join_table => :users_like_posts, class_name: 'User'
  has_many :notifications, as: :notifiable
  belongs_to :user, :touch => true

  validates :title, presence: true
  validate :check_user_role

  scope :public_post, -> { where(post_type: Post::TYPE_LIST['public']) }
  scope :corona_post, -> { where(post_type: Post::TYPE_LIST['covid-19']) }
  scope :search_by_post_type, -> (post_type) { where(post_type: post_type) }
  scope :search_by_region, -> (region) { where(region_id: region) }

  def author_name
    user.full_name
  end

  def corona_post?
    Post::TYPE_LIST['covid-19'].include? self.post_type
  end

  def self.search_filters(filters = {})
    data = where(false)

    if filters.present?
      data = data.search_by_post_type(filters[:category]) if filters[:category]
      data = data.search_by_region(filters[:region]) if filters[:region]
    end

    data
  end

  def self.search_order(order = 'newest')
    data = where(false)

    ordering_by = case order
                  when 'newest'
                    'created_at DESC'
                  when 'oldest'
                    'created_at ASC'
                  else
                    'created_at DESC'
                  end

    data.order(ordering_by)
  end

  private

  def check_user_role
    return if user.super_admin? || user.health_admin?

    self.errors.add(:post_type, 'Post type must be one of the public') if user.user? && !Post::TYPE_LIST['public'].include?(post_type)
  end
end

# == Schema Information
#
# Table name: posts
#
#  id                  :bigint           not null, primary key
#  content             :text             not null
#  hero_image_data     :text
#  hero_image_filename :string
#  post_type           :string           not null
#  slug                :string           not null
#  title               :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  region_id           :bigint
#  user_id             :bigint
#
# Indexes
#
#  index_posts_on_user_id  (user_id)
#
