# frozen_string_literal: true

class Marae < ApplicationRecord
  include InfoMap
  include InfoData
  include LocationGeocoder

  has_and_belongs_to_many :hapus, :join_table => :hapus_maraes
  has_and_belongs_to_many :iwis, :join_table => :iwis_maraes
  has_and_belongs_to_many :rohes, :join_table => :rohes_maraes
  has_and_belongs_to_many :wharenuis, :join_table => :maraes_wharenuis
  has_many :regions, through: :rohes

  multisearchable against: [:name, :contact, :suburb, :postcode]

  validates :name, presence: true
end

# == Schema Information
#
# Table name: maraes
#
#  id        :bigint           not null, primary key
#  abstract  :text
#  address   :text
#  contact   :text
#  country   :string
#  latitude  :float
#  longitude :float
#  name      :string           not null
#  postcode  :string
#  slug      :string           not null
#  suburb    :string
#
