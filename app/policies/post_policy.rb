class PostPolicy < ApplicationPolicy
  def update?
    is_owner? || user.super_admin?
  end

  def destroy?
    is_owner? || user.super_admin?
  end

  private
  def is_owner?
    record.user_id == user.id
  end
end