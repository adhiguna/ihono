class HealthProviderPolicy < ApplicationPolicy
  def can_booking_appointment?
    record.allow_appointment?
  end
end