class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def health_admin?
    record.health_admin?
  end

  def super_admin?
    record.super_admin?
  end
end
