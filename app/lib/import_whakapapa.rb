class ImportWhakapapa
  def self.start(doc_name = 'Te Tai Tokerau')
    spreadsheet = Roo::Spreadsheet.open(Rails.root.join("doc", "ihono_data.xlsx").to_s)
    spreadsheet.default_sheet = doc_name
    header = spreadsheet.row(2)
    (3..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      regions = []
      rohes = []
      iwis = []
      hapus = []
      maraes = []
      wharenuis = []

      regions = doc_name.split('/').map do |region|
        region = Region.find_or_create_by(name: region.strip)
  
        region
      end
      
      if row['Rohe'].present?
        rohes = row['Rohe'].split('/').map do |rohe|
          puts "--------- #{rohe} ---------"
          data_rohe = Rohe.find_or_create_by(name: rohe.strip)

          regions.uniq.each do |region|
            data_rohe.regions << region if region.present? && !data_rohe.regions.exists?(region.try(:id))
          end

          data_rohe
        end
      end

      if row['Iwi'].present?
        iwis = row['Iwi'].split('/').map do |iwi|
          puts "--------- #{iwi} ---------"
          data_iwi = Iwi.find_or_create_by(name: iwi.strip)
          
          rohes.uniq.each do |rohe|
            data_iwi.rohes << rohe if rohe.present? && !data_iwi.rohes.exists?(rohe.try(:id))
          end

          data_iwi
        end
      end

      if row['Hapū'].present?
        hapus = row['Hapū'].split('/').map do |hapu|
          puts "--------- #{hapu} ---------"
          data_hapu = Hapu.find_or_create_by(name: hapu.strip)

          iwis.uniq.each do |iwi|
            data_hapu.iwis << iwi if iwi.present? && !data_hapu.iwis.exists?(iwi.try(:id))
          end

          rohes.uniq.each do |rohe|
            data_hapu.rohes << rohe if rohe.present? && !data_hapu.rohes.exists?(rohe.try(:id))
          end

          data_hapu
        end
      end

      if row['Marae'].present?
        maraes = row['Marae'].split('/').map do |marae|
          puts "--------- #{marae} ---------"
          data_marae = Marae.find_or_create_by(name: marae.strip)

          if row['Location'].present? && data_marae.address != row['Location']
            data_marae.update(address: row['Location'])
            sleep(2)
          end
          
          hapus.uniq.each do |hapu|
            data_marae.hapus << hapu if hapu.present? && !data_marae.hapus.exists?(hapu.try(:id))
          end

          iwis.uniq.each do |iwi|
            data_marae.iwis << iwi if iwi.present? && !data_marae.iwis.exists?(iwi.try(:id))
          end

          rohes.uniq.each do |rohe|
            data_marae.rohes << rohe if rohe.present? && !data_marae.rohes.exists?(rohe.try(:id))
          end

          data_marae
        end
      end

      if row['Wharenui'].present?
        wharenuis = row['Wharenui'].split('/').map do |wharenui|
          puts "--------- #{wharenui} ---------"
          data_wharenui = Wharenui.find_or_create_by(name: wharenui.strip)

          maraes.uniq.each do |marae|
            data_wharenui.maraes << marae if marae.present? && !data_wharenui.maraes.exists?(marae.try(:id))
          end

          hapus.uniq.each do |hapu|
            data_wharenui.hapus << hapu if hapu.present? && !data_wharenui.hapus.exists?(hapu.try(:id))
          end

          iwis.uniq.each do |iwi|
            data_wharenui.iwis << iwi if iwi.present? && !data_wharenui.iwis.exists?(iwi.try(:id))
          end

          rohes.uniq.each do |rohe|
            data_wharenui.rohes << rohe if rohe.present? && !data_wharenui.rohes.exists?(rohe.try(:id))
          end

          data_wharenui
        end
      end
    end
  end
end