require 'uri'

class ShortUrl
  attr_accessor :url

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def initialize(url)
    @url = URI::encode(url)
  end

  def call
    generate
  end

  def generate
    client = Bitly::API::Client.new(token: ENV['BITLY_TOKEN'])
    bitlink = client.shorten(long_url: url)

    return bitlink.link
  end
end

