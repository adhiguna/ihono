require 'nexmo'
require 'phonelib'

class Message::Sms
  attr_accessor :to_phone_number, :message

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def initialize(to_phone_number, message)
    @to_phone_number = parse_phone_number(to_phone_number)
    @message = message
  end

  def call
    sms_receipt = send_sms
    create_sms_receipt(sms_receipt)
  end

  def send_sms
    client = Nexmo::Client.new(
      api_key: ENV['NEXMO_ACCOUNT_SID'],
      api_secret: ENV['NEXMO_AUTH_TOKEN']
    )

    response = client.sms.send(
      from: ENV['NEXMO_ID'],
      to: to_phone_number,
      text: message
    )

    message_response =  response.messages.first

    raise message_response['error-text'] if message_response['error-text'].present?

    return message_response
  end

  def parse_phone_number(number)
    Phonelib.parse(number).international(false)
  end

  def create_sms_receipt(sms_receipt)
    SmsReceipt.create(msisdn: sms_receipt['to'], message_id: sms_receipt['message_id'], api_key: ENV['NEXMO_ACCOUNT_SID'])
  end
end

