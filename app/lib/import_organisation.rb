class ImportOrganisation
  def self.start(doc_name = 'Te Tai Tokerau')
    spreadsheet = Roo::Spreadsheet.open(Rails.root.join("doc", "ihono_organisation.xlsx").to_s)
    spreadsheet.default_sheet = doc_name
    header = spreadsheet.row(2)
    (3..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      regions = []
      rohes = []
      iwis = []

      if row['Organisation'].present?
        puts '----------------'
        puts row['Organisation']
        organisation = Organisation.find_or_create_by(name: row['Organisation'], organisation_type: row['Organisation type'])
        organisation.save
 
        puts organisation.errors.full_messages if organisation.errors.present?

        if row['Address'].present?
          address = row['Address'] + ', New Zealand'
          location = organisation.locations.find_or_create_by(address: address)

          if location.present?
            # prefill location contact
            location.contact = "" unless location.contact.present?

            if row['Website'].present?
              location.contact += "#{row['Website']}\n" unless location.contact.include?(row['Website'])
            end

            if row['Email'].present?
              location.contact += "#{row['Email']}\n" unless location.contact.include?(row['Email'])
            end

            location.save
            sleep(2)
          end
        end

        if row['Rohe'].present?
          regions = row['Rohe'].split('/').map do |data_region|
            region = Region.find_by_name(data_region.strip)
      
            region
          end

          regions.uniq.reject(&:blank?).each do |region|
            organisation.regions << region if region.present? && !organisation.regions.exists?(region.id)
          end

          rohes = row['Rohe'].split('/').map do |data_rohe|
            rohe = Rohe.find_by_name!(data_rohe.strip)
      
            rohe
          end

          rohes.uniq.reject(&:blank?).each do |rohe|
            organisation.rohes << rohe if rohe.present? && !organisation.rohes.exists?(rohe.id)
          end
        end

        if row['Iwi'].present? && row['Iwi'] != 'https://whanauora.nz/partners/'
          iwis = row['Iwi'].split('/').map do |data_iwi|
            iwi = Iwi.find_by_name!(data_iwi.strip)
      
            iwi
          end

          iwis.uniq.reject(&:blank?).each do |iwi|
            organisation.iwis << iwi if iwi.present? && !organisation.iwis.exists?(iwi.id)
          end
        end
      end
    end
  end
end