// library
import $ from 'jquery'
global.$ = jQuery
const Rails = require('rails-ujs')
import Turbolinks from 'turbolinks'
import _ from 'lodash'
import 'bootstrap'
import 'parsleyjs'
import 'cocoon-js'

// setup library
import '../js/vendor/toastr'
import '../js/vendor/pwa'

// shared
import '../js/shared/select2_module'
import '../js/shared/quill_module'
import '../js/shared/map_module' 
import '../js/shared/search_map_module'
import '../js/shared/map_show_module'
import '../js/shared/search_group_module'
import '../js/shared/share_button_module'
import '../js/shared/toggle_menu_module'
import '../js/shared/cropper_module'
import '../js/shared/show_hide_page_search_module'
import '../js/shared/whakapapa_list_module'
import '../js/shared/post_infinite_scroll_module'
import '../js/shared/detect_location_module'
import '../js/shared/datepicker_module'
import '../js/shared/timepicker_module'
import '../js/shared/daterangepicker_module'
import '../js/shared/input_module'
import '../js/shared/button_filter_module'
import '../js/shared/parsley_custom_module'
import '../js/shared/sort_filter_module'

// view render
import '../js/application/profile'
import '../js/application/post'
import '../js/application/corona/health_providers'

// styling
import '../styles/application'

Rails.start()
Turbolinks.start()


