window.Parsley
  .addValidator('groupRequired', {
    requirementType: 'string',
    validate: function(_value, requirement, instance) {
      let returnNow = true;
      let getInputsValue = $(instance.element).find('input').map(function(index, el) {
        if (el.value != '' && $(el).parsley().isValid()) {
          return true
        } else {
          return false
        }
      });
      
      return _.some(getInputsValue);
    },
    messages: {
      en: 'Please input one of the field',
    }
  });



$( document ).on('turbolinks:load', function() {
  $('[data-parsley-validate]').parsley({
    inputs: Parsley.options.inputs + ',[data-parsley-group-required]'
  });
})