import 'daterangepicker';

$(document).on('turbolinks:load', function() {
  $('[data-date-range-picker]').each(function() {
    let options = {
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    }
    $(this).daterangepicker(options);

    $(this).on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $(this).on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $(this).on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  });
});