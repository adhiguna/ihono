function loadMap () {
  if ($('#map').length > 0 && $('#googlemaps_api').length == 0) {
    var head = document.getElementsByTagName('head')[0];
    var googleMapId = $('#map').data('google-map-id');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.id = 'googlemaps_api';
    script.src = 'https://maps.googleapis.com/maps/api/js?key='+googleMapId+'&callback=initMap&libraries=places';
    head.appendChild(script);
  }
}

function initMap() {
  if ($('#map').length > 0) {
    document.getElementById('pac-input').onkeypress = function(e){
      if (!e) e = window.event;
      var keyCode = e.keyCode || e.which;
      if (keyCode == '13'){
        return false;
      }
    }

    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -41.743449, lng: 172.901472},
      zoom: 5,
      mapTypeId: 'roadmap'
    });
  
    var icon = {
      url: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25)
    };
  
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  
    var markers = [];
    var target_input = $('#map').data('target')
    var address = document.getElementById(target_input+'_address');
    var postcode = document.getElementById(target_input+'_postcode');
    var suburb = document.getElementById(target_input+'_suburb');
    var city = document.getElementById(target_input+'_city');
    var latitude = document.getElementById(target_input+'_latitude');
    var longitude = document.getElementById(target_input+'_longitude');
    var region = document.getElementById(target_input+'_region');
    var country = document.getElementById(target_input+'_country');
  
    // set default map if already present latitude longitude
    if(!!latitude.value && !!longitude.value) {
      addMarker({ lat: parseFloat(latitude.value), lng: parseFloat(longitude.value) });
    }
  
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
  
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
  
      if (places.length == 0) {
        return;
      }
  
      // For each place, get the icon, name and location.
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        setAddress(place);
        addMarker(place.geometry.location);
      });
    });
  
    function addMarker(latLang) {
      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      
      markers = [];
  
      var bounds = new google.maps.LatLngBounds();
      bounds.extend(latLang);
  
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        position: latLang,
      }));
  
      map.fitBounds(bounds);
    }
  
    function setAddress(place) {
      if (address) address.value = parsePlace(place, 'address');
      if (suburb) suburb.value = parsePlace(place, 'suburb');
      if (city) city.value = parsePlace(place, 'city');
      if (postcode) postcode.value = parsePlace(place, 'postcode');
      if (latitude) latitude.value = parsePlace(place, 'latitude');
      if (longitude) longitude.value = parsePlace(place, 'longitude');
      if (region) region.value = parsePlace(place, 'region');
      if (country) country.value = parsePlace(place, 'country');
    }
  
    function parsePlace(place, type) {
      console.log(place)
      var result = '';
      switch(type) {
        case 'address':
          result = place.formatted_address;
          break;
        case 'city':
          result = extractAddressComponents(place.address_components, 'locality');
          break;
        case 'postcode':
          result = extractAddressComponents(place.address_components, 'postal_code');
          break;
        case 'suburb':
          result = extractAddressComponents(place.address_components, 'sublocality');
          break;
        case 'region':
          result = extractAddressComponents(place.address_components, 'administrative_area_level_1');
          break;
        case 'country':
          result = extractAddressComponents(place.address_components, 'country');
          break;
        case 'latitude':
          result = place.geometry.location.lat();
          break;
        case 'longitude':
          result = place.geometry.location.lng();
          break;
      }
  
      return result;
    }
  
    function extractAddressComponents(address_components, text_data) {
      var result = address_components.find(function(element) { return element.types.includes(text_data); });
  
      if(result !== undefined) {
        return result.long_name;
      } else {
        return '';
      }
    }
  }
}

window.initMap = initMap;

$( document ).on('turbolinks:load', function() {
  loadMap()
})
