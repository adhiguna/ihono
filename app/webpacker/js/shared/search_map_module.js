import MarkerCluster from '@google/markerclusterer';
import markerConstant from './marker_constant';
import './ip_look_up_module';

function loadSearchMap () {
  if ($('#search-map').length > 0) {
    var head = document.getElementsByTagName('head')[0];
    var googleMapId = $('#search-map').data('google-map-id');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key='+googleMapId+'&callback=initSearchMap';
    head.appendChild(script);

    $('[data-target="#filter-modal"]').on('click', function (e) {
      if (e.currentTarget.getAttribute('data-type') == 'sort' && ($('#search_map_latitude').val().length == 0 || $('#search_map_longitude').val().length == 0)) {
        buildErrorDisabledLocation();
      } else {
        $('#filter-modal').modal('show');
      }
    });
  }
}

var map, fixLatitude, fixLongitude;
var markers = [];
var _markerCluster;

function initSearchMap() {
  if ($('#search-map').length > 0) {
    setFixLocation()

    map = new google.maps.Map(document.getElementById('search-map'), {
      center: {lat: (fixLatitude || -41.743449), lng: (fixLongitude || 172.901472)},
      zoom: ((fixLatitude != undefined && fixLongitude != undefined) ? 16 : 5),
      mapTypeId: 'roadmap',
      clickableIcons: false
    });

    _markerCluster = new MarkerCluster(map, [], { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' } );

    startMarker();
    askingLocation();
  }
}

function setFixLocation () {
  if ($('#fix-latitude').length > 0 && $('#fix-latitude').val() != '') {
    fixLatitude = parseFloat($('#fix-latitude').val());
  }

  if ($('#fix-longitude').length > 0 && $('#fix-longitude').val() != '') {
    fixLongitude = parseFloat($('#fix-longitude').val());
  }
}

function clearMarker() {
  // Clear out the old markers.
  markers.forEach(function(marker) {
    marker.setMap(null);
  });

  markers = [];
  _markerCluster.clearMarkers();
}

function startMarker () {
  loadMarker(init_array_data)
}

function loadMarker (array_data) {
  clearMarker()

  $.each(array_data, function (index, data) {
    addMarker(index, data)
  });

}

function addMarker(index, data) {
  var type = data['type'];
  var type_name = data['type_name'];
  var icon_symbol = data['icon_symbol'];
  var latitude, longitude;

  if (type == 'user') {
    latitude = markerConstant.hideActualMapLoc(data['latitude']) ;
    longitude = markerConstant.hideActualMapLoc(data['longitude']);
  } else {
    latitude = data['latitude'];
    longitude = data['longitude'];
  }

  var latLang = { lat: parseFloat(latitude), lng: parseFloat(longitude) };

  var icon = {
    url: markerConstant.setIconMarker(icon_symbol),
    size: new google.maps.Size(25, 25),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34), 
    scaledSize: new google.maps.Size(25, 25)
  };
  
  var marker = new google.maps.Marker({
    map: map,
    icon: icon,
    position: latLang
  })

  marker.addListener('click', function() {
    var text = "<h3><a href='"+data['link']+"'>"+data['name']+"</a></h3>";

    text += "<p class='description'>"+data['short_abstract']+"</p>";
    text += "<p class='icon'><i class='ihono-icon-"+icon_symbol+"'></i> "+type_name+"</p>";
    $('#modal-info-map .body').html(text);
    $('#modal-info-map').show();
  });

  if (type == 'user') {
    var circle = new google.maps.Circle(markerConstant.circleMapsOption);

    circle.bindTo('center', marker, 'position');
    circle.bindTo('map', marker, 'map');
  }
  // Create a marker for each place.
  markers.push(marker);
  _markerCluster.addMarker(marker);

  // autoclick when get same coordinate
  if (latLang.lat == fixLatitude && latLang.lng == fixLongitude) {
    new google.maps.event.trigger( marker, 'click' );
  }
}

function askingLocation () {
  var not_support_location_banner = $('#not-support-location');
  var geoOptions = { 
    maximumAge: 5 * 60 * 1000,
    timeout: 10 * 1000
  }

  var geoSuccess = function(position) {
    
    $('#search_map_latitude').val(position.coords.latitude)
    $('#search_map_longitude').val(position.coords.longitude)

    console.log(position.coords.latitude +' - '+ position.coords.longitude)

    if ((fixLatitude == undefined || fixLatitude == '') && (fixLongitude == undefined || fixLongitude == '')) {
      var bounds = new google.maps.LatLngBounds();
      bounds.extend({ lat: position.coords.latitude, lng: position.coords.longitude });
      map.fitBounds(bounds);

      var listener = google.maps.event.addListener(map, "idle", function() { 
        if (map.getZoom() > 16) map.setZoom(16); 
        google.maps.event.removeListener(listener); 
      });
    }
  };

  var geoError = async function(error_code) {
    let getIpLocation = await ipLookUp();
    if (getIpLocation) {
      geoSuccess(getIpLocation);
    } else {
      buildErrorDisabledLocation();
    }
  };

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(geoSuccess, function(err) { geoError(err.code) }, geoOptions);
  }
  else {
    not_support_location_banner.show()
  }
}

function buildErrorDisabledLocation () {
  var html = `<div class='modal fade' id='modal-disabled' tabindex='-1 role='dialog'>
                <div style='margin-top: 20%;' class='modal-dialog' role='document'>
                  <div class='modal-content'>
                    <div class='modal-body' style='text-align: center;'>
                      <p>Looked like your web browser are disabled for getting your location to doing sort function. Please read <a href='https://www.wikihow.com/Enable-Location-Services-on-Google-Chrome' target='blank'>this article for enabled it</a> and refresh your web browser</p>
                      <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                  </div>
                </div>
              </div>`;
  $('body').append(html);
  $('#modal-disabled')
    .on('hidden.bs.modal', function (e) {
      $('#modal-disabled').remove();
    })
    .modal('show');
}

function onClickButtonFilter () {
  if ($('[data-target="#filter-modal"]').length > 0) {
    $('[data-target="#filter-modal"]').on('click', function () {
      $('.filter-selection').hide();
      $('#filters-' + $(this).data('type')).show()
    });
  }
}

function onClickCloseInfoMapButton () {
  $('#modal-info-map .close').click(function() {
    $('#modal-info-map').hide();
  });
}

window.initSearchMap = initSearchMap;
window.loadMarker = loadMarker;

$( document ).on('turbolinks:load', function() {
  loadSearchMap()
  onClickButtonFilter()
  onClickCloseInfoMapButton()
})