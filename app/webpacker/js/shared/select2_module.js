import 'select2/dist/js/select2.full'
import '../../js/vendor/select2.multi-checkboxes'

function initSelect2Tagging() {
  $('.select2-tagging').each(function () {
    $(this).select2({
      theme: 'bootstrap4',
      tags: $(this).data('tag'),
      placeholder: $(this).attr('placeholder')
    })
  });
}

function initSelect2() {
  $('[data-select-2-input]').each(function () {
    $(this).select2({
      theme: 'bootstrap4',
    })
  });
}

function select2CheckboxInput () {
  $('[data-select-2-checkbox]').each(function () {

    $(this).select2MultiCheckboxes({
      theme: 'bootstrap4',    
      templateSelection: function(selected, total) {
        selected = _.reject(selected, _.isEmpty);
        total = total - 1;

        if (selected.length == 0) {
          return "All";
        } else {
          return "Selected " + selected.length + " of " + total;
        }
        
      }
    })
  });
}

$( document ).on('turbolinks:load', function() {
  initSelect2Tagging();
  select2CheckboxInput();
  initSelect2();
})