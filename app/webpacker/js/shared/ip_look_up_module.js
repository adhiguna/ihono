function ipLookUp () {
  return new Promise(resolve => {
    let position = { coords: { latitude: null, longitude: null } }
    let getIP = document.getElementsByTagName('body')[0].getAttribute('data-ip-api')
    $.ajax('https://pro.ip-api.com/json?key='+getIP)
    .then(
        function success(response) {
          position.coords.latitude = response.lat
          position.coords.longitude = response.lon

          resolve(position)
        },

        function fail(data, status) {
          console.log('ip lookup failed', status);

          resolve(false);
        }
    );
  });
}

window.ipLookUp = ipLookUp;