$(document).on('click', '[data-toggle="offcanvas"], #navToggle', function (e) {
  e.preventDefault();
  $('.offcanvas-collapse').toggleClass('open')
});

$( document ).on('turbolinks:load', function() {
  if ($('.bg-cover').length > 0) {
    var bgCover = [1, 2, 3, 4, 5, 6];
    $('.bg-cover').css('background-image', 'url(https://ihono-assets-staging.manawa.nz/home-background/home-'+bgCover[Math.floor(Math.random() * bgCover.length)]+'.jpg)');
  }
})