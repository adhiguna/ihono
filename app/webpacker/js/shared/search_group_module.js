function initSearch() {
  if ($('#search-group-wrapper').length > 0) {
    var typingTimer;                //timer identifier
    var doneTypingInterval = 500;  //time in ms, 5 second for example
    var $input = $('#search-input');
    var $reset = $('[type="reset"]');
    var $results = $('#search-result-wrapper');

    $reset.on('click', function () {
      $input.val('')
      $results.html('')
      $input.focus()
    });

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
      $.ajax({
        url: "/search",
        dataType: 'json',
        data: {
          search_group: {
            keyword: $input.val()
          }
        },
        success:function(results) {
          $results.html('')
          $.each(results, function(key, value) {
            $results.append(wrapperText(value));
          });
        }
      });
    }

    function wrapperText (value) {
      text = "<div class='search-result-item'>"
      text += "<div class='media'>"
      text += "<div class='result-icon'><i class='"+value['icon']+" mr-3'></i></div>"
      text += "<div class='media-body'><h6>"+value['group_type']+" ("+value['size']+" results)</h6><a href='"+value['link']+"' class='text-decoration-none see-all'> SEE ALL RESULTS »</a></div>"
      text += "</div></div>"

      return text;
    }
  }
}

$( document ).on('turbolinks:load', function() {
  initSearch();
})