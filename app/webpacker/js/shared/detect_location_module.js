import Cookies from 'js-cookie'

function detectLocation () {
  if ($('#toogle-current-location').length > 0) {
    var refresh_interval = 60 * 1000;
    var geoOptions = { 
      maximumAge: 5 * 60 * 1000,
      timeout: 10 * 1000
    }

    function loopCheckLocation (force) {
      if (force == undefined) { force = false }
      if (getUserGeolocation() || force) {
        navigator.geolocation.getCurrentPosition(success, function(err) { error(err.code) }, geoOptions);
      }
    }

    function toogleMapMarker(active = false) {
      if (active == undefined) { active = false }

      if (active) {
        $('#toogle-no-current-location-marker').hide()
      } else {
        $('#toogle-no-current-location-marker').show()
      }
    }

    function setUserGeolocation(active = true) {
      if (active == undefined) { active = true }
      if (active) {
        Cookies.set('userGeolocation', true);
      } else {
        Cookies.remove('userGeolocation')
      }
    }

    function getUserGeolocation () {
      return Cookies.get('userGeolocation') || false;
    }

    function setCookiePosition (latitude, longitude) {
      Cookies.set('userGeolocationLatitude', latitude);
      Cookies.set('userGeolocationLongitude', longitude);
    }

    function getCookiePosition () {
      return {
        latitude: parseFloat(Cookies.get('userGeolocationLatitude')),
        longitude: parseFloat(Cookies.get('userGeolocationLongitude'))
      }
    }

    function success (position) {
      setUserGeolocation(true);
      toogleMapMarker(true);

      if (position.coords.latitude != getCookiePosition().latitude || position.coords.longitude != getCookiePosition().longitude) {
        setCookiePosition(position.coords.latitude, position.coords.longitude);
        requestToServer(true);
      }
    }

    // err 
    // 1 = permission blocked
    // 9 = user turnoff the locationable
    async function error (err) {
      if (err == 9) {
        requestToServer(false);
        setCookiePosition(null, null);
        setUserGeolocation(false);
        toogleMapMarker();
      } else {
        let getIpLocation = await ipLookUp();

        if (getIpLocation) {
          success(getIpLocation);
        } else {
          buildErrorDisabledLocation();
          setUserGeolocation(false);
          toogleMapMarker();
        }
      }
    }

    function buildErrorDisabledLocation () {
      var html = `<div class='modal fade' id='modal-disabled' tabindex='-1 role='dialog'>
                    <div style='margin-top: 20%;' class='modal-dialog' role='document'>
                      <div class='modal-content'>
                        <div class='modal-body' style='text-align: center;'>
                          <p>Looked like your web browser are disabled for getting your location. Please read <a href='https://www.wikihow.com/Enable-Location-Services-on-Google-Chrome' target='blank'>this article for enabled it</a> and refresh your web browser</p>
                          <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>`;
      $('body').append(html);
      $('#modal-disabled')
        .on('hidden.bs.modal', function (e) {
          $('#modal-disabled').remove();
        })
        .modal('show');
    }

    function requestToServer (visibility = true) {
      if (visibility == undefined) { visibility = true }
      var data = {}

      if (visibility) {
        data['visibility'] = true
        data['latitude'] = getCookiePosition().latitude
        data['longitude'] = getCookiePosition().longitude
      } else {
        data['visibility'] = false
      }

      $.ajax({
        url: '/users/profile/update_location',
        method: 'patch',
        data: data
      });
    }

    $('.footer .container .selectors').on('click', '#toogle-current-location', function () {
      if(getUserGeolocation()) {
        error(9);
      } else {
        if (navigator.geolocation) {
          loopCheckLocation(true);
        } else {
          alert("Error: Your browser doesn't support geolocation.")
        }
      }
    });

    setInterval(loopCheckLocation, refresh_interval);
  }
}

$( document ).on('turbolinks:load', function() {
  detectLocation();
})