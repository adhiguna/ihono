import Quill from 'quill';
import imageUploader from 'quill-image-uploader';
 
Quill.register("modules/imageUploader", imageUploader);

function initQuill () {
  $('.quill-editor').each(function () {
    var target = $(this).data('target');
    var image_base_url = $(this).data('image-base-url');
    var editor = new Quill($(this)[0], {
      modules: {
        toolbar: [
          [{ header: [2, 3, false] }],
          [{ 'align': ['', 'center', 'right', 'justify'] }],
          ['bold', 'italic', 'underline'], 
          ['image']
        ],
        imageUploader: {
          upload: file => {
            return new Promise((resolve, reject) => {
              var formData = new FormData();
              formData.append('file', file)

              $.ajax({
                type: "POST",
                url: '/image_cache_uploader/upload',
                processData: false,
                contentType: false,
                data: formData,
                success: (data) => {
                  resolve(['https:/', image_base_url, data.storage, data.id].join('/'))
                },
                error: function(error) {
                  if( error.responseText == 'Upload Too Large' ) {
                    alert('Image too large (max 3 MB)')
                  } else {
                    alert(error.responseText)
                  }
                  resolve('')
                },
              });
            });
          }
        }
      },
      theme: 'snow'
    });

    editor.on('text-change', function(delta, source) {
      $(target).val(editor.root.innerHTML)
    });
  });
}

$( document ).on('turbolinks:load', function() {
  initQuill();
})