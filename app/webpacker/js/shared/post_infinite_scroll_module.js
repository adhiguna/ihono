// auto open whakapapa_list when landing in page

function postInfiniteScroll () {
  if ($('#post-infinite-scroll').length > 0) {
    $(window).scroll(function () { 
      if (($(window).scrollTop() >= $(document).height() - $(window).height() - 50) && $('#next-page').length > 0) {
        document.getElementById('next-page').click()
        $('#pagination-wrapper').html("<div class='article article-index;'><h3 class='mt-5 mb-5 text-center'>Please wait ...</h3></div>");
      }
   });
  }
}

$( document ).on('turbolinks:load', function() {
  postInfiniteScroll();
})

