function showHidePageSearch () {
  if ($('#page-search-wrapper').length > 0) {
    if (location.hash != undefined && location.hash == '#search-section') {
      showHideSearchSection('show')
    } else {
      showHideSearchSection('hide')
    }
  }

  $(document).on('click', '#search-section-trigger', function () {
    location.hash = "search-section";
    showHideSearchSection('show');
  });

  function showHideSearchSection (state) {
    if (state == undefined) {
      state = 'hide';
    }

    if (state == 'hide') {
      $('#search-section').hide();
      $('#landing-section').show();
      $('.navbar.fixed-top').addClass('hidden');
    } else {
      $('#search-section').show();
      $('#landing-section').hide();
      $('.navbar.fixed-top').removeClass('hidden');
      $('#search-input').focus();
    }
  }
}

$( document ).on('turbolinks:load', function() {
  showHidePageSearch()
})