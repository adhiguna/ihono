var markerConstant = {
  hideActualMapLoc: function (map_loc) {
    return map_loc + 0.001;
  },

  circleMapsOption: {
    radius: 150,
    fillColor: '#AECA36',
    strokeOpacity: 0,
    fillOpacity: 0.35,
    strokeWeight: 0,
  },

  setIconMarker: function (type) {
    return 'https://ihono-assets-staging.manawa.nz/icon/map-icon/'+type+'.png'
  }
}

export default markerConstant;