import markerConstant from './marker_constant';

function loadShowMap () {
  if ($('#show_map').length > 0) {
    var head = document.getElementsByTagName('head')[0];
    var googleMapId = $('#show_map').data('google-map-id');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key='+googleMapId+'&callback=initShowMap';
    head.appendChild(script);
  }
}

function initShowMap() {
  if ($('#show_map').length > 0) {
    var latitude = parseFloat($('#show_map').data('latitude'));
    var longitude = parseFloat($('#show_map').data('longitude'));
    var type = $('#show_map').data('type');
    var markers = [];

    if (type == 'user') {
      latitude = markerConstant.hideActualMapLoc(latitude) ;
      longitude = markerConstant.hideActualMapLoc(longitude);
    }

    var map = new google.maps.Map(document.getElementById('show_map'), {
      center: { lat: latitude, lng: longitude },
      zoom: 16,
      mapTypeId: 'roadmap'
    });
  
    var icon = {
      url: markerConstant.setIconMarker(type),
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25)
    };
  
    clearMarker()
    // set default map if already present latitude longitude
    addMarker({ lat: parseFloat(latitude), lng: parseFloat(longitude) });
  
    function clearMarker() {
      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
    
      markers = [];
    }

    function addMarker(latLang) {
      var bounds = new google.maps.LatLngBounds();
      var circle = new google.maps.Circle(markerConstant.circleMapsOption);
      var marker = new google.maps.Marker({
        map: map,
        icon: icon,
        position: latLang
      })

      circle.bindTo('center', marker, 'position');
      circle.bindTo('map', marker, 'map');
  
      // Create a marker for each place.
      markers.push(marker);
      bounds.extend(latLang);
      map.fitBounds(bounds);
      
      // making set zoom level after set fitbounds
      var listener = google.maps.event.addListener(map, "idle", function() { 
        if (map.getZoom() > 16) map.setZoom(16); 
        google.maps.event.removeListener(listener); 
      });
    }
  }
}

window.initShowMap = initShowMap;

$( document ).on('turbolinks:load', function() {
  loadShowMap()
})