import 'cropperjs/dist/cropper.min'
import 'jquery-cropper/dist/jquery-cropper.min'

function initCropper() {
  if ($('.cropper-wrapper').length > 0) {
    $('.cropper-wrapper').each(function () {
      var photo_input = $(this).find('input[type="file"]');
      var id_target = photo_input.data('target');
      var aspect_ratio = photo_input.data('aspect-ratio');
      var image_selector = $('#preview_'+id_target);
      var target_x = $('#x_'+id_target);
      var target_y = $('#y_'+id_target);
      var target_h = $('#h_'+id_target);
      var target_w = $('#w_'+id_target);

      $(document).on('change', photo_input, function () {
        const file = photo_input[0].files[0];
        const fileReader = new FileReader();
        
        fileReader.onload = function(){
          clearInput();

          var base64ImageSrc = fileReader.result;
          image_selector.attr('src', base64ImageSrc);
          image_selector.addClass('cropper-hidden');
          image_selector.after('<h4 id="loading-image">Load Image</h4>');
          
          buildCropper();
        };

        if (file != undefined) {
          fileReader.readAsDataURL(file);
        }
      });

      function clearInput() {
        if(image_selector.hasClass('cropper-hidden')) {
          image_selector.cropper('destroy');
        }

        image_selector.attr('src', '');
        target_x.val('');
        target_y.val('');
        target_w.val('');
        target_h.val('');
      }

      function buildCropper() {
        image_selector.cropper({
          aspectRatio: aspect_ratio,
          zoomable: false,
          scalable: false,
          rotatable: false,
          crop(event) {
            console.log(event);
            target_x.val(event.detail.x);
            target_y.val(event.detail.y);
            target_h.val(event.detail.height);
            target_w.val(event.detail.width);
          }
        }).ready(function () {
          $('#loading-image').remove();
        });
      }
    });
  }
}

$( document ).on('turbolinks:load', function() {
  initCropper();
})
