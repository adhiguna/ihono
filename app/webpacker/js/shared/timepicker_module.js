import 'jquery-timepicker/jquery.timepicker'

$(document).on('turbolinks:load', function() {
  $('[data-toggle="timepicker"]').each(function() {
    let options = {
      timeFormat: 'h:mm p',
      interval: 30,
      dynamic: false,
      dropdown: true,
      scrollbar: true
    }

    if($(this).data('options')) {
      options = {...options, ...$(this).data('options')}
    }

    $(this).timepicker(options);
  });
})