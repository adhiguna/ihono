function onClickButtonFilter () {
  if ($('[data-target="#filter-modal"]').length > 0) {
    $('[data-target="#filter-modal"]').on('click', function () {
      $('.filter-selection').hide();
      $('#filters-' + $(this).data('type')).show()
      $('.modal-title #title').html($(this).data('type'));
    });
  }
}

$( document ).on('turbolinks:load', function() {
  onClickButtonFilter()
})