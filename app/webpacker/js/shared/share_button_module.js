import '../../js/vendor/addthis_widget';

function initShareButton () {
  if ($('#article-share').length > 0) {
    addthis.shareButton();
  }
}

$( document ).on('turbolinks:load', function() {
  initShareButton();
})

