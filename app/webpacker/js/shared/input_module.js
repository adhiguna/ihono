import 'jquery-mask-plugin'

function maskingInput () {
  $('[data-phone]').mask('000-000-0000')
  $('[data-date]').mask('00-00-0000')
}

function readOnlyInput () {
  $('.readonly').on('keydown change', function(e) {
    e.stopPropagation();
    e.preventDefault();  
    e.returnValue = false;
    e.cancelBubble = true;
    return false;
  });
}

$( document ).on('turbolinks:load cocoon:after-insert', function() {
  readOnlyInput();
  maskingInput();
})