function sortFilter () {
  if (document.getElementById('sort-filter-wrapper')) {
    $('#sort-filter-wrapper').on('click', '.sort-filter', function () {
      let target = $(this)
      let sortType = target.data('sort-type')
      let sortposition = target.data('sort-position')

      if (sortposition == 'asc') {
        sortposition = 'desc'
      } else {
        sortposition = 'asc'
      }

      $('#sort').val(sortType + ' ' + sortposition);
      target.closest('form').submit();
    });
  }
}

$( document ).on('turbolinks:load', function() {
  sortFilter();
})