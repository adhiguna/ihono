import '@chenfengyuan/datepicker';

$(document).on('turbolinks:load', function() {
  $('[data-toggle="datepicker"]').each(function() {
    let options = {
      startDate: new Date(),
      format: 'dd-mm-yyyy',
      autoHide: true
    }

    if($(this).data('options')) {
      options = {...options, ...$(this).data('options')}
    }

    $(this).datepicker(options);
  });
  
})