// auto open whakapapa_list when landing in page

function showHideLineAgeHapu () {
  if ($('#whakapapa').length > 0) {
    $('.nested-lineage').each(function () {
      showHideLineAgeHapu($(this))
    });

    $('#whakapapa')
      .on('cocoon:after-insert', function() {
        showHideLineAgeHapu($('.nested-lineage').last())
      })
    
    $(document).on('change', '.iwi_id', function () {
      showHideLineAgeHapu($(this).parents('.nested-lineage').first())
    });
  
    function showHideLineAgeHapu (wrapper) {
      var iwiID = wrapper.find('.iwi_id').first()
      var hapuID = wrapper.find('.hapu_id').first()
      var hapuIDWrapper = wrapper.find('.hapu_id_wrapper').first()
      select2InInput(iwiID);
      select2InInput(hapuID);
  
      if (iwiID.val() != undefined && iwiID.val() != '') {
        getListHapu(iwiID, hapuID, hapuIDWrapper);
      } else {
        hapuIDWrapper.hide()
        hapuID.val('')
      }
    }

    function getListHapu (iwiID, hapuID, hapuIDWrapper) {
      $.ajax({
        url: '/iwi/'+iwiID.val()+'/get_hapu',
        dataType: 'json',
        success:function(results) {
          var getOldHapuID = hapuID.val() || hapuID.data('id');
          hapuID.html('');

          hapuID.append($("<option></option>").text('- Select Hapu -'));
          if (results.length > 0) {
            $.each(results, function(key, value) {
              hapuID.append($("<option></option>").attr('value', value[1]).text(value[0]));
            });
          }

          if (results.length > 0) {
            hapuIDWrapper.show()
            hapuID.val(getOldHapuID)
          } else {
            hapuIDWrapper.hide()
            hapuID.val('- Select Hapu -')
            hapuID.data('id', '')
          }
        }
      });
    }

    function select2InInput (target) {
      $(target).select2({
        theme: 'bootstrap4',
        placeholder: $(target).attr('placeholder')
      })
    }
  }
}

$( document ).on('turbolinks:load', function() {
  showHideLineAgeHapu();
})

