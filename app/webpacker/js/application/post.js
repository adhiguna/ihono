function showHidePostType () {
  if(document.getElementById('post-form')) {
    let target = document.getElementById('post-type-toggle');
    let targetInput = document.getElementById('post_post_type');

    showHide();

    $(targetInput).on('change', showHide);

    function showHide () {
      let value = targetInput.value
      
      if (!value || ['News', 'Events', 'Posts'].includes(value)) {
        target.style.display = 'none';
      } else {
        target.style.display = 'block';
      }
    }
  }
}

$(document).on('turbolinks:load', function() {
  showHidePostType();
})