import '../../shared/ip_look_up_module';

function filterBuild () {
  if (document.getElementById('search-section-health-provider')) {
    askingLocation();

    $('[data-target="#filter-modal"]').on('click', function (e) {
      if (e.currentTarget.getAttribute('data-type') == 'sort' && ($('#search_map_latitude').val().length == 0 || $('#search_map_longitude').val().length == 0)) {
        buildErrorDisabledLocation();
      } else {
        $('#filter-modal').modal('show');
      }
    });
  
    function askingLocation () {
      var not_support_location_banner = $('#not-support-location');
      var geoOptions = { 
        maximumAge: 5 * 60 * 1000,
        timeout: 10 * 1000
      }
    
      var geoSuccess = function(position) {
        $('#search_map_latitude').val(position.coords.latitude)
        $('#search_map_longitude').val(position.coords.longitude)
        
        console.log(position.coords.latitude +' - '+ position.coords.longitude)
      };
    
      var geoError = async function(error_code) {
        let getIpLocation = await ipLookUp();
        if (getIpLocation) {
          geoSuccess(getIpLocation);
        } else {
          buildErrorDisabledLocation();
        }
      };
    
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(geoSuccess, function(err) { geoError(err.code) }, geoOptions);
      }
      else {
        not_support_location_banner.show()
      }
    }
    
    function buildErrorDisabledLocation () {
      var html = `<div class='modal fade' id='modal-disabled' tabindex='-1 role='dialog'>
                    <div style='margin-top: 20%;' class='modal-dialog' role='document'>
                      <div class='modal-content'>
                        <div class='modal-body' style='text-align: center;'>
                          <p>Looked like your web browser are disabled for getting your location to doing sort function. Please read <a href='https://www.wikihow.com/Enable-Location-Services-on-Google-Chrome' target='blank'>this article for enabled it</a> and refresh your web browser</p>
                          <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>`;
      $('body').append(html);
      $('#modal-disabled')
        .on('hidden.bs.modal', function (e) {
          $('#modal-disabled').remove();
        })
        .modal('show');
    }
  }
}



function showHideFilterHealthProviderList() {
  if (document.getElementById('search-section-health-provider')) {
    let input = document.getElementById('search-section-health-provider-input');

    function doFilter() {
      let inputValue = input.value;
      let regex = new RegExp(inputValue, 'gi');
      let rowList = $('#health-provider-list-result [data-region]');

      rowList.each(function(index, element) {
        let getRegion = element.getAttribute('data-region');

        if (getRegion.match(regex) || inputValue.length == 0) {
          element.style.display = 'block'
        } else {
          element.style.display = 'none'
        }
      });
    }

    doFilter();

    $('#search-section-health-provider-input').on('keyup', function() {
      doFilter()
    });

    $('#search-section-health-provider-reset').on('click', function() {
      input.value = ''
      doFilter()
    });
  }
}

window.showHideFilterHealthProviderList = showHideFilterHealthProviderList;

$(document).on('turbolinks:load', function() {
  showHideFilterHealthProviderList();
  filterBuild();
})
