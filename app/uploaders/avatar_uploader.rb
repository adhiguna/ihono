# https://github.com/shrinerb/shrine/blob/master/doc/processing.md#optimizing-thumbnails

require "image_processing/mini_magick"

class AvatarUploader < Shrine
  include ImageProcessing::MiniMagick

  plugin :remote_url, max_size: nil
  plugin :processing
  plugin :versions
  plugin :delete_raw
  plugin :determine_mime_type
  plugin :store_dimensions, analyzer: :mini_magick
  plugin :validation_helpers
  plugin :pretty_location
  plugin :default_url

  Attacher.default_url do |options|
    "https://#{ENV['AWS_BUCKET_NAME']}/store/users/default/default_#{options[:version]}.png"
  end

  Attacher.validate do
    validate_max_size 5.megabytes, message: 'is too large (max is 5 MB)'
    validate_mime_type_inclusion ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
  end

  process(:store) do |io, context|
    cropping = io.metadata['cropping']
    original = io.download
    versions = { original: io } # retain original
    pipeline = ImageProcessing::MiniMagick.source(original)

    if cropping.present?
      versions[:original] = pipeline.crop!("#{cropping['w']}x#{cropping['h']}+#{cropping['x']}+#{cropping['y']}")
      pipeline = pipeline.source(versions[:original].path)
    else
      versions[:original] = io
    end

    large  = pipeline.resize_to_limit!(225, 225)
    ImageOptimizer.new(large.path).optimize
    large.open
    versions[:large] = large

    small  = pipeline.resize_to_limit!(100, 100)
    ImageOptimizer.new(small.path).optimize
    small.open
    versions[:small] = small

    original.close!

    versions # return the hash of processed files
  end

  def generate_location(io, context)
    original_filename = context[:metadata]['filename']

    # saving original filename so no more uniq id
    if context[:version]
      if context[:version] == :original
        context[:record].update_column(:avatar_filename, context[:metadata]["filename"]) 
      end

      original_filename = context[:record].avatar_filename
    end
    
    version_suffix    = "_#{context[:version]}" if context[:version] && context[:version] != :original
    basename          = File.basename(original_filename, ".*")
    extension         = File.extname(original_filename).downcase
  
    "#{context[:record].class.name.downcase.pluralize}/#{context[:record]&.id}/#{basename}#{version_suffix}#{extension}"
  end
end