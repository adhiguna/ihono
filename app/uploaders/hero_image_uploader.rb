# https://github.com/shrinerb/shrine/blob/master/doc/processing.md#optimizing-thumbnails

require "image_processing/mini_magick"

class HeroImageUploader < Shrine
  include ImageProcessing::MiniMagick

  plugin :remote_url, max_size: nil
  plugin :processing
  plugin :versions
  plugin :delete_raw
  plugin :determine_mime_type
  plugin :store_dimensions, analyzer: :mini_magick
  plugin :validation_helpers
  plugin :pretty_location
  plugin :default_url

  Attacher.default_url do |options|
    # for the reason please take a look to s3
    url_number = if options[:version] == :thumbnail
                  if record.id % 5 == 0
                    1
                  elsif record.id % 4 == 0
                    2
                  elsif record.id % 3 == 0
                    3
                  elsif record.id % 2 == 0
                    4
                  else
                    5
                  end
                elsif options[:version] == :cover
                  if record.id % 3 == 0
                    3
                  elsif record.id % 2 == 0
                    2
                  else
                    1
                  end
                end

    "https://#{ENV['AWS_BUCKET_NAME']}/store/posts/default/default_#{options[:version]}_#{url_number}.jpg"
  end

  Attacher.validate do
    validate_max_size 3.megabytes, message: 'venue photo is too large (max is 3 MB)'
    validate_mime_type_inclusion ['image/jpg', 'image/jpeg', 'image/png']
  end

  process(:store) do |io, context|
    cropping = io.metadata['cropping']
    original = io.download
    versions = {}

    puts '----------'
    puts cropping

    pipeline = ImageProcessing::MiniMagick.source(original)

    versions[:original] = io
    
    cover = pipeline.quality(80).resize_to_fill!(600, 900)
    ImageOptimizer.new(cover.path).optimize
    cover.open
    versions[:cover] = cover

    thumbnail = pipeline.quality(80).resize_to_fill!(480, 270)
    ImageOptimizer.new(thumbnail.path).optimize
    thumbnail.open
    versions[:thumbnail] = thumbnail

    original.close!

    versions # return the hash of processed files
  end

  def generate_location(io, context)
    original_filename = context[:metadata]['filename']

    # saving original filename so no more uniq id
    if context[:version]
      if context[:version] == :original
        context[:record].update_column(:hero_image_filename, context[:metadata]['filename']) 
      end

      original_filename = context[:record].hero_image_filename
    end
    
    version_suffix    = "_#{context[:version]}" if context[:version] && context[:version] != :original
    basename          = File.basename(original_filename, ".*")
    extension         = File.extname(original_filename).downcase
  
    "#{context[:record].class.name.downcase.pluralize}/#{context[:record]&.id}/#{basename}#{version_suffix}#{extension}"
  end
end