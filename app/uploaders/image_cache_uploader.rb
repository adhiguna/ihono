class ImageCacheUploader < Shrine
  include ImageProcessing::MiniMagick

  plugin :validation_helpers
  plugin :determine_mime_type
  plugin :pretty_location
  plugin :restore_cached_data
  plugin :cached_attachment_data
  plugin :upload_endpoint, max_size: 3*1024*1024, message: 'is too large (max is 3 MB)' 

  Attacher.validate do
    validate_mime_type_inclusion ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
  end

  def generate_location(io, context)
    original_filename = context[:metadata]['filename']
    basename          = File.basename(original_filename, ".*")
    extension         = File.extname(original_filename).downcase
  
    "cache_images/#{basename}_#{DateTime.now.to_i}#{extension}"
  end
end