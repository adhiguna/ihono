module PostHelper
  def type_list_input
    return Post::TYPE_LIST.slice('public') if current_user.user?
    return Post::TYPE_LIST
  end

  def show_icon_post?
    controller_name == 'posts' && (action_name == 'index' || action_name == 'show')
  end

  def list_posts_button_path
    if controller.class.name.downcase.include?('corona')
      corona_posts_path
    else
      posts_path
    end
  end
end