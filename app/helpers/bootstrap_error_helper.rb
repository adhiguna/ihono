module BootstrapErrorHelper
  def bootstrap_error(model)
    if model.errors.present?
      content_tag :div, class: 'alert alert-danger text-left error-form' do
        html_messages = content_tag :h6, 'Please check error below :'
        html_messages += content_tag :ul do
          model.errors.full_messages.each do |message|
            concat content_tag :li, message
          end
        end

        html_messages.html_safe
      end
    end
  end
end