module NotificationHelper
  def notification_link_message(receive_notification)
    case receive_notification.notifiable_type
    when 'UserInvitationFriendship'
      if receive_notification.status == 'create'
        "<p>#{link_to receive_notification.sender.full_name, user_path(receive_notification.sender)} sent you friend invitation request</p>".html_safe
      end
    end
  end
end