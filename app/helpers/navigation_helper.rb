module NavigationHelper
  def open_super_admin_menu?
    (controller_name == 'users' && action_name != 'show')     ||
    (controller_name == 'regions' && action_name != 'show')   ||
    (controller_name == 'rohes' && action_name != 'show')     ||
    (controller_name == 'iwis' && action_name != 'show')      ||
    (controller_name == 'hapus' && action_name != 'show')     ||
    (controller_name == 'maraes' && action_name != 'show')    ||
    (controller_name == 'wharenuis' && action_name != 'show')
  end

  def open_health_admin_menu?
    (controller_name == 'health_providers') ||
    (controller_name == 'appointments')
  end
end