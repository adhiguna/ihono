module ApplicationHelper
  def full_date(date)
    date.strftime('%d %B %Y')
  end

  def alternative_redirect(data_link)
    current_user ? data_link : new_user_session_path(redirect_to: data_link)
  end

  def back_link
    @back_link || params[:redirect_back_to] || :back
  end

  def show_back_button?
    controller_name != 'setup_account'
  end

  def notification_count
    if current_user && current_user.receiver_notifications_count > 0
      content_tag :span, current_user.receiver_notifications_count, class: 'count'
    else
      ''
    end
  end

  def filter_search_value(parameter)
    # params[:suggest_keyword_type] = { :key => :value }
    # take a look to searches_controller.rb
    
    if params[:filters]
      params[:filters].send(:[], parameter)
    else
      nil
    end
  end

  def google_maps_tag_location(latitude, longitude)
    "https://www.google.com/maps/search/?api=1&query=#{latitude},#{longitude}"
  end
end
