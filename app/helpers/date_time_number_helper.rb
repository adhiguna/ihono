module DateTimeNumberHelper
  def format_date(date)
    return if date.blank?
    date = date.is_a?(Date) ? date : Date.parse(date)

    date.strftime('%d %B %Y')
  end

  def format_time(time)
    return if time.blank?
    time = time.is_a?(Time) ? time : Time.parse(time)

    time.strftime('%I:%M %p')
  end
end