class HapuPresenter
  def initialize(hapu)
    @hapu = hapu
  end

  def get_icon
    'ihono-icon-iwi-hapu' 
  end

  def get_rohes
    rohes.pluck(:name).uniq.join(' / ')
  end

  # [
  #   {
  #     name: 'name', active: false, type: 'iwi', data: {
  #       name: 'name', active: false, type: 'hapu', data: {
  #         name: 'name', active: false
  #       }
  #     }
  #   },
  #   {
  #     name: 'name', active: false, type: 'iwi', data: {
  #       name: 'name', active: false, type: 'hapu', data: {
  #         name: 'name', active: false
  #       }
  #     }
  #   }
  # ]  
  def get_whakapapa_structure
    iwis.map do |data_iwi|
      { 
        id: data_iwi.id,
        name: data_iwi.name, 
        active: false, 
        type: 'iwi',
        data_hapu: data_iwi.hapus.order('name').map do |data_hapu|
          {
            id: data_hapu.id,
            name: data_hapu.name, 
            active: data_hapu == @hapu, 
            type: 'hāpu',
            data_marae: data_hapu.maraes.order('name').map do |data_marae|
              { 
                id: data_marae.id,
                name: data_marae.name,
                active: false,
                type: 'marae',
                address: data_marae.address,
                latitude: data_marae.latitude,
                longitude: data_marae.longitude,
                data_wharenuis: data_marae.wharenuis.pluck(:name).join(', ')
              }
            end
          }
        end
      }
    end
  end

  private
  def method_missing(*args, &block)
    @hapu.send(*args, &block)
  end
end