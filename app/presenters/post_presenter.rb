class PostPresenter
  def initialize(post)
    @post = post
  end

  def user_avatar(size = :small)
    user.avatar_url(size)
  end

  def user_fullname
    user.full_name
  end

  def total_user_likes
    user_likes.size
  end

  def cover_image
    hero_image_url(:cover)
  end

  def cover_thumbnail_image
    hero_image_url(:thumbnail)
  end

  private
  def method_missing(*args, &block)
    @post.send(*args, &block)
  end
end