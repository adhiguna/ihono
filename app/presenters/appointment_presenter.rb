class AppointmentPresenter
  include ActionView::Helpers::UrlHelper
  include DateTimeNumberHelper
  

  attr_accessor :appointment

  def initialize(appointment)
    @appointment = appointment
  end

  def date_time_booking
    "#{appointment_date} #{appointment_time}"
  end

  def health_provider_name
    health_provider.name
  end

  def appointment_date
    format_date(appointment.appointment_date)
  end

  def appointment_time
    format_time(appointment.appointment_time)
  end

  def feels
    appointment.feels.join(', ')
  end

  def service_worker
    appointment.health_care_worker || appointment.essential_services_worker ? 'Yes' : 'No'
  end

  def contacted
    appointment.contact_with_corona || appointment.travel_overseas || appointment.contact_with_overseas_traveller ? 'Yes' : 'No'
  end

  def contact_details
    [email, cell_phone, home_phone].reject(&:blank?).join('<br>')
  end

  def email
    return '' if appointment.email.blank?

    mail_to(appointment.email, appointment.email)
  end

  def cell_phone
    return '' if appointment.cell_phone.blank?

    link_to(appointment.cell_phone, "tel:#{appointment.cell_phone}")
  end

  def home_phone
    return '' if appointment.home_phone.blank?

    link_to(appointment.home_phone, "tel:#{appointment.home_phone}")
  end

  def pre_exist_health_condition
    appointment.pre_exist_health_condition.join(', ')
  end

  def contact_with_corona
    appointment.contact_with_corona ? 'Yes' : 'No'
  end

  def travel_overseas
    appointment.travel_overseas ? 'Yes' : 'No'
  end

  def contact_with_overseas_traveller
    appointment.contact_with_overseas_traveller ? 'Yes' : 'No'
  end

  def health_care_worker
    appointment.health_care_worker ? 'Yes' : 'No'
  end

  def essential_services_worker
    appointment.essential_services_worker ? 'Yes' : 'No'
  end

  private
  def method_missing(*args, &block)
    @appointment.send(*args, &block)
  end
end