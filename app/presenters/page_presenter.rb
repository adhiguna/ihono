class PagePresenter
  def initialize(page)
    @page = page
  end

  def cover_image
    hero_image_url(:cover)
  end

  def cover_thumbnail_image
    hero_image_url(:thumbnail)
  end

  private
  def method_missing(*args, &block)
    @page.send(*args, &block)
  end
end