class MaraePresenter
  include Rails.application.routes.url_helpers

  def initialize(marae)
    @marae = marae
  end

  def get_icon
    'ihono-icon-marae'
  end

  def info_map_icon_symbol
    'marae'
  end

  def info_map_type_name
    'MARAE'
  end

  def info_map_type
    'marae'
  end

  def info_map_link_generate
    marae_path(@marae)
  end

  def get_rohes
    rohes.pluck(:name).uniq.join(' / ')
  end
 
  # [
  #   {
  #     name: 'name', active: false, type: 'iwi', data: {
  #       name: 'name', active: false, type: 'hapu', data: {
  #         name: 'name', active: false
  #       }
  #     }
  #   },
  #   {
  #     name: 'name', active: false, type: 'iwi', data: {
  #       name: 'name', active: false, type: 'hapu', data: {
  #         name: 'name', active: false
  #       }
  #     }
  #   }
  # ]  
  def get_whakapapa_structure
    iwis.map do |data_iwi|
      { 
        id: data_iwi.id,
        name: data_iwi.name, 
        active: false, 
        type: 'iwi',
        data_hapu: data_iwi.hapus.order('name').map do |data_hapu|
          {
            id: data_hapu.id,
            name: data_hapu.name, 
            active: false, 
            type: 'hāpu',
            data_marae: data_hapu.maraes.order('name').map do |data_marae|
              { 
                id: data_marae.id,
                name: data_marae.name,
                active: data_marae == @marae,
                type: 'marae',
                address: data_marae.address,
                latitude: data_marae.latitude,
                longitude: data_marae.longitude,
                data_wharenuis: data_marae.wharenuis.pluck(:name).join(', ')
              }
            end
          }
        end
      }
    end
  end

  private
  def method_missing(*args, &block)
    @marae.send(*args, &block)
  end
end