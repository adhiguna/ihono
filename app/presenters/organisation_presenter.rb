require 'mini_magick'

class OrganisationPresenter
  include Rails.application.routes.url_helpers

  def initialize(organisation)
    @organisation = organisation
  end

  def organisation_hero_image(size = :large)
    return '' unless hero_image_data.present?

    if json_hero_image_data['storage'] == 'cache'
      unless File.exist?(full_tmp_path)
        generate_tmp_hero_image(size)
      end

      return tmp_path
    else
      hero_image_url(size)
    end
  end

  def tmp_path
    "/tmp_new_organisation_hero_image_#{id}_#{json_hero_image_data['metadata']['size']}.#{hero_image.mime_type.split('/').last}"
  end

  def full_tmp_path
    "#{Rails.root}/public#{tmp_path}"
  end

  def json_hero_image_data
    JSON.parse(hero_image_data)
  end

  def generate_tmp_hero_image(size)
    image = MiniMagick::Image.open(hero_image_url(size))

    if json_hero_image_data['metadata'].present? && json_hero_image_data['metadata']['cropping'].present? && 
      json_hero_image_data['metadata']['cropping']['x'].present? && 
      json_hero_image_data['metadata']['cropping']['y'].present? && 
      json_hero_image_data['metadata']['cropping']['h'].present? && 
      json_hero_image_data['metadata']['cropping']['w'].present? then
        
        image.crop("#{json_hero_image_data['metadata']['cropping']['w']}x#{json_hero_image_data['metadata']['cropping']['h']}+#{json_hero_image_data['metadata']['cropping']['x']}+#{json_hero_image_data['metadata']['cropping']['y']}")
    
    end

    image.write(full_tmp_path)
  end

  def info_map_icon_symbol
    organisation_type.downcase.gsub(' ', '-')
  end

  def info_map_type_name
    organisation_type.upcase
  end

  def info_map_type
    organisation_type.downcase.gsub(' ', '-')
  end

  def info_map_link_generate
    organisation_path(@organisation)
  end

  private
  def method_missing(*args, &block)
    @organisation.send(*args, &block)
  end
end