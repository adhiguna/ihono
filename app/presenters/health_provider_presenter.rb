class HealthProviderPresenter
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::UrlHelper
  include DateTimeNumberHelper
  
  attr_accessor :health_provider

  def initialize(health_provider)
    @health_provider = health_provider
  end

  def get_icon
    'ihono-icon-healthprovider' 
  end

  def info_map_icon_symbol
    'healthprovider'
  end

  def info_map_type_name
    'HEALTH PROVIDER'
  end

  def info_map_type
    'healthprovider'
  end

  def info_map_link_generate
    corona_health_provider_path(@health_provider)
  end

  def short_abstract
    address + '</br>' +
    abstract + '</br>' +
    "Open hours: #{format_time(@health_provider.open)} - #{format_time(@health_provider.close)}" + '</br>' +
    link_appointment
  end

  def link_appointment
    return '' unless allow_appointment

    link_to('Request appointment', corona_health_provider_appointment_path(slug), class: 'booking-bottom')
  end

  def link_to_map
    "https://www.google.com/maps/search/?api=1&query=#{latitude},#{longitude}"
  end

  def get_min_max_time
    { minTime: format_time(@health_provider.open), maxTime: format_time(@health_provider.close) }
  end

  private
  def method_missing(*args, &block)
    @health_provider.send(*args, &block)
  end
end