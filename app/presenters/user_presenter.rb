require 'mini_magick'

class UserPresenter
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::TagHelper
  include ActionView::Context
  include ActionView::Helpers::AssetTagHelper
  include ActionView::Helpers::UrlHelper

  def initialize(user)
    @user = user
  end

  def user_avatar(size = :large)
    return avatar_url(size) unless avatar_data.present?

    if json_avatar_data['storage'] == 'cache'
      unless File.exist?(full_tmp_path)
        generate_tmp_avatar(size)
      end

      return tmp_path
    else
      avatar_url(size)
    end
  end

  def tmp_path
    "/tmp_new_avatar_#{id}_#{json_avatar_data['metadata']['size']}.#{avatar.mime_type.split('/').last}"
  end

  def full_tmp_path
    "#{Rails.root}/public#{tmp_path}"
  end

  def json_avatar_data
    JSON.parse(avatar_data)
  end

  def generate_tmp_avatar(size)
    image = MiniMagick::Image.open(avatar_url(size))

    if json_avatar_data['metadata'].present? && json_avatar_data['metadata']['cropping'].present? && 
      json_avatar_data['metadata']['cropping']['x'].present? && 
      json_avatar_data['metadata']['cropping']['y'].present? && 
      json_avatar_data['metadata']['cropping']['h'].present? && 
      json_avatar_data['metadata']['cropping']['w'].present? then
        
        image.crop("#{json_avatar_data['metadata']['cropping']['w']}x#{json_avatar_data['metadata']['cropping']['h']}+#{json_avatar_data['metadata']['cropping']['x']}+#{json_avatar_data['metadata']['cropping']['y']}")
    
    end

    image.write(full_tmp_path)
  end

  def list_social_media
    return '' unless social_media.present?

    html_full_contents = ''

    social_media.each do |sc|
      html_full_contents += link_to sc.link, class: 'social-media-link', target: '_blank' do
                              content_tag :div, class: "#{sc.social_media_type.downcase} button-wrapper" do
                                html_data = get_social_media_icon(sc.social_media_type)
                                html_data += sc.name
                                html_data.html_safe
                              end
                            end
    end

    html_full_contents.html_safe
  end

  def get_social_media_icon(social_media_type)
    if social_media_type == 'Facebook'
      content_tag :i, '', class: 'fab fa-facebook-f'
    else
      content_tag :i, '', class: "fab fa-#{social_media_type.downcase}"
    end
  end

  def info_map_icon_symbol
    'user'
  end

  def info_map_type_name
    'PEOPLE'
  end

  def info_map_type
    'user'
  end

  def info_map_link_generate
    user_path(@user)
  end

  private
  def method_missing(*args, &block)
    @user.send(*args, &block)
  end
end