class UserInvitationSignupMailer < ApplicationMailer
  def notification(user_invitation_signup)
    @user_invitation_signup_token = user_invitation_signup.token
    @owner_invited_name = user_invitation_signup.owner_invited.name
    @user_invited_name = user_invitation_signup.name
    
    mail(to: user_invitation_signup.email, subject: 'You have been invited to signup on īHono') do |format|
      format.html { render 'mailers/user_invitation_signup_mailer/notification' }
    end
  end
end
