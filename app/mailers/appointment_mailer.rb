class AppointmentMailer < ApplicationMailer
  add_template_helper(DateTimeNumberHelper)

  def create_health_admin_notification(appointment, admin_email)
    @appointment = AppointmentPresenter.new(appointment)
    @health_provider = HealthProviderPresenter.new(@appointment.health_provider)

    mail(to: admin_email, subject: "īhono - New appointment for #{@health_provider.name}") do |format|
      format.html { render 'mailers/appointment_mailer/create_health_admin_notification' }
    end
  end

  def create_patient_notification(appointment, patient_email)
    @appointment = AppointmentPresenter.new(appointment)
    @health_provider = HealthProviderPresenter.new(@appointment.health_provider)

    mail(to: patient_email, subject: "īhono - Your appointment at #{@health_provider.name}") do |format|
      format.html { render 'mailers/appointment_mailer/create_patient_notification' }
    end
  end
end
