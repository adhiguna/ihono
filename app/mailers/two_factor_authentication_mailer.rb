class TwoFactorAuthenticationMailer < ApplicationMailer
  def send_to_user(user, code)
    @code = code
    @user  = user
    mail(to: @user.email, subject: 'īhono - temporary access code') do |format|
      format.html { render 'mailers/two_factor_authentication_mailer/send_to_user' }
    end
  end
end
