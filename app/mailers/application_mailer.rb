class ApplicationMailer < ActionMailer::Base
  default from: "dont-reply@#{ENV['DOMAIN_NAME']}"
  layout 'mailer'
end
