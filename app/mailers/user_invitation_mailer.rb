class UserInvitationMailer < ApplicationMailer
  def create(user_invitation_friendship)
    @user = user_invitation_friendship.user
    @user_friend = user_invitation_friendship.user_friend
    
    mail(to: @user_friend.email, subject: 'You have been invited to connect on īHono') do |format|
      format.html { render 'mailers/user_invitation_mailer/create' }
    end
  end
end
