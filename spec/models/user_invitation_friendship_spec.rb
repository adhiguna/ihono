require 'rails_helper'

RSpec.describe UserInvitationFriendship, type: :model do
  let(:uif) { create(:user_invitation_friendship) }

  it "has a valid factory" do
    expect(build(:user_invitation_friendship)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_many(:notifications) }
    it { should belong_to(:user) }
    it { should belong_to(:user_friend) }
    it { should belong_to(:request_by) }
  end

  it "create notification after create" do
    expect {uif}.to change{[UserInvitationFriendship.count, Notification.count]}.by([1, 1])
  end

  it "should be add friend on accepted" do
    user = uif.user
    expect { uif.accept! }.to change{user.friends.count}.by(1)
  end

  it "should be remove user_invitation_friendship on rejected" do
    uif.reject!
    expect(uif.destroyed?).to be_truthy
  end
end
