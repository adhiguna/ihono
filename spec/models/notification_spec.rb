require 'rails_helper'

RSpec.describe Notification, type: :model do
  it "has a valid factory" do
    expect {
      expect(create(:post_notification)).to be_valid
    }.to change{[User.count, Notification.count, Post.count]}.by([3, 1, 1])
  end

  describe "ActiveRecord associations" do
    it { should have_many(:receivers) }
    it { should belong_to(:sender) }
    it { should belong_to(:notifiable) }

    it { should accept_nested_attributes_for(:receivers) }
    it { should accept_nested_attributes_for(:sender) }
  end

  describe "Class methods" do
    let(:uif) { create(:user_invitation_friendship) }
    let(:user) { create(:user) }

    it "should create notifications" do
      expect {
        Notification.create_notification(uif.user, uif.user_friend, uif, 'create')
      }.to change{Notification.count}.by(2)
    end

    it "should return user object if passed user.id or user object" do
      expect(Notification.get_user(user)).to eq(user)
      expect(Notification.get_user(user.id)).to eq(user)
    end

    it "should return user array and user object inside it" do
      expect(Notification.get_user_receivers_data([user])).to eq([user])
      expect(Notification.get_user_receivers_data([user.id])).to eq([user])
      expect(Notification.get_user_receivers_data(user)).to eq([user])
      expect(Notification.get_user_receivers_data(user.id)).to eq([user])
    end

    it "should return true if passed correct notification type" do
      expect(Notification.checking_notifiable_status(uif, 'create')).to be_truthy
    end

    it "should raise error if passed not registered notif type" do
      expect{Notification.checking_notifiable_status(create(:post), 'create')}.to raise_error( "can't find Post with status create")
    end
  end
end
