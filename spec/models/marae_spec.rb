# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Marae, type: :model do
  let(:marae) { create(:marae, contact: 'marae@test.com') }

  it "has a valid factory" do
    expect(build(:marae)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:hapus) }
    it { should have_and_belong_to_many(:iwis) }
    it { should have_and_belong_to_many(:rohes) }
    it { should have_and_belong_to_many(:wharenuis) }
    it { should have_many(:regions) }
  end

  it { should validate_presence_of(:name) }

  describe "Geocoder" do
    # it "should decode address into latitude, longitude, postcode, suburb and country" do
    #   marae.update(address: 'Kuala Lumpur')
    #   DEFAULT_GEOCODE.each do |key, value|
    #     expect(marae[key]).not_to eq(value)
    #   end
    # end

    it "should not decode address into latitude, longitude, postcode, suburb and country" do
      marae.update(address: 'Kuala Lumpur', turn_off_geocoder: true)
      DEFAULT_GEOCODE.each do |key, value|
        expect(marae[key]).to eq(value)
      end
    end
  end

  describe "multisearchable" do
    it { expect(PgSearch.multisearch(marae.contact.first(3)).count).to eq(1) }
    it { expect(PgSearch.multisearch(marae.name.first(3)).count).to eq(1) }
    it { expect(PgSearch.multisearch(marae.suburb.first(3)).count).to eq(1) }
    it { expect(PgSearch.multisearch(marae.postcode.first(3)).count).to eq(1) }

    it "should return correct results when search by name" do
      results = Marae.search_filters(params: { keyword: marae.name.first(3) })
      expect(results.count).to eq(1)
    end
  end

  describe "InfoMap" do
    before do
      expect {
        marae
        create(:marae, turn_off_geocoder: true, latitude: nil, longitude: nil)
      }.to change{Marae.count}.by(2)
    end
    
    # 3KM lat long from default lat lng for test
    LAT_LONG = { latitude: -41.323573, longitude: 174.745412 }

    it { expect(Marae.get_base_data.length).to eq(1) }
    it { expect(Marae.extract_data(LAT_LONG[:latitude], LAT_LONG[:longitude], 2).length).to eq(0) }
    it { expect(Marae.extract_data(LAT_LONG[:latitude], LAT_LONG[:longitude], 4).length).to eq(1) }
    it { expect(Marae.extract_data(LAT_LONG[:latitude], LAT_LONG[:longitude], 51).length).to eq(1) }
  end
end
