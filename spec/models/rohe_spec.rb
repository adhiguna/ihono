require 'rails_helper'

RSpec.describe Rohe, type: :model do
  it "has a valid factory" do
    expect(build(:rohe)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:regions) }
    it { should have_and_belong_to_many(:iwis) }
    it { should have_and_belong_to_many(:hapus) }
    it { should have_and_belong_to_many(:maraes) }
    it { should have_and_belong_to_many(:wharenuis) }
    it { should have_and_belong_to_many(:organisations) }
  end

  it { should validate_presence_of(:name) }
end
