require 'rails_helper'

RSpec.describe Region, type: :model do
  it "has a valid factory" do
    expect(build(:region)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:rohes) }
    it { should have_and_belong_to_many(:organisations) }
    it { should have_many(:iwis) }
    it { should have_many(:hapus) }
    it { should have_many(:maraes) }
    it { should have_many(:wharenuis) }
  end

  it { should validate_presence_of(:name) }
end
