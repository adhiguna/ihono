require 'rails_helper'

RSpec.describe ReceiverNotification, type: :model do
  let(:notification) { create(:user_invitation_friendship).notifications.first }
  let(:receiver) { notification.receivers.first }
  let(:user) { receiver.user }

  it "should create receiver when user_invitation_friendship created" do
    expect{receiver}.to change{ReceiverNotification.count}.by(1)
  end

  describe "ActiveRecord associations" do
    it { should belong_to(:notification) }
    it { should belong_to(:user) }
  end

  describe "scope" do
    before { receiver }

    it "should not raise an error" do
      expect(ReceiverNotification.undeleted.count).to eq(1)
      expect(ReceiverNotification.unread.count).to eq(1)
    end
  end

  describe "instance methods" do
    it "should not raise an error" do
      expect(receiver.receiver).to eq(user)
      expect(receiver.sender).to eq(notification.sender.user)
      expect(receiver.status).to eq(notification.notifiable_status)
      expect(receiver.notifiable).to eq(notification.notifiable)
      expect(receiver.notifiable_type).to eq(notification.notifiable_type)
      expect(receiver.notifiable_id).to eq(notification.notifiable_id)
    end
  end
end
