require 'rails_helper'

RSpec.describe Iwi, type: :model do
  it "has a valid factory" do
    expect(build(:iwi)).to be_valid
  end

  it { should validate_presence_of(:name) }

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:rohes) }
    it { should have_and_belong_to_many(:hapus) }
    it { should have_and_belong_to_many(:maraes) }
    it { should have_and_belong_to_many(:wharenuis) }
    it { should have_many(:regions) }
  end

  describe "Custom methods" do
    before do
      text = 5.times.map { Faker::Quote.matz }
      @iwi = create(:iwi, abstract: text, name: 'Anakin Skywalker')
    end

    it "should generate slug on create" do
      expect(@iwi.slug).to eq('anakin-skywalker')
    end

    it "should return short abstract" do
      expect(@iwi.abstract.length).to be > 200
      expect(@iwi.short_abstract.length).to equal(200)
    end

    it "should return correct results on search" do
      # search with nothing passed
      results = Iwi.search_filters
      expect(results.length).to equal 1

      # search with param passed
      keyword = { keyword: 'Testing' }
      results = Iwi.search_filters(params: keyword)
      expect(results.length).to equal 0

      # search with param and should be matched record with name Anakin Skywalker
      keyword = { keyword: 'sky' }
      results = Iwi.search_filters(params: keyword)
      expect(results.length).to equal 1
    end

    it "should return correct results on multisearch" do
      documents = PgSearch.multisearch("Anakin")
      expect(documents.count).to equal 1
      expect(documents.first.searchable).to eq @iwi
    end

    it "should reindex pgSearch on iwi updated" do
      @iwi.update(name: 'King of Hyrule')
      documents = PgSearch.multisearch("hyrule")
      expect(documents.count).to equal 1
      expect(documents.first.searchable).to eq @iwi

      documents = PgSearch.multisearch("Anakin")
      expect(documents.count).to equal 0
    end
  end
end
