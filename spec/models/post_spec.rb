require 'rails_helper'

RSpec.describe Post, type: :model do
  describe "validations" do
    context "title" do
      it "valid when title is present" do
        post = build(:post, title: "The Title")

        expect(post.valid?).to eq(true)
      end

      it "invalid when title is not present" do post = build(:post, title: nil)

        expect(post.valid?).to eq(false)
      end
    end

    context "user role to create covid-19 news" do
      describe "regular user" do
        let(:user) { create(:user, role: 0, password: "my-secret-password") }
        let(:post) { build(:post, user: user, post_type: Post::TYPE_LIST["covid-19"][0]) }

        it "can't create post with covid-19 category" do
          result = post.save

          expect(result).to eq(false)
          expect(post.errors[:post_type].first).to eq('Post type must be one of the public')
          expect(Post.count).to eq(0)
        end
      end

      describe "health-admin user" do
        let(:user) { create(:user, role: 1, password: "my-secret-password") }
        let(:post) { build(:post, user: user, post_type: Post::TYPE_LIST["covid-19"][0]) }

        it "can create post with covid-19 category" do
          result = post.save

          expect(result).to eq(true)
          expect(Post.count).to eq(1)
        end
      end

      describe "super-admin user" do
        let(:user) { create(:user, role: 2, password: "my-secret-password") }
        let(:post) { build(:post, user: user, post_type: Post::TYPE_LIST["covid-19"][0]) }

        it "can create post with covid-19 category" do
          result = post.save

          expect(result).to eq(true)
          expect(Post.count).to eq(1)
        end
      end
    end

    context "scope methods" do
      let!(:public_post_one) { create(:post, post_type: Post::TYPE_LIST['public'][0]) }
      let!(:public_post_two) { create(:post, post_type: Post::TYPE_LIST['public'][1], region_id: 1) }
      let(:user) { create(:user, role: 2) }
      let!(:corona_post_one) { create(:post, post_type: Post::TYPE_LIST['covid-19'][0], user: user) }
      let!(:corona_post_two) { create(:post, post_type: Post::TYPE_LIST['covid-19'][1], user: user) }

      describe ".public_post" do
        it "can get public posts" do
          expect(Post.count).to eq(4)

          posts = Post.public_post

          expect(posts.count).to eq(2)
          expect(posts).to include(public_post_one)
          expect(posts).to include(public_post_two)
        end
      end

      describe ".corona_post" do
        it "can get public posts" do
          expect(Post.count).to eq(4)

          posts = Post.corona_post

          expect(posts.count).to eq(2)
          expect(posts).to include(corona_post_one)
          expect(posts).to include(corona_post_two)
        end
      end

      describe ".search_by_post_type" do
        it "can get posts with given post_type" do
          posts = Post.search_by_post_type('News')

          expect(posts).to include(public_post_one)
        end
      end

      describe ".search_by_region" do
        it "can get posts with given region id" do
          posts = Post.search_by_region(1)

          expect(posts).to include(public_post_two)
        end
      end
    end

    describe "#author_name" do
      it "can get name of the author" do
        user = create(:user, first_name: "Me", last_name: "Shopify")
        post = create(:post, user: user)

        expect(post.author_name).to eq("Me Shopify")
      end
    end

    context ".search_filters" do
      let!(:public_post_one) { create(:post, post_type: Post::TYPE_LIST['public'][0]) }
      let!(:public_post_two) { create(:post, post_type: Post::TYPE_LIST['public'][1], region_id: 1) }
      let(:user) { create(:user, role: 2) }
      let!(:corona_post_one) { create(:post, post_type: Post::TYPE_LIST['covid-19'][0], user: user) }
      let!(:corona_post_two) { create(:post, post_type: Post::TYPE_LIST['covid-19'][1], user: user) }

      describe "filter by nothing" do
        it "gets all the data" do
          posts = Post.search_filters

          expect(Post.count).to eq(4)
          expect(posts.count).to eq(4)
        end
      end

      describe "filter by empty hash" do
        it "gets all the data" do
          filter = {}
          posts = Post.search_filters(filter)

          expect(Post.count).to eq(4)
          expect(posts.count).to eq(4)
        end
      end

      describe "filter by unknown hash" do
        it "gets all the data" do
          filter = { kategori: Post::TYPE_LIST["public"][1] }
          posts = Post.search_filters(filter)

          expect(Post.count).to eq(4)
          expect(posts.count).to eq(4)
        end
      end

      describe "filter by category" do
        it "can get posts on given category" do
          filter = { category: Post::TYPE_LIST["public"][0] }
          posts = Post.search_filters(filter)

          expect(posts.count).to eq(1)
          expect(posts).to include(public_post_one)
        end
      end

      describe "filter by region" do
        it "can get posts on given region" do
          filter = { region: 1 }
          posts = Post.search_filters(filter)

          expect(posts.count).to eq(1)
          expect(posts).to include(public_post_two)
        end
      end

      describe "filter by category and region" do
        it "can get posts on given category" do
          filter = { category: Post::TYPE_LIST["public"][1], region: 1 }
          posts = Post.search_filters(filter)

          expect(posts.count).to eq(1)
          expect(posts).to include(public_post_two)
        end
      end
    end

    context ".search_order" do
      let!(:public_post_one) { create(:post, post_type: Post::TYPE_LIST['public'][0]) }
      let!(:public_post_two) { create(:post, post_type: Post::TYPE_LIST['public'][1]) }
      let(:user) { create(:user, role: 2) }
      let!(:corona_post_one) { create(:post, post_type: Post::TYPE_LIST['covid-19'][0], user: user) }
      let!(:corona_post_two) { create(:post, post_type: Post::TYPE_LIST['covid-19'][1], user: user) }

      describe "order by none" do
        it "ordered by default created_at desc" do
          posts = Post.search_order

          expect(posts.first).to eq(corona_post_two)
          expect(posts.second).to eq(corona_post_one)
          expect(posts.third).to eq(public_post_two)
          expect(posts.fourth).to eq(public_post_one)
        end
      end

      describe "order by newest" do
        it "ordered by created_at desc" do
          posts = Post.search_order("newest")

          expect(posts.first).to eq(corona_post_two)
          expect(posts.second).to eq(corona_post_one)
          expect(posts.third).to eq(public_post_two)
          expect(posts.fourth).to eq(public_post_one)
        end
      end

      describe "order by oldest" do
        it "ordered by created_at asc" do
          posts = Post.search_order("oldest")

          expect(posts.first).to eq(public_post_one)
          expect(posts.second).to eq(public_post_two)
          expect(posts.third).to eq(corona_post_one)
          expect(posts.fourth).to eq(corona_post_two)
        end
      end

      describe "order by others" do
        it "ordered by created_at desc" do
          posts = Post.search_order("you-can-order-by-anything")

          expect(posts.first).to eq(corona_post_two)
          expect(posts.second).to eq(corona_post_one)
          expect(posts.third).to eq(public_post_two)
          expect(posts.fourth).to eq(public_post_one)
        end
      end
    end

  end
end
