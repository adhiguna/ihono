require 'rails_helper'

RSpec.describe Organisation, type: :model do
  let(:organisation) { create(:organisation) }
  let(:organisation_with_image) { create(:organisation, :with_image) }

  it "has a valid factory" do
    expect {
      expect(create(:organisation)).to be_valid
    }.to change{Organisation.count}.by(1)
  end

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:regions) }
    it { should have_and_belong_to_many(:rohes) }
    it { should have_and_belong_to_many(:iwis) }
    it { should have_many(:locations) }
    it { should accept_nested_attributes_for(:locations) }
  end

  it { should validate_presence_of(:name) }
  it { should validate_inclusion_of(:organisation_type).in_array(Organisation::TYPE_LIST) }

  describe "private methods" do
    DEFAULT_HERO_IMAGE_JSON = { "id"=>"organisations//1.jpg", "storage"=>"cache", "metadata"=>{ "filename"=>"1.jpg", "size"=>63083, "mime_type"=>"image/jpeg", "width"=>771, "height"=>411 }}
 
    it "should change metadata for hero image" do
      expect(JSON.parse(organisation_with_image.hero_image_data)).to eq(DEFAULT_HERO_IMAGE_JSON)
      organisation_with_image.update(hero_image_crop_x: 1, hero_image_crop_y: 2, hero_image_crop_h: 1, hero_image_crop_w: 2)

      updated_json = DEFAULT_HERO_IMAGE_JSON
      updated_json["metadata"]["cropping"] = {
        'x' => 1,
        'y' => 2,
        'h' => 1,
        'w' => 2,
      }

      expect(JSON.parse(organisation_with_image.hero_image_data)).to eq(updated_json)
    end
  end

  describe "InfoMap" do
    before do
      organisation = create(:organisation, latitude: nil, longitude: nil, geocoder_off: true)
    end

    it "should only return organisation with lat long not nil" do
      expect(Organisation.get_base_data).to eq([organisation])
      expect(Organisation.count).to eq(2)
      organisation.locations.create(attributes_for(:location))
      expect(Organisation.get_base_data.count).to eq(2)
    end

    it "should not raise any error and return correct marker counts" do
      expect(Organisation.extract_data.count).to eq(0)
      2.times { organisation.locations.create(attributes_for(:location)) }
      expect(Organisation.extract_data.count).to eq(3)
    end
  end
end
