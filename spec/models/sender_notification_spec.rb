require 'rails_helper'

RSpec.describe SenderNotification, type: :model do
  describe "ActiveRecord associations" do
    it { should belong_to(:user) }
    it { should have_one(:notification) }
  end
end
