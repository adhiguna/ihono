require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }
  it "has valid factory" do
    expect(build(:user)).to be_valid
    expect(build(:user, :with_avatar)).to be_valid
    expect(build(:user, :with_geocode)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_many(:posts) }
    it { should have_many(:organisations) }
    it { should have_many(:receiver_notifications) }
    it { should have_many(:receiver_notify) }
    it { should have_and_belong_to_many(:post_favorites) }
    it { should have_and_belong_to_many(:post_likes) }
    it { should have_many(:social_media) }
    it { should accept_nested_attributes_for(:social_media) }
    it { have_many(:lineages) }
    it { should have_and_belong_to_many(:friends) }
  end

  describe "ActiveRecord validations" do
    before { user }
    it { should validate_uniqueness_of(:username) }
    it "validate whakapapa" do
      user = build(:user, lineages_attributes: [])
      expect(user.valid?).to be_truthy
    end
  end

  describe "instance method" do
    it "should show correct full_name" do
      expect(user.full_name).to eq(user.email)
      user.assign_attributes(first_name: 'First')
      expect(user.full_name).to eq('First')
      user.assign_attributes(last_name: 'Last')
      expect(user.full_name).to eq('First Last')
      user.assign_attributes(first_name: nil)
      expect(user.full_name).to eq('Last')
      expect(user.full_name).to eq(user.name)
    end

    it "should toggle favourite post" do
      post = create(:post)
      expect {
        user.toggle_favorite_post!(post)
      }.to change{user.post_favorites.count}.by(1)
      expect(user.post_favorite?(post)).to be_truthy
      expect {
        user.toggle_favorite_post!(post)
      }.to change{user.post_favorites.count}.by(-1)
      expect(user.post_favorite?(post)).to be_falsey
    end

    it "should toggle like post" do
      post = create(:post)
      expect {
        user.toggle_like_post!(post)
      }.to change{user.post_likes.count}.by(1)
      expect(user.post_like?(post)).to be_truthy
      expect {
        user.toggle_like_post!(post)
      }.to change{user.post_likes.count}.by(-1)
      expect(user.post_like?(post)).to be_falsey
    end

    it "should return username or email ( prioritize username )" do
      expect(user.login).to eq(user.username)
      user.username = nil
      expect(user.login).to eq(user.email)
    end

    it "should mark all notification as read" do
      create(:user_invitation_friendship, user_friend: user)
      expect(user.reload.receiver_notifications_count).to eq(1)
      user.read_all_notification
      expect(user.reload.receiver_notifications_count).to eq(0)
    end

    it "should not raise an error" do
      uif = create(:user_invitation_friendship, user_friend: user)
      expect(user.invitation_friendships.count).to eq(1)
      expect(user.already_friend?(uif.user)).to be_falsey
      user.accept_invite_friend(uif.user)
      expect(user.already_friend?(uif.user)).to be_truthy
      user.remove_friend(uif.user)
      expect(user.already_friend?(uif.user)).to be_falsey
      uif.user.invite_friend(user)
      expect(user.reload.invitation_friendships.count).to eq(1)
      user.reject_invite_friend(uif.user)
      user.uninvite_friend(uif.user)
      expect(user.reload.invitation_friendships.count).to eq(0)
      uif.user.invite_friend(user)
    end
  end

  describe "class method" do
    it "return user object when passing login, username or email" do
      expect(User.find_for_database_authentication(login: user.username)).to eq(user)
      expect(User.find_for_database_authentication(username: user.username)).to eq(user)
      expect(User.find_for_database_authentication(email: user.email)).to eq(user)
    end

    it "should not raise error when extract_marker_info for maps" do
      create(:user, :with_geocode)
      results = User.extract_data(nil, nil, 51)
      expect(results.count).to eq(1)
    end
  end

  describe "callback" do
    DEFAULT_AVATAR_JSON = { "id"=>"users//1.jpg", "storage"=>"cache", "metadata"=>{ "filename"=>"1.jpg", "size"=>63083, "mime_type"=>"image/jpeg", "width"=>771, "height"=>411 }}

    it "should change metadata for hero image" do
      user = create(:user, :with_avatar)
      expect(JSON.parse(user.avatar_data)).to eq(DEFAULT_AVATAR_JSON)
      user.update(avatar_crop_x: 1, avatar_crop_y: 2, avatar_crop_h: 1, avatar_crop_w: 2)

      updated_json = DEFAULT_AVATAR_JSON
      updated_json["metadata"]["cropping"] = {
        'x' => 1,
        'y' => 2,
        'h' => 1,
        'w' => 2,
      }

      expect(JSON.parse(user.avatar_data)).to eq(updated_json)
    end

  end
end
