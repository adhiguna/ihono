require 'rails_helper'

RSpec.describe Wharenui, type: :model do
  it "has a valid factory" do
    expect(build(:wharenui)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should have_and_belong_to_many(:maraes) }
    it { should have_and_belong_to_many(:hapus) }
    it { should have_and_belong_to_many(:iwis) }
    it { should have_and_belong_to_many(:rohes) }
    it { should have_many(:organisations) }
    it { should have_many(:regions) }
  end

  it { should validate_presence_of(:name) }
end
