require 'rails_helper'

RSpec.describe SocialMedia, type: :model do
  it "has a valid factory" do
    expect(build(:social_media)).to be_valid
  end

  describe "ActiveRecord associations" do
    it { should belong_to(:mediable) }
  end

  describe "ActiveRecord validations" do
    it { should validate_presence_of(:link) }
    it { should validate_presence_of(:name) }
    it { should validate_inclusion_of(:social_media_type).in_array(SocialMedia::SOCIAL_MEDIA_TYPE_LIST) }
  end
end
