require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:organisation) { create(:organisation) }
  let(:location) { organisation.locations.first }

  it "has a valid factory" do
    expect { expect(create(:organisation)).to be_truthy }.to change{Location.count}.by(1)
  end

  it { should validate_presence_of(:address) }
  it { should belong_to(:locationable) }

  describe "Geocoder" do
    # it "should decode address into latitude, longitude, postcode, suburb and country" do
    #   location.update(address: 'Kuala Lumpur')
    #   DEFAULT_GEOCODE.each do |key, value|
    #     expect(location[key]).not_to eq(value)
    #   end
    # end

    it "should not decode address into latitude, longitude, postcode, suburb and country" do
      location.update(address: 'Kuala Lumpur', turn_off_geocoder: true)
      DEFAULT_GEOCODE.each do |key, value|
        expect(location[key]).to eq(value)
      end
    end

    # it "should return only locations with latitude and longitude" do
    #   create(:location, locationable: organisation, turn_off_geocoder: true)
    #   expect(Location.all.length).to eq(2)
    #   expect(Location.latitude_longitude_present.length).to eq(1)
    # end
  end
end
