require 'rails_helper'

RSpec.describe Lineage, type: :model do
  it "has a valid factory" do
    expect(build(:lineage)).not_to be_valid
    expect(build(:lineage, :with_owner)).to be_valid
    expect(build(:lineage, :with_owner, :with_hapu)).to be_valid
  end

  describe "ActiveRecord relations" do
    it { should belong_to(:user) }
    it { should belong_to(:iwi) }
    it { should belong_to(:hapu).optional }
  end
end
