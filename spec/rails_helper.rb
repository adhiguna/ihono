require 'simplecov'
SimpleCov.add_filter(['spec', 'config', 'app/workers', 'app/uploaders'])
SimpleCov.add_group('Models', 'app/models')
SimpleCov.add_group('Controllers', 'app/controllers')
SimpleCov.start
# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'

require 'support/factory_bot'
require 'support/custom_input_support'
require 'support/auth_support'
require 'support/ajax_support'
require 'capybara/rspec'
require 'webdrivers/chromedriver'
require File.expand_path('../config/environment', __dir__)
Capybara.server_port = 8080
Capybara.app_host = 'http://localhost:8080'

# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'

DEFAULT_GEOCODE = { latitude: -41.3174262, longitude: 174.7816931, postcode: "6021", suburb: "Wellington", country: "New Zealand" }

# Add additional requires below this line. Rails is not loaded until this point!
# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.

# browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
#   opts.args << '--window-size=1920,1080'
#   opts.args << '--blink-settings=imagesEnabled=false'
# end

# Capybara.register_driver :chrome do |app|
#   Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
# end

# Capybara.javascript_driver = :selenium_chrome_headless

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end
RSpec.configure do |config|
  config.include Devise::Test::IntegrationHelpers, type: :feature
  config.include Devise::Test::IntegrationHelpers, type: :system
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")

  # config.after(:each, type: :system) do
  #   errors = page.driver.browser.manage.logs.get(:browser)
  #   if errors.present?
  #     aggregate_failures 'javascript errrors' do
  #       errors.each do |error|
  #         expect(error.level).not_to eq('SEVERE'), error.message
  #         next unless error.level == 'WARNING'
  #         STDERR.puts 'WARN: javascript warning'
  #         STDERR.puts error.message
  #       end
  #     end
  #   end
  # end

  config.before(:each, type: :system) do
    driven_by :selenium_chrome_headless
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
