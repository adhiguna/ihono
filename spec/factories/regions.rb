FactoryBot.define do
  factory :region do
    name { "Sendai" }
    slug { "sendai" }
  end
end
