FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    username { Faker::Internet.username }
    lineages_attributes { [attributes_for(:lineage)] }

    trait :for_search do
      email { 'test@email.com' }
      first_name { 'First Name' }
      last_name { 'Last Name' }
      abstract { 'Abstract for testing' }
      password { 'd3v3l0pm3nt' }
    end
    trait :without_lineage do
      lineages_attributes { [] }
    end
    trait :with_avatar do
      avatar { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', '1.jpg'), 'image/jpg')  }
    end
    trait :with_geocode do
      latitude { -44.7031813 }
      longitude { 169.1320981 }
    end
  end

  factory :user_invitation_friendship do
    user { create(:user) }
    user_friend { create(:user) }
    after :build do |uif|
      uif.request_by_id = uif.user.id
    end
  end
end
