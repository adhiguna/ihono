FactoryBot.define do
  factory :iwi do
    name { Faker::Movies::StarWars.character }
    abstract { Faker::Quote.matz }
    contact { Faker::Movies::StarWars.specie }
  end
end
