FactoryBot.define do
  factory :health_provider do
    name { "Health Provider One" }
    postcode { "123321" }
    slug { "health-provider-one" }
    suburb { "Utawi" }
    region { "Northland" }
    service { "Covid - 19 testing & other health checks" }
    address { "Long address in Ottawa" }
    contact { "1-22-333-4444-55555" }
    country { "Canada" }
    abstract { "Abstract" }
    open { Time.now }
    close { Time.now }
    allow_appointment { true }
  end
end
