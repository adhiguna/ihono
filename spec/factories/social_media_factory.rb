FactoryBot.define do
  factory :social_media do
    name { Faker::Internet.username }
    link { 'https://www.google.com' }
    social_media_type { SocialMedia::SOCIAL_MEDIA_TYPE_LIST[0] }
    mediable { create(:user) }
  end
end
