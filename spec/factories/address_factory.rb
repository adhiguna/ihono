FactoryBot.define do
  factory :location do
    address { 'Horner st, New Zaeland' }
    contact { 'link@hyrule.com' }
    turn_off_geocoder { true }
    latitude { DEFAULT_GEOCODE[:latitude] }
    longitude { DEFAULT_GEOCODE[:longitude] }
    postcode { DEFAULT_GEOCODE[:postcode] }
    suburb { DEFAULT_GEOCODE[:suburb] }
    country { DEFAULT_GEOCODE[:country] }
  end
end
