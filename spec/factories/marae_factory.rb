FactoryBot.define do
  factory :marae do
    name { Faker::Movies::StarWars.character } 
    address { 'Wellington, New Zaeland' }
    contact { Faker::Internet.email }
    turn_off_geocoder{ true }
    latitude { DEFAULT_GEOCODE[:latitude] } 
    longitude { DEFAULT_GEOCODE[:longitude] } 
    postcode { DEFAULT_GEOCODE[:postcode] }
    suburb { DEFAULT_GEOCODE[:suburb] }
    country { DEFAULT_GEOCODE[:country] }
  end
end
