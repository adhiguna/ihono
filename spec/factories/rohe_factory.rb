FactoryBot.define do
  factory :rohe do
    name { Faker::Movies::StarWars.planet }
    abstract { Faker::Movies::StarWars.wookiee_sentence }
    contact { Faker::Internet.email }
  end
end
