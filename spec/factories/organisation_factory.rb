FactoryBot.define do
  factory :organisation do
    user { create(:user) }
    name { Faker::Movies::StarWars.droid }
    organisation_type { Organisation::TYPE_LIST[0] }
    transient do
      geocoder_off { true }
      latitude { DEFAULT_GEOCODE[:latitude] }
      longitude { DEFAULT_GEOCODE[:longitude] }
      postcode { DEFAULT_GEOCODE[:postcode] }
      suburb { DEFAULT_GEOCODE[:suburb] }
      country { DEFAULT_GEOCODE[:country] }
    end

    after :create do |org, options|
      create :location, 
      address: 'Wellington, New Zaeland',
      latitude: options.latitude, 
      longitude: options.longitude, 
      postcode: options.postcode,
      suburb: options.suburb,
      country: options.country,
      locationable: org,
      turn_off_geocoder: options.geocoder_off
    end

    trait :with_image do
      hero_image { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', '1.jpg'), 'image/jpg')  }
    end
  end
end
