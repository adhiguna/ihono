FactoryBot.define do
  factory :lineage do
    whanau { Faker::Movies::StarWars.character }
    iwi { create(:iwi) }

    trait :with_owner do
      user { create(:user) }
    end
    trait :with_hapu do
      hapu { create(:hapu) }
    end
  end
end
