FactoryBot.define do
  factory :post do
    title { Faker::Movies::StarWars.specie }
    content { Faker::Quote.matz }
    user { create(:user) }
    post_type { Post::TYPE_LIST['public'][0] }
  end

  factory :post_notification, class: 'Notification' do
    notifiable { create(:post) }
    notifiable_status { 'create' }
    before :create do |n|
      n.build_sender(user: create(:user))
      n.receivers.build(user: create(:user))
    end
  end
end
