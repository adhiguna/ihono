module AuthSupport
  def do_login(login)
    find("#user_login").set(login)
    find("#user_password").set('d3v3l0pm3nt')
    click_button 'Log in'
  end

  def do_logout
    click_link('navToggle')
    click_link('Signout')
  end

  def validate_otp(code)
    find("#code").set(code)
    click_button 'Submit'
  end

  def whole_login_process(user)
    sign_in(user)
    visit root_path
    # validate_otp(user.reload.direct_otp)
  end
end

RSpec.configure do |config|
  config.include AuthSupport, type: :feature
  config.include AuthSupport, type: :system
end
