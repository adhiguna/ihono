module AjaxSupport
  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def finished_all_ajax_requests?
    page.evaluate_script('$.active').zero?
  end

  def wait_for_turbolinks
    if page.has_css?('div.turbolinks-progressbar')
      Timeout.timeout(Capybara.default_max_wait_time) do
        loop until !page.has_css?('div.turbolinks-progressbar')
      end
    end
  end

  def wait_until_ready
    wait_for_turbolinks && wait_for_ajax
  end
end

RSpec.configure do |config|
  config.include AjaxSupport, type: :feature
  config.include AjaxSupport, type: :system
end
