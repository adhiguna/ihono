module CustomInputSupport
  def fill_select2(parent_class, value)
    first("#{parent_class} .select2-container", minimum: 1).click
    find('.select2-search__field').send_keys(value, :enter)
  end

  def fill_quill(value)
    find('.ql-editor', minimum: 1).click
    find('.ql-editor p').send_keys(value)
  end

  def fill_post_form(title, description)
    fill_in 'Title', with: title
    fill_quill description
    attach_file('post[hero_image]', Rails.root.join('spec/fixtures/1.jpg'))
  end

  def click_back
    find(:css, '.navbar.fixed-top.navbar-dark.bg-black a', match: :first).click
  end
end

RSpec.configure do |config|
  config.include CustomInputSupport, type: :feature
  config.include CustomInputSupport, type: :system
end
