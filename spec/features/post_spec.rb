require 'rails_helper'

RSpec.describe "Post management", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt') }

  describe "as logged in user" do
    before do
      @post = create(:post, user: user)
      whole_login_process(user)
    end

    it "can not create post" do
      find_link('create-post-link').click

      fill_post_form("", Faker::Quote.yoda)
      click_button 'Create Post'

      message = find("#post_title").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")

      click_link 'navToggle'
      click_link 'My Posts'

      expect(page).to have_selector 'tbody tr', count: 1
    end

    it "can create post" do
      find_link('create-post-link').click

      fill_post_form("Booo, I'm writing by my self", Faker::Quote.yoda)
      click_button 'Create Post'

      click_link 'navToggle'
      click_link 'My Posts'

      expect(page).to have_selector 'tbody tr', count: 2
    end

    it "can update their own post" do
      click_link 'navToggle'
      click_link 'My Posts'

      find(:xpath, "//a[@href='#{edit_post_path(@post)}']").click
      fill_post_form("Booo, I'm writing by my self", Faker::Quote.yoda)
      click_button 'Update Post'
      expect(page).to have_content("Booo, I'm writing by my self")
    end

    it "can not update their own post with wrong values" do
      click_link 'navToggle'
      click_link 'My Posts'
      find(:xpath, "//a[@href='#{edit_post_path(@post)}']").click

      fill_post_form("", Faker::Quote.yoda)
      click_button 'Update Post'

      message = find("#post_title").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")
    end

    it "can't update other user post" do
      post = create(:post)
      visit edit_post_path(post)
      expect(current_path).to eq(root_path)
    end

    it "can delete their own post" do
      click_link('navToggle')
      click_link('My Posts')

      accept_confirm do
        click_link('Delete')
      end

      expect(page).to have_selector 'tbody tr', count: 0
    end

    it "can like post" do
      visit posts_path
      find(:xpath, "//a[@href='#{post_path(@post)}']").click
      click_link('Like')

      within("#like_post_#{@post.id}") do
        expect(page).to have_selector(".fa.activated.fa-thumbs-up", count: 1)
      end
    end

    it "can unlike post" do
      visit posts_path
      user.toggle_like_post!(@post)
      find(:xpath, "//a[@href='#{post_path(@post)}']").click
      click_link('Like')

      within("#like_post_#{@post.id}") do
        expect(page).to have_selector(".far.fa-thumbs-up", count: 1)
      end
    end

    it "can set post as favorite" do
      visit posts_path
      find(:xpath, "//a[@href='#{post_path(@post)}']").click
      click_link('Favorite')

      within("#favorite_post_#{@post.id}") do
        expect(page).to have_selector(".fa.activated.fa-heart", count: 1)
      end

      find(:xpath, "//a[@href='#{favorites_posts_path}']").click
      expect(page).to have_selector('.article.article-index', count: 1)
    end

    it "can unset post as favorite" do
      visit posts_path
      user.toggle_favorite_post!(@post)
      find(:xpath, "//a[@href='#{post_path(@post)}']").click
      click_link('Favorite')

      within("#favorite_post_#{@post.id}") do
        expect(page).to have_selector(".fa.activated.fa-heart", count: 0)
        expect(page).to have_selector(".far.fa-heart", count: 1)
      end
    end

    it "can access posts list page" do
      visit posts_path
      find(:xpath, "//a[@href='#{post_path(@post)}']").click
      find(:xpath, "//a[@id='list-post-icon']").click
      expect(page).to have_selector('.article.article-index', count: 2)
    end
  end

  describe "as guest user" do
    it "can not access edit post page" do
      post = create(:post)
      visit edit_post_path(post)
      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content('You need to sign in or sign up before continuing.')
    end

    it "can visit post page but can't favorite nor like" do
      post = create(:post)
      visit posts_path
      find(:xpath, "//a[@href='#{post_path(post)}']").click
      expect(page).to have_content(post.title)

      click_link 'Favorite'
      expect(current_path).to eq(new_user_session_path)

      visit posts_path
      find(:xpath, "//a[@href='#{post_path(post)}']").click
      expect(page).to have_content(post.title)

      click_link 'Like'
      expect(current_path).to eq(new_user_session_path)
    end

    it "can not access my_post page" do
      visit my_posts_posts_path
      expect(current_path).to eq(new_user_session_path)
    end
  end
end
