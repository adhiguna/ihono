require 'rails_helper'

RSpec.describe "Notifications", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt') }
  let(:friend) { create(:user) }

  describe "as guest user" do
    it "can't access notifications page" do
      visit notifications_path
      expect(current_path).to eq(new_user_session_path)
    end
  end

  describe "as logged in user" do
    before do
      whole_login_process(user)
      friend.invite_friend(user)
    end

    it "can see listing user friend and reject" do
      find(:css, '.notification').click

      expect(page).to have_selector('.list-group-item', count: 1)

      click_link(friend.name)

      expect(page).to have_content('Accept')
      expect(page).to have_content('Reject')

      accept_confirm do
        click_link('Reject')
      end

      expect(page).to have_content('Connect')

      click_link('Connect')
      expect(page).to have_content('Cancel Request')

      click_link('Cancel Request')
      expect(page).to have_content('Connect')
    end

    it "can see listing user friend notifications and accept" do
      find(:css, '.notification').click
      expect(page).to have_selector('.list-group-item', count: 1)

      click_link(friend.name)

      expect(page).to have_content('Accept')
      expect(page).to have_content('Reject')

      accept_confirm do
        click_link('Accept')
      end

      expect(page).to have_content('Unfriend')

      accept_confirm do
        click_link('Unfriend')
      end

      expect(page).to have_content('Connect')

      click_link('Connect')
      expect(page).to have_content('Cancel Request')

      click_link('Cancel Request')
      expect(page).to have_content('Connect')
    end
  end
end
