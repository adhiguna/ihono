require 'rails_helper'

RSpec.describe "User creates corona news post", type: :system do
  context "as super-admin" do
    let(:user) { create(:user, first_name: "Agung", last_name: "Shopify", role: 2, password: 'my-secret-password') }

    it "can create post with covid-19 : Official government updates as its category" do
      sign_in user
      visit new_post_path

      fill_in "Title", with: "Shopify is a great company"
      fill_quill "Because they're using Ruby on Rails to power its user business"
      select "Official government updates"
      click_on "Create Post"

      expect(page).to have_content("Shopify is a great company")
      expect(page).to have_content("Because they're using Ruby on Rails to power its user business")
      expect(page).to have_content("Agung Shopify")
    end
  end

  context "as admin-health" do
    let(:user) { create(:user, first_name: "Agung", last_name: "GitLab", role: 1, password: 'my-secret-password') }

    it "can create post with covid-19 : Medical Service updates as its category" do
      sign_in user
      visit new_post_path

      fill_in "Title", with: "I'm joining GitLab"
      fill_quill "First day at GitLab, I'm sooo happy"
      select "Medical Service updates"
      click_on "Create Post"

      expect(page).to have_content("I'm joining GitLab")
      expect(page).to have_content("First day at GitLab, I'm sooo happy")
      expect(page).to have_content("Agung GitLab")
    end
  end

  context "as regular user" do
    let(:user) { create(:user, first_name: "Agung", last_name: "GitLab", role: 0, password: 'my-secret-password') }

    it "can't see covid-19 category options" do
      sign_in user
      visit new_post_path

      expect(page).to_not have_content("Official government updates")
      expect(page).to_not have_content("Medical Service updates")
      expect(page).to_not have_content("Whānau help and advice")
    end
  end
end
