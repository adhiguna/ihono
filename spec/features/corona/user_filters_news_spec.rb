require "rails_helper"

RSpec.describe "User filters news", type: :system do
  let(:user) { create(:user, role: 2) }

  context "Filter by category" do
    let!(:news_one) do
      create(:post, title: "Gov Update",
             post_type: Post::TYPE_LIST["covid-19"][0],
             user: user)
    end

    let!(:news_two) do
      create(:post, title: "Med Update",
             post_type: Post::TYPE_LIST["covid-19"][1],
             user: user)
    end

    let!(:news_three) do
      create(:post, title: "Whanau Update",
             post_type: Post::TYPE_LIST["covid-19"][2],
             user: user)
    end

    describe "Official government updates category" do
      it "can filter news from Official government updates" do
        visit corona_posts_path

        expect(page).to have_selector(".article-title", count: 3)

        click_on "CATEGORY"
        wait_until_ready
        choose "Official government updates"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_selector(".article-title", count: 1)
        expect(page).to have_content("Gov Update")
      end
    end

    describe "Medical Service updates category" do
      it "can filter news from Medical Service updates" do
        visit corona_posts_path

        expect(page).to have_selector(".article-title", count: 3)

        click_on "CATEGORY"
        wait_until_ready
        choose "Medical Service updates"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_selector(".article-title", count: 1)
        expect(page).to have_content("Med Update")
      end
    end

    describe "Whanau help and advice category" do
      it "can filter new from Whanau help and advice" do
        visit corona_posts_path

        expect(page).to have_selector(".article-title", count: 3)

        click_on "CATEGORY"
        wait_until_ready
        choose "Whānau help and advice"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_selector(".article-title", count: 1)
        expect(page).to have_content("Whanau Update")
      end
    end

    describe "All categories" do
      it "can filter news from all" do
        visit corona_posts_path

        expect(page).to have_selector(".article-title", count: 3)

        click_on "CATEGORY"
        wait_until_ready
        choose "Whānau help and advice"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_selector(".article-title", count: 1)

        click_on "CATEGORY"
        wait_until_ready
        choose "All"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_selector(".article-title", count: 3)
      end
    end
  end

  context "Filter by region" do
    let(:region) { create(:region, name: "Sendai") }

    let!(:news_one) do
      create(:post, title: "Gov Update",
             post_type: Post::TYPE_LIST["covid-19"][0],
             user: user)
    end

    let!(:news_two) do
      create(:post, title: "Med Update",
             post_type: Post::TYPE_LIST["covid-19"][1],
             user: user, region_id: region.id)
    end

    it "can filter by region" do
      visit corona_posts_path

      expect(page).to have_selector(".article-title", count: 2)

      click_on "REGION"
      wait_until_ready
      choose "Sendai"
      wait_until_ready
      click_on "Save"
      wait_until_ready

      expect(page).to have_selector(".article-title", count: 1)
    end

    it "can filter by all" do
      visit corona_posts_path

      expect(page).to have_selector(".article-title", count: 2)

      click_on "REGION"
      wait_until_ready
      choose "Sendai"
      wait_until_ready
      click_on "Save"
      wait_until_ready

      expect(page).to have_selector(".article-title", count: 1)

      click_on "REGION"
      wait_until_ready
      choose "All"
      wait_until_ready
      click_on "Save"
      wait_until_ready

      expect(page).to have_selector(".article-title", count: 2)
    end
  end
end
