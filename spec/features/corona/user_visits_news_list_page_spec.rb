require "rails_helper"

RSpec.describe "User visits covid-19 news page", type: :system do

  context "Viewing news" do

    describe "no news available" do
      it "can see that there's no news" do
        visit corona_posts_path

        expect(page).to have_selector(".article-title", count: 0)
        expect(page).to have_content("No more posts")
      end
    end

    describe "news available" do

      context "news from Official government updates" do
        let(:user) { create(:user, role: 2) }
        let!(:news_one) do
          create(:post, title: "All izz well",
                 post_type: Post::TYPE_LIST["covid-19"][0],
                 user: user)
        end

        let!(:news_two) do
          create(:post, title: "Use mask",
                 post_type: Post::TYPE_LIST["covid-19"][0],
                 user: user)
        end

        it "can see that there're two news" do
          visit corona_posts_path

          expect(page).to have_selector(".article-title", count: 2)
          expect(page).to have_content("All izz well")
          expect(page).to have_content("Use mask")
          expect(page).to have_content("No more posts")
        end
      end

      context "news from Medical Service updates" do
        let(:user) { create(:user, role: 2) }
        let!(:news_one) do
          create(:post, title: "Social Distancing",
                 post_type: Post::TYPE_LIST["covid-19"][1],
                 user: user)
        end

        let!(:news_two) do
          create(:post, title: "Stay Home",
                 post_type: Post::TYPE_LIST["covid-19"][1],
                 user: user)
        end

        it "can see that there're two news" do
          visit corona_posts_path

          expect(page).to have_selector(".article-title", count: 2)
          expect(page).to have_content("Social Distancing")
          expect(page).to have_content("Stay Home")
          expect(page).to have_content("No more posts")
        end
      end

      context "news from Whānau help and advice" do
        let(:user) { create(:user, role: 2) }
        let!(:news_one) do
          create(:post, title: "Eat chicken",
                 post_type: Post::TYPE_LIST["covid-19"][2],
                 user: user)
        end

        let!(:news_two) do
          create(:post, title: "Drink milk to stay healthy",
                 post_type: Post::TYPE_LIST["covid-19"][2],
                 user: user)
        end

        it "can see that there're two news" do
          visit corona_posts_path

          expect(page).to have_selector(".article-title", count: 2)
          expect(page).to have_content("Eat chicken")
          expect(page).to have_content("Drink milk to stay healthy")
          expect(page).to have_content("No more posts")
        end
      end

    end

  end
end
