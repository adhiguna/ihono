require "rails_helper"

RSpec.describe "User visits covid-19 index page", type: :system do
  it "Can see main menu" do
    visit root_path
    click_link "COVID-19"

    expect(page).to have_content("Covid-19 News")
    expect(page).to have_content("Get a Medical Checkup")
    expect(page).to have_content("Covid-19 Help & Advice")
  end
end
