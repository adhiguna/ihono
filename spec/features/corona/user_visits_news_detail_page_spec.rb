require "rails_helper"

RSpec.describe "User visits covid-19 news detail page", type: :system do
  let(:user) { create(:user, role: 2, first_name: "Agung", last_name: "Setiawan") }

  let!(:news) do
    create(:post, title: "Ruby on Rails 6 and RSpec 4",
           content: "Let's go to Japan for RubyKaigi 2021",
           post_type: Post::TYPE_LIST["covid-19"][0],
           user: user)
  end

  it "Can read a news" do
    visit corona_posts_path
    click_link "Ruby on Rails 6 and RSpec 4"

    expect(page).to have_content("Ruby on Rails 6 and RSpec 4")
    expect(page).to have_content("Let's go to Japan for RubyKaigi 2021")
    expect(page).to have_content("Agung Setiawan")
  end
end
