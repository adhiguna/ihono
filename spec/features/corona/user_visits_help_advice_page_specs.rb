require "rails_helper"

RSpec.describe "User visits covid-19 help and advice page", type: :system do
  let(:user) { create(:user, role: 2) }

  let!(:news_one) do
    create(:post, title: "Eat chicken",
           post_type: Post::TYPE_LIST["covid-19"][1],
           user: user)
  end

  let!(:news_two) do
    create(:post, title: "Drink milk to stay healthy",
           post_type: Post::TYPE_LIST["covid-19"][2],
           user: user)
  end

  it "can see news from Whanu help and advice" do
    visit corona_root_path
    click_link "Covid-19 Help & Advice"

    expect(page).to have_selector(".article-title", count: 1)
    expect(page).to have_content("Drink milk to stay healthy")
  end
end

