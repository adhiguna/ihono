require 'rails_helper'

RSpec.describe "Profile management", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt') }
  let(:iwi) { create(:iwi) }

  describe "as logged in user" do
    before do
      whole_login_process(user)

      find_link("navToggle").click

      click_link 'My Profile'
    end

    it "can update user address" do
      expect(page).to have_content('WHAKAPAPA')

      click_link 'Edit Profile'

      fill_in 'City', with: Faker::Movies::StarWars.planet
      fill_in 'Suburb', with: Faker::Movies::StarWars.planet

      click_on 'Update'

      expect(page).to have_content('CITY / SUBURB')
    end

    it "can update user description, interes, work, contact and Te Reo" do
      expect(page).to have_content('WHAKAPAPA')
      expect(page).not_to have_content('INTRODUCTION')
      expect(page).not_to have_content('INTEREST')
      expect(page).not_to have_content('WORK')
      expect(page).not_to have_content('CONTACT')
      expect(page).not_to have_content('TE REO')

      click_link 'Edit Profile'

      fill_in 'Introduction', with: Faker::Quote.yoda
      fill_in 'Interest', with: Faker::Quote.yoda
      fill_in 'Work', with: 'Ruby on Rails Developer'
      fill_in 'Contact', with: 'www.google.com'
      fill_in 'City', with: Faker::Movies::StarWars.planet
      fill_in 'Suburb', with: Faker::Movies::StarWars.planet
      choose 'Novice'
      click_on 'Update'

      expect(page).to have_content('WHAKAPAPA')
      expect(page).to have_content('INTRODUCTION')
      expect(page).to have_content('INTEREST')
      expect(page).to have_content('WORK')
      expect(page).to have_content('CONTACT')
      expect(page).to have_content('TE REO')
    end

    it "can remove and add WHAKAPAPA" do
      iwi
      click_link 'Edit Profile'

      fill_in 'Suburb', with: Faker::Movies::StarWars.planet
      fill_in 'City', with: Faker::Movies::StarWars.planet
      find(:css, 'a.remove_fields').click
      click_on 'Update'

      expect(page).not_to have_content('WHAKAPAPA')

      click_link 'Edit Profile'

      click_link 'Add Whakapapa'
      fill_select2('.user_lineages_iwi_id', iwi.name)
      fill_in 'Whānau Name', with: 'Testing'
      click_on 'Update'

      expect(page).to have_content('WHAKAPAPA')
    end

    it "can add / remove social media" do
      click_link 'Edit Profile'

      fill_in 'Suburb', with: Faker::Movies::StarWars.planet
      fill_in 'City', with: Faker::Movies::StarWars.planet
      click_on 'Add Social Media'

      within(:css, '#social-media') do
        select 'Facebook', from: 'Social media type'
        fill_in 'Name', with: 'Testing'
        fill_in 'Link', with: 'https://www.facebook.com'
      end

      click_on 'Update'

      expect(page).to have_content('SOCIAL MEDIA')
      click_link 'Edit Profile'

      within(:css, '#social-media') do
        find(:css, '.remove_fields').click
      end

      click_on 'Update'

      expect(page).not_to have_content('SOCIAL MEDIA')
    end
  end

  describe "as guest user" do
    before { user }

    it "can not access user profile page, edit profile page and profile index page" do
      visit user_path(user)
      expect(current_path).to eq(new_user_session_path)

      visit edit_users_profile_index_path
      expect(current_path).to eq(new_user_session_path)

      visit users_profile_index_path
      expect(current_path).to eq(new_user_session_path)
    end
  end
end
