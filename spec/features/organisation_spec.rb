require 'rails_helper'

RSpec.describe "Organisation management", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt', role: 2) }
  let(:organisation) { create(:organisation, name: 'Other organisation') }
  let(:click_admin_organisation_step_link) {
    find_link("navToggle").click
    find_link("treeViewAdmin").click
    click_link 'Organisations'
  }

  describe "as logged in user admin" do
    before do
      organisation
      whole_login_process(user)
    end

    it "should validate org name" do
      click_admin_organisation_step_link

      expect(page).to have_selector('tbody tr', count: 1)
      click_link 'new organisation'

      click_button 'Save'

      message = find("#organisation_name").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")
    end

    it "can create organisation" do
      click_admin_organisation_step_link

      expect(page).to have_selector('tbody tr', count: 1)

      click_link 'new organisation'

      fill_in 'Name', with: Faker::Movies::StarWars.character
      fill_in 'NZBN', with: Faker::Movies::StarWars.planet
      fill_in 'Abstract', with: Faker::Quote.yoda
      click_button 'Save'

      click_admin_organisation_step_link
      expect(page).to have_selector('tbody tr', count: 2)
    end

    it "can not update organisation when name is blank" do
      create(:organisation, user: user, name: 'Current User Organisation')

      click_admin_organisation_step_link

      expect(page).to have_selector('tbody tr', count: 2)
      expect(all('tbody tr')).not_to have_css('fa-edit')
      expect(all('tbody tr')).not_to have_css('fa-trash')
      within all('tbody tr').last do
        click_link 'Edit'
      end

      fill_in 'Name', with: ''
      click_button 'Save'

      message = find("#organisation_name").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")
    end

    it "can edit own organisation" do
      create(:organisation, user: user, name: 'Current User Organisation')

      click_admin_organisation_step_link

      expect(all('tbody tr')).not_to have_css('fa-edit')
      expect(all('tbody tr')).not_to have_css('fa-trash')
      within find('tbody tr', match: :first) do
        click_link 'Edit'
      end

      fill_in 'Name', with: 'Organisation new name'
      fill_in 'Abstract', with: Faker::Quote.yoda
      click_button 'Save'

      expect(page).to have_content('Organisation new name')
    end

    it "can destroy own organisation" do
      create(:organisation, user: user)

      click_admin_organisation_step_link

      within find('tbody tr', match: :first) do
        accept_confirm do
          click_link 'Delete'
        end
      end

      expect(all('tbody tr')).not_to have_css('fa-edit')
      expect(all('tbody tr')).not_to have_css('fa-trash')
      expect(page).to have_selector 'tbody tr', count: 1
    end

    it "can search organisation" do
      create(:organisation, user: user)

      click_admin_organisation_step_link

      expect(page).to have_selector 'tbody tr', count: 2

      find("#search_keyword").set('Other')

      click_on 'search'

      expect(page).to have_selector 'tbody tr', count: 1
    end
  end

  describe "as guest user" do
    it "can access organisation show" do
      visit organisation_path(organisation)
      expect(current_path).to eq(organisation_path(organisation))
    end

    it "can't access organisations [index]" do
      visit admin_organisations_path
      expect(current_path).to eq(new_user_session_path)
    end
  end
end
