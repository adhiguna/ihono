require 'rails_helper'

RSpec.describe "Authentication", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt') }

  it "should not register when only email passed" do
    iwi = create(:iwi)
    visit new_registration_path

    fill_in 'Email', with: 'test@email.com'
    click_button 'Submit'
  end

  it "should not raise error when do full registration flow" do
    iwi = create(:iwi)
    visit new_registration_path
    fill_in 'Email', with: 'test@email.com'
    click_button 'Submit'
    incomplete_user = User.first

    # find("#code").set(incomplete_user.direct_otp)
    # click_button 'Submit'
    expect(page).to have_content 'Tell us your name'

    within "#edit_user_#{incomplete_user.id}" do
      fill_in 'First name', with: Faker::Movies::StarWars.character
      fill_in 'Last name', with: "of #{Faker::Movies::StarWars.planet}"
      click_button 'Next'
    end

    expect(page).to have_content 'Username and password'

    within "#edit_user_#{incomplete_user.id}" do
      fill_in 'Username', with: "myusername"
      fill_in 'Password', with: "d3v3l0pm3nt"
      click_button 'Next'
    end

    expect(page).to have_content 'Thanks, that’s all we need to get you signed up for now.'
  end

  # it "need to verify OTP every 30 days" do
  #   visit new_user_session_path
  #   do_login(user.login)
  #   validate_otp(user.reload.direct_otp)
  #   expect(current_path).to eq(root_path)
  #   do_logout

  #   click_link('Login')
  #   do_login(user.login)
  #   expect(current_path).to eq(root_path)
  #   do_logout
  #   click_link('Login')

  #   Timecop.freeze(30.days.from_now) do
  #     do_login(user.login)
  #     expect(current_path).to eq(user_two_factor_authentication_path)
  #   end
  # end
end
