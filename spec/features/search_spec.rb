require 'rails_helper'

RSpec.describe "Search", type: :system do
  let(:user) { create(:user, :for_search) }

  describe "as guest user" do
    before { visit root_path }
    it "can not search user" do
      user
      find(:css, '#search-section-trigger input').click

      find(:css, '#search-input').send_keys('First')

      expect(page).not_to have_selector('.search-result-item')
    end

    it "can search marae" do
      find(:css, '#search-section-trigger input').click

      marae = create(:marae)

      find(:css, '#search-input').send_keys(marae.name)

      expect(page).to have_content('Marae (1 results)')

      click_link "SEE ALL RESULTS"

      expect(page).to have_content("Result of #{marae.name}")
      expect(page).to have_content(marae.name)
    end

    it "can search organisation" do
      find(:css, '#search-section-trigger input').click

      organisation = create(:organisation, geocoder_off: true)

      find(:css, '#search-input').send_keys(organisation.name)

      expect(page).to have_content("#{organisation.organisation_type} (1 results)")

      click_link "SEE ALL RESULTS"

      expect(page).to have_content("Result of #{organisation.name}")
      expect(page).to have_content(organisation.name)
    end
  end

  describe "as logged in user" do
    before do
      whole_login_process(user)
      find(:css, '#search-section-trigger input').click
    end

    it "can search user" do
      find(:css, '#search-input').send_keys('First')

      expect(page).to have_content('People (1 results)')

      click_link "SEE ALL RESULTS"

      expect(page).to have_content('Result of First')
      expect(page).to have_content(user.name)
    end

    it "can search post" do
      post = create(:post, title: 'Hello Test')

      find(:css, '#search-input').send_keys('Hello')

      expect(page).to have_content('Post (1 results)')

      click_link "SEE ALL RESULTS"

      expect(page).to have_content('Result of Hello')
      expect(page).to have_content(post.title)
    end
  end
end
