require "rails_helper"

RSpec.describe "User visits health provider list", type: :system do
  it "can visit from medical checkup page" do
    visit corona_root_path
    click_on "Get a Medical Checkup"
    click_on "Find providers from a list"

    expect(page).to have_current_path(corona_health_provider_list_path)
  end

  describe "registered health provider" do
    let!(:provider_one) { create(:health_provider, name: "Provider One", region: "Northland", address: "Address of the facility", abstract: "We are help for you") }
    let!(:provider_two) { create(:health_provider, region: "Nelson") }
    let!(:provider_three) { create(:health_provider, name: "Provider Three", region: "Auckland", address: "Address of the facility three", abstract: "Abstract Three") }
    let!(:provider_four) { create(:health_provider, name: "Provider Four", region: "Auckland", abstract: "Abstract Four") }
    let(:user) { create(:user, role: 2, password: "my-secret-password") }

    it "can see region with providers" do
      sign_in(user)
      visit corona_health_provider_list_path
      wait_until_ready

      expect(page).to have_content("Northland")
      expect(page).to have_content("Nelson")
      expect(page).to have_content("Auckland")
    end

    it "can see one provider on a region" do
      sign_in(user)
      visit corona_health_provider_list_path
      wait_until_ready

      click_on "Northland"
      wait_until_ready

      expect(page).to have_content("Provider One")
      expect(page).to have_content("Address of the facility")
      expect(page).to have_content("We are help for you")
      expect(page).to have_content("Request appointment")
    end

    it "can see multiple providers on a region" do
      sign_in(user)
      visit corona_health_provider_list_path
      wait_until_ready

      click_on "Auckland"
      wait_until_ready

      expect(page).to have_content("Provider Three")
      expect(page).to have_content("Address of the facility three")
      expect(page).to have_content("Abstract Three")

      expect(page).to have_content("Provider Four")
      expect(page).to have_content("Abstract Four")
    end
  end
end
