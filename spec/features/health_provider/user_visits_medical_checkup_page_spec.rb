require "rails_helper"

RSpec.describe "User visits medical checkup page", type: :system do
  it "can visit medical checkup page" do
    visit corona_root_path
    click_on "Get a Medical Checkup"

    expect(page).to have_content("Find a Māori Health Provider near you")
    expect(page).to have_content("Find providers on the map")
    expect(page).to have_content("Find providers from a list")
  end
end
