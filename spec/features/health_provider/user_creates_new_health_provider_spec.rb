require "rails_helper"

RSpec.describe "User creates new health provider", type: :system do
  let(:user) { create(:user, role: 2, password: "my-secret-password") }

  describe "with valid data" do
    it "can create new health provider data" do
      sign_in(user)

      visit admin_health_providers_path
      click_on "new health provider"
      wait_until_ready

      fill_in "health_provider_name", with: "Force One With You"
      find(:xpath, "//input[@name='pac-input']").set("Auckland")
      find('.pac-item', match: :first).click
      select "Northland"
      fill_in "health_provider[abstract]", with: "Detail of the location is here"
      select "Covid-19 testing & other health checks", from: "health_provider[service]"
      fill_in "health_provider[contact]", with: "1-22-333-4444-55555"
      page.execute_script("$('#health_provider_open').timepicker('setTime', '1:00 AM')")
      page.execute_script("$('#health_provider_close').timepicker('setTime', '1:00 AM')")
      click_on "Save"

      expect(page).to have_content("Health provider was successfully created.")
      expect(page).to have_current_path(admin_health_providers_path)
      expect(page).to have_content("Force One With You")
    end
  end

  describe "with invalid data" do
    it "fails to create new health provider data" do
      sign_in(user)

      visit admin_health_providers_path
      click_on "new health provider"
      wait_until_ready
      click_on "Save"

      message = find("#health_provider_name").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")
    end
  end
end
