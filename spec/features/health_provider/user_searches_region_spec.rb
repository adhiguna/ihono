require "rails_helper"

RSpec.describe "User searches region", type: :system do
  describe "filter by region name" do
    let!(:health_provider_one) { create(:health_provider, region: "Tasman") }
    let!(:health_provider_two) { create(:health_provider, region: "Nelson") }

    it "can get region by name" do
      visit corona_health_provider_list_path

      expect(page).to have_content("Tasman")
      expect(page).to have_content("Nelson")

      fill_in "search-section-health-provider-input", with: "Tas"

      expect(page).to have_content("Tasman")
      expect(page).to_not have_content("Nelson")
    end
  end
end
