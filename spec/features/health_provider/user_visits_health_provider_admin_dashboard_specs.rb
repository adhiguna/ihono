require "rails_helper"

RSpec.describe "user visits health provider admin dashboard", type: :system do
  describe "super-admin can visit from menu" do
    let(:user) { create(:user, role: 2, password: "my-secret-password") }

    it "can visit health provider admin dashboard" do
      sign_in(user)
      visit root_path
      click_link("navToggle")
      click_link("Admin")
      click_link("Medical Units")

      expect(page).to have_content("Health Providers")
      expect(page).to have_content("new health provider")
      expect(page).to have_current_path(admin_health_providers_path)
    end
  end

  context "visiting health provider admin dashboard directly" do
    describe "regular user" do
      let(:user) { create(:user, role: 0, password: "my-secret-password") }

      it "can't visit the page" do
        sign_in(user)

        visit admin_health_providers_path

        expect(page).to have_content("You are not authorized to perform this action.")
      end
    end

    describe "health-admin user" do
      let(:user) { create(:user, role: 1, password: "my-secret-password") }

      it "can't visit the page" do
        sign_in(user)

        visit admin_health_providers_path

        expect(page).to have_content("You are not authorized to perform this action.")
      end
    end

    describe "super-admin user" do
      let(:user) { create(:user, role: 2, password: "my-secret-password") }
      let(:health_provider_one) { create(:health_provider, name: "One with The Force") }
      let(:health_provider_two) { create(:health_provider, name: "We Like You") }

      it "can see list of health providers" do
        health_provider_one
        health_provider_two

        sign_in(user)

        visit admin_health_providers_path

        expect(page).to have_content("Health Providers")
        expect(page).to have_content("new health provider")
        expect(page).to have_content("One with The Force")
        expect(page).to have_content("We Like You")
      end

      it "can see text placeholder when there are no data" do
        sign_in(user)

        visit admin_health_providers_path

        expect(page).to have_content("No Result Found")
      end
    end
  end
end
