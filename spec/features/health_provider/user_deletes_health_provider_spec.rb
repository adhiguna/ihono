require "rails_helper"

RSpec.describe "User deletes health provider spec", type: :system do
  let(:user) { create(:user, role: 2, password: "my-secret-password") }
  let!(:health_provider) { create(:health_provider, name: "I'm The One") }

  it "can delete health provider" do
    sign_in(user)
    visit admin_health_providers_path

    expect(page).to have_content("I'm The One")

    accept_confirm do
      click_on "Delete"
    end

    expect(page).to have_content("Health provider was successfully destroyed.")
    expect(page).to have_content("No Result Found")
  end
end
