require "rails_helper"

RSpec.describe "User edits health provider", type: :system do
  let(:user) { create(:user, role: 2, password: "my-secret-password") }
  let!(:health_provider) { create(:health_provider, name: "I'm The One") }

  describe "with valid data" do
    it "can edit existing data" do
      sign_in(user)
      visit admin_health_providers_path

      expect(page).to have_content("I'm The One")

      click_on "Edit"

      fill_in "health_provider_name", with: "TWO TWO TWO"
      click_on "Save"

      expect(page).to have_content("Health provider was successfully updated.")
      expect(page).to have_content("TWO TWO TWO")
      expect(page).to_not have_content("I'm The One")
    end
  end

  describe "with invalid data" do
    it "can't edit existing data" do
      sign_in(user)
      visit admin_health_providers_path

      expect(page).to have_content("I'm The One")

      click_on "Edit"

      fill_in "health_provider_name", with: ""
      click_on "Save"

      message = find("#health_provider_name").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")
    end
  end
end
