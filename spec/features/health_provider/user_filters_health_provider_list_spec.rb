require "rails_helper"

RSpec.describe "User filters health provider list", type: :system do
  describe "click category filter link" do
    it "can see the filter options" do
      visit corona_health_provider_list_path

      click_on "CATEGORY"
      wait_until_ready

      expect(page).to have_content("All Medical Health Providers")
      expect(page).to have_content("Covid-19 testing only")
      expect(page).to have_content("Covid-19 testing & other health checks")
    end
  end

  context "filter by category" do
    let!(:provider_one) { create(:health_provider, name: "Provider One", service: "Covid-19 testing only", region: "Northland") }
    let!(:provider_two) { create(:health_provider, name: "Provider Two", service: "Covid-19 testing only", region: "Nelson") }
    let!(:provider_three) { create(:health_provider, name: "Provider Three", service: "Covid-19 testing & other health checks", region: "Nelson") }
    let!(:provider_four) { create(:health_provider, name: "Provider Four", service: "Covid-19 testing & other health checks", region: "Auckland") }
    let!(:provider_five) { create(:health_provider, name: "Provider Five", service: "Covid-19 testing & other health checks", region: "Auckland") }

    describe "filter by Covid-19 testing only" do
      it "shows region with testing only health provider" do
        visit corona_health_provider_list_path

        expect(page).to have_content("Northland")
        expect(page).to have_content("Nelson")
        expect(page).to have_content("Auckland")

        click_on "CATEGORY"
        wait_until_ready
        choose "Covid-19 testing only"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_content("Northland")
        expect(page).to have_content("Nelson")
      end

      it "shows testing only health provider" do
        visit corona_health_provider_list_path

        click_on "CATEGORY"
        wait_until_ready
        choose "Covid-19 testing only"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        click_on "Nelson"
        wait_until_ready

        expect(page).to have_content("Provider Two")
        expect(page).to_not have_content("Provider Three")
      end
    end

    describe "filter by Covid-19 testing & other health checks" do
      it "shows region with testing & other health checks provider" do
        visit corona_health_provider_list_path

        expect(page).to have_content("Northland")
        expect(page).to have_content("Nelson")
        expect(page).to have_content("Auckland")

        click_on "CATEGORY"
        wait_until_ready
        choose "Covid-19 testing & other health checks"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        expect(page).to have_content("Nelson")
        expect(page).to have_content("Auckland")
      end

      it "shows testing & other health checks provider" do
        visit corona_health_provider_list_path

        click_on "CATEGORY"
        wait_until_ready
        choose "Covid-19 testing & other health checks"
        wait_until_ready
        click_on "Save"
        wait_until_ready

        click_on "Nelson"
        wait_until_ready

        expect(page).to_not have_content("Provider Two")
        expect(page).to have_content("Provider Three")
      end
    end
  end

  context "filter by distance" do
    let!(:health_provider_one) { create(:health_provider, region: "Tasman") }
    let!(:health_provider_two) { create(:health_provider, region: "Nelson") }

    it "can see the filter options" do
      visit corona_health_provider_list_path

      click_on "SORT"
      wait_until_ready

      expect(page).to have_content("Within 1km")
      expect(page).to have_content("Within 10km")
      expect(page).to have_content("Within 50km")
      expect(page).to have_content("50km or more")
    end

    it "can't find providers within range" do
      visit corona_health_provider_list_path

      expect(page).to have_content("Tasman")
      expect(page).to have_content("Nelson")

      click_on "SORT"
      wait_until_ready
      choose "Within 1km"
      wait_until_ready
      click_on "Save"
      wait_until_ready

      expect(page).to_not have_content("Tasman")
      expect(page).to_not have_content("Nelson")
    end
  end
end
