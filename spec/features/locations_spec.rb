require 'rails_helper'

RSpec.describe "Locations management", type: :system do
  let(:user) { create(:user, password: 'd3v3l0pm3nt', role: 2) }
  let(:organisation) { create(:organisation, user: user) }

  describe "as logged in user admin" do
    before do
      organisation
      whole_login_process(user)
      find_link("navToggle").click
      find_link("treeViewAdmin").click
      click_link 'Organisations'
      click_link 'Location'
    end

    it "should validate address" do
      click_link 'new location'
      click_on 'Save'

      message = find("#location_address").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")

      click_back

      click_link 'Edit'

      fill_in 'Address', with: ''
      click_on 'Save'

      message = find("#location_address").native.attribute("validationMessage")

      expect(message).to eq("Please fill out this field.")

      click_back

      expect(current_path).to eq(admin_organisation_locations_path(organisation))
    end

    it "should auto complete address and other by typing address on google maps field" do
      click_link 'new location'
      wait_until_ready

      find(:xpath, "//input[@name='pac-input']").set('Wren Street, Albert Town 9382')
      page.should have_css('.pac-item')
      find('.pac-item', match: :first).click
      click_on 'Save'
      expect(page).to have_css('tbody tr', count: 2)
    end

    # it "can edit location" do
    #   click_link 'Edit'
    #   wait_until_ready

    #   find(:xpath, "//input[@name='pac-input']").set('Wren Street, Albert Town 9382')
    #   page.should have_css('.pac-item')
    #   find('.pac-item', match: :first).click
    #   click_on 'Save'
    #   expect(page).to have_content('Wren Street, Albert Town 9382')
    # end

    it "can destroy location" do
      accept_confirm do
        click_link 'Delete'
      end

      expect(page).to have_css('tbody tr', count: 0)
    end
  end

  describe "as guest user" do
    it "can't access locations [new edit index]" do
      visit admin_organisation_locations_path(organisation)
      expect(current_path).to eq(new_user_session_path)

      visit edit_admin_organisation_location_path(organisation, organisation.locations.first)
      expect(current_path).to eq(new_user_session_path)

      visit new_admin_organisation_location_path(organisation)
      expect(current_path).to eq(new_user_session_path)
    end
  end
end
