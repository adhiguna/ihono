# create user admin
User.create(
  email: 'adhiguna.sabril@gmail.com', 
  first_name: 'adhiguna', 
  last_name: 'sabril', 
  username: 'adhiguna', 
  password: 'password', 
  role: 2
  )

User.create(
  email: 'user_1@user.com', 
  first_name: 'user_1', 
  last_name: 'user', 
  username: 'user_1', 
  password: 'password',
  )

[
  'Te Tai Tokerau', 
  'Tāmaki', 'Hauraki', 
  'Tainui', 'Tauranga Moana', 
  'Te Arawa Waka', 'Mātaatua', 
  'Te Tai Rāwhiti', 'Tākitimu', 
  'Hauāuru', 'Te Moana O Raukawa', 
  'Te Tau Ihu', 'Waipounamu and Rekohu', 'Wharekauri'].each do |region|
    Region.create(name: region)
end