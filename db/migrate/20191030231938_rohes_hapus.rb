class RohesHapus < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes_hapus, id: false do |t|
      t.belongs_to :rohe, index: true
      t.belongs_to :hapu, index: true
    end

    add_index :rohes_hapus, [:rohe_id, :hapu_id], :unique => true
  end
end
