class CreateOrganisations < ActiveRecord::Migration[5.2]
  def change
    create_table :organisations do |t|
      t.references :user
      t.string :name, null: false
      t.string :nzbn
      t.string :organisation_type, null: false
      t.text :abstract
      t.string :slug, uniq: true, null: false
      t.text :hero_image_data
      t.string :hero_image_filename
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
