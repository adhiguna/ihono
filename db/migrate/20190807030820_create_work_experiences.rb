class CreateWorkExperiences < ActiveRecord::Migration[5.2]
  def change
    create_table :work_experiences do |t|
      t.string :title, null: false
      t.string :company, null: false
      t.string :location
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.text :description

      t.timestamps
    end
  end
end
