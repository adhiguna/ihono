class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :sender_notification
      t.belongs_to :notifiable, polymorphic: true
      t.string :notifiable_status, null: false
      t.boolean :is_show, default: true, null: false

      t.timestamps
    end
  end
end
