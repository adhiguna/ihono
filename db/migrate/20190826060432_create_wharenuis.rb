class CreateWharenuis < ActiveRecord::Migration[5.2]
  def change
    create_table :wharenuis do |t|
      t.string :name, null: false, uniq: true
      t.string :contact
      t.string :abstract
      t.string :slug, uniq: true, null: false
    end
  end
end
