class CreateSenderNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :sender_notifications do |t|
      t.references :user
      t.boolean :is_show, default: true, null: false

      t.timestamps
    end
  end
end
