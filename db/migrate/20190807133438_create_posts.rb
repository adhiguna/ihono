class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.references :user
      t.string :title, null: false
      t.text :content, null: false
      t.string :post_type, null: false
      t.string :slug, uniq: true, null: false
      t.text :hero_image_data
      t.string :hero_image_filename
      
      t.timestamps
    end
  end
end
