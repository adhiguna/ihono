class CreatePages < ActiveRecord::Migration[5.2]
  def up
    create_table :pages do |t|
      t.string :title, nil: false
      t.string :page_type, uniq: true, nil: false
      t.text :description
      t.string :slug, uniq: true, null: false
      t.text :hero_image_data
      t.string :hero_image_filename

      t.timestamps
    end

    Page::PAGE_TYPES.each do |page_type|
      Page.create(page_type: page_type, title: page_type)
    end
  end

  def down
    drop_table :pages
  end
end
