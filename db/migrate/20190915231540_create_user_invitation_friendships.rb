class CreateUserInvitationFriendships < ActiveRecord::Migration[5.2]
  def change
    create_table :user_invitation_friendships do |t|
      t.references :user, index: true
      t.references :user_friend, foreign_key: { to_table: :users }, index: true
      t.references :request_by, foreign_key: { to_table: :users }, index: true
      t.string :invitation_status, default: 'pending', null: false

      t.timestamps
    end

    add_index :user_invitation_friendships, [:user_id, :user_friend_id], :unique => true
  end
end
