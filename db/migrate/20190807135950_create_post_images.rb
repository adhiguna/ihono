class CreatePostImages < ActiveRecord::Migration[5.2]
  def change
    create_table :post_images do |t|
      t.text :image_data
      t.string :image_filename

      t.timestamps
    end
  end
end
