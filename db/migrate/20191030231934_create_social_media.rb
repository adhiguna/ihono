class CreateSocialMedia < ActiveRecord::Migration[5.2]
  def change
    create_table :social_media do |t|
      t.references :mediable, polymorphic: true
      t.string :social_media_type, null: false
      t.string :name, null: false
      t.string :link, null: false

      t.timestamps
    end
  end
end
