class RegionsOrganisations < ActiveRecord::Migration[5.2]
  def change
    create_table :regions_organisations, id: false do |t|
      t.belongs_to :region, index: true
      t.belongs_to :organisation, index: true
    end

    add_index :regions_organisations, [:region_id, :organisation_id], :unique => true
  end
end
