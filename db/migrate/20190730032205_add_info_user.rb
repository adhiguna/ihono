class AddInfoUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :role, :integer, null: false, default: 0
    add_column :users, :username, :string, unique: true
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :abstract, :text
    add_column :users, :competency, :string
    add_column :users, :latitude, :float
    add_column :users, :longitude, :float
    add_column :users, :address, :text
    add_column :users, :city, :string
    add_column :users, :suburb, :string
    add_column :users, :postcode, :string
    add_column :users, :contact, :text
    add_column :users, :phone, :string
    add_column :users, :interest, :text
    add_column :users, :work, :text
    add_column :users, :visibility, :boolean, default: true, null: false
    add_column :users, :receiver_notifications_count, :integer, null: false, default: 0 
    add_column :users, :visible_latitude, :float
    add_column :users, :visible_longitude, :float
    add_column :users, :discarded_at, :datetime

    add_index :users, :discarded_at
  end 
end
