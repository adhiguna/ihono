class CreateSmsReceipts < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_receipts do |t|
      t.string :msisdn
      t.string :to
      t.string :network_code
      t.string :message_id, null: false
      t.string :price
      t.string :status
      t.string :scts
      t.string :error_code
      t.string :api_key
      t.string :message_timestamp
    end
  end
end
