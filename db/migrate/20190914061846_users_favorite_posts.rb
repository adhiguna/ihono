class UsersFavoritePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :users_favorite_posts, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :post, index: true
    end

    add_index :users_favorite_posts, [:user_id, :post_id], :unique => true
  end
end
