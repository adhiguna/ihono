class CreateUserInvitationSignups < ActiveRecord::Migration[5.2]
  def change
    create_table :user_invitation_signups do |t|
      t.references :user, index: true
      t.references :user_invited, foreign_key: { to_table: :users }, index: true, unique: true
      t.string :email
      t.string :phone
      t.string :name
      t.string :token

      t.timestamps
    end
  end
end
