class UsersFriendships < ActiveRecord::Migration[5.2]
  def change
    create_table :users_friendships, id: false do |t|
      t.references :user, index: true
      t.references :user_friend, foreign_key: { to_table: :users }, index: true
    end

    add_index :users_friendships, [:user_id, :user_friend_id], :unique => true
    add_index :users_friendships, [:user_friend_id, :user_id], :unique => true
  end
end
