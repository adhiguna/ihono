class RohesMaraes < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes_maraes, id: false do |t|
      t.belongs_to :rohe, index: true
      t.belongs_to :marae, index: true
    end

    add_index :rohes_maraes, [:rohe_id, :marae_id], :unique => true
  end
end
