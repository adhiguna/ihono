class IwisOrganisations < ActiveRecord::Migration[5.2]
  def change
    create_table :iwis_organisations, id: false do |t|
      t.belongs_to :iwi, index: true
      t.belongs_to :organisation, index: true
    end

    add_index :iwis_organisations, [:iwi_id, :organisation_id], :unique => true
  end
end
