class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.string :name, null: false
      t.text :address
      t.string :email
      t.string :home_phone
      t.string :cell_phone
      t.string :feels, array: true, null: false, default: []
      t.string :pre_exist_health_condition, array: true, null: false, default: []
      t.string :age
      t.date :appointment_date, null: false
      t.time :appointment_time, null: false
      t.references :health_provider, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :contact_with_corona, default: false, null: false
      t.boolean :travel_overseas, default: false, null: false
      t.boolean :contact_with_overseas_traveller, default: false, null: false
      t.boolean :health_care_worker, default: false, null: false
      t.boolean :essential_services_worker, default: false, null: false

      t.timestamps
    end
  end
end
