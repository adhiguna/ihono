class RohesWharenuis < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes_wharenuis, id: false do |t|
      t.belongs_to :rohe, index: true
      t.belongs_to :wharenui, index: true
    end

    add_index :rohes_wharenuis, [:rohe_id, :wharenui_id], :unique => true
  end
end
