class HapusMaraes < ActiveRecord::Migration[5.2]
  def change
    create_table :hapus_maraes, id: false do |t|
      t.belongs_to :hapu, index: true
      t.belongs_to :marae, index: true
    end

    add_index :hapus_maraes, [:hapu_id, :marae_id], :unique => true
  end
end
