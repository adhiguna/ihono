class RohesOrganisations < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes_organisations, id: false do |t|
      t.belongs_to :rohe, index: true
      t.belongs_to :organisation, index: true
    end

    add_index :rohes_organisations, [:rohe_id, :organisation_id], :unique => true
  end
end
