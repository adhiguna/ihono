class CreateHealthProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :health_providers do |t|
      t.string :name, null: false
      t.string :postcode
      t.string :slug
      t.string :suburb
      t.string :region
      t.text :service
      t.text :address
      t.text :contact
      t.string :country
      t.float :latitude
      t.float :longitude
      t.text :abstract
      t.time :open
      t.time :close
      t.boolean :allow_appointment, default: true, null: false

      t.timestamps
    end
  end
end
