class IwisWharenuis < ActiveRecord::Migration[5.2]
  def change
    create_table :iwis_wharenuis, id: false do |t|
      t.belongs_to :iwi, index: true
      t.belongs_to :wharenui, index: true
    end

    add_index :iwis_wharenuis, [:iwi_id, :wharenui_id], :unique => true
  end
end
