class MaraesWharenuis < ActiveRecord::Migration[5.2]
  def change
    create_table :maraes_wharenuis, id: false do |t|
      t.belongs_to :marae, index: true
      t.belongs_to :wharenui, index: true
    end

    add_index :maraes_wharenuis, [:marae_id, :wharenui_id], :unique => true
  end
end
