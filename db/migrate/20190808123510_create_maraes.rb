class CreateMaraes < ActiveRecord::Migration[5.2]
  def change
    create_table :maraes do |t|
      t.string :name, null: false, uniq: true
      t.string :suburb
      t.string :postcode
      t.string :country
      t.float :latitude
      t.float :longitude
      t.text :contact
      t.text :abstract
      t.text :address
      t.string :slug, uniq: true, null: false
    end
  end
end
