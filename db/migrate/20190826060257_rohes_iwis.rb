class RohesIwis < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes_iwis, id: false do |t|
      t.belongs_to :rohe, index: true
      t.belongs_to :iwi, index: true
    end

    add_index :rohes_iwis, [:rohe_id, :iwi_id], :unique => true
  end
end
