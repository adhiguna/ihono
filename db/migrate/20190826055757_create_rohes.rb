class CreateRohes < ActiveRecord::Migration[5.2]
  def change
    create_table :rohes do |t|
      t.string :name, null: false, uniq: true
      t.text :contact
      t.text :abstract
      t.string :slug, uniq: true, null: false
    end
  end
end
