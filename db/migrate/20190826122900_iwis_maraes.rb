class IwisMaraes < ActiveRecord::Migration[5.2]
  def change
    create_table :iwis_maraes, id: false do |t|
      t.belongs_to :iwi, index: true
      t.belongs_to :marae, index: true
    end

    add_index :iwis_maraes, [:iwi_id, :marae_id], :unique => true
  end
end
