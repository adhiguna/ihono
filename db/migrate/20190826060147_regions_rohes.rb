class RegionsRohes < ActiveRecord::Migration[5.2]
  def change
    create_table :regions_rohes, id: false do |t|
      t.belongs_to :region, index: true
      t.belongs_to :rohe, index: true
    end

    add_index :regions_rohes, [:region_id, :rohe_id], :unique => true
  end
end
