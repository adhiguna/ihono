class AddIndexToMaraeAndOrganisation < ActiveRecord::Migration[5.2]
  def up
    add_earthdistance_index :maraes, { lat: 'latitude', lng: 'longitude' }
    add_earthdistance_index :organisations, { lat: 'latitude', lng: 'longitude' }
  end

  def down
    remove_earthdistance_index :places
    remove_earthdistance_index :organisations
  end
end
