class CreateLineages < ActiveRecord::Migration[5.2]
  def change
    create_table :lineages do |t|
      t.references :user, index: true
      t.references :iwi
      t.references :hapu
      t.string :whanau
      
      t.timestamps
    end
  end
end
