class CreateReceiverNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :receiver_notifications do |t|
      t.references :user
      t.references :notification
      t.boolean :is_show, default: true, null: false
      t.boolean :is_read, default: false, null: false

      t.timestamps
    end
  end
end
