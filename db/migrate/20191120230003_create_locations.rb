class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.belongs_to :locationable, polymorphic: true
      t.text :address, null: false
      t.text :contact
      t.float :latitude
      t.float :longitude
      t.string :postcode
      t.string :suburb
      t.string :country

      t.timestamps
    end
  end
end
