class IwisHapus < ActiveRecord::Migration[5.2]
  def change
    create_table :iwis_hapus, id: false do |t|
      t.belongs_to :iwi, index: true
      t.belongs_to :hapu, index: true
    end

    add_index :iwis_hapus, [:iwi_id, :hapu_id], :unique => true
  end
end
