class HapusWharenuis < ActiveRecord::Migration[5.2]
  def change
    create_table :hapus_wharenuis, id: false do |t|
      t.belongs_to :hapu, index: true
      t.belongs_to :wharenui, index: true
    end

    add_index :hapus_wharenuis, [:hapu_id, :wharenui_id], :unique => true
  end
end
