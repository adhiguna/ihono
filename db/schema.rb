# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_02_043839) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "cube"
  enable_extension "earthdistance"
  enable_extension "pg_trgm"
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "appointments", force: :cascade do |t|
    t.string "name", null: false
    t.text "address"
    t.string "email"
    t.string "home_phone"
    t.string "cell_phone"
    t.string "feels", default: [], null: false, array: true
    t.string "pre_exist_health_condition", default: [], null: false, array: true
    t.string "age"
    t.date "appointment_date", null: false
    t.time "appointment_time", null: false
    t.bigint "health_provider_id"
    t.bigint "user_id"
    t.boolean "contact_with_corona", default: false, null: false
    t.boolean "travel_overseas", default: false, null: false
    t.boolean "contact_with_overseas_traveller", default: false, null: false
    t.boolean "health_care_worker", default: false, null: false
    t.boolean "essential_services_worker", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["health_provider_id"], name: "index_appointments_on_health_provider_id"
    t.index ["user_id"], name: "index_appointments_on_user_id"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "hapus", force: :cascade do |t|
    t.string "name", null: false
    t.text "contact"
    t.text "abstract"
    t.string "slug", null: false
  end

  create_table "hapus_maraes", id: false, force: :cascade do |t|
    t.bigint "hapu_id"
    t.bigint "marae_id"
    t.index ["hapu_id", "marae_id"], name: "index_hapus_maraes_on_hapu_id_and_marae_id", unique: true
    t.index ["hapu_id"], name: "index_hapus_maraes_on_hapu_id"
    t.index ["marae_id"], name: "index_hapus_maraes_on_marae_id"
  end

  create_table "hapus_wharenuis", id: false, force: :cascade do |t|
    t.bigint "hapu_id"
    t.bigint "wharenui_id"
    t.index ["hapu_id", "wharenui_id"], name: "index_hapus_wharenuis_on_hapu_id_and_wharenui_id", unique: true
    t.index ["hapu_id"], name: "index_hapus_wharenuis_on_hapu_id"
    t.index ["wharenui_id"], name: "index_hapus_wharenuis_on_wharenui_id"
  end

  create_table "health_providers", force: :cascade do |t|
    t.string "name", null: false
    t.string "postcode"
    t.string "slug"
    t.string "suburb"
    t.string "region"
    t.text "service"
    t.text "address"
    t.text "contact"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.text "abstract"
    t.time "open"
    t.time "close"
    t.boolean "allow_appointment", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "iwis", force: :cascade do |t|
    t.string "name", null: false
    t.text "contact"
    t.text "abstract"
    t.string "slug", null: false
  end

  create_table "iwis_hapus", id: false, force: :cascade do |t|
    t.bigint "iwi_id"
    t.bigint "hapu_id"
    t.index ["hapu_id"], name: "index_iwis_hapus_on_hapu_id"
    t.index ["iwi_id", "hapu_id"], name: "index_iwis_hapus_on_iwi_id_and_hapu_id", unique: true
    t.index ["iwi_id"], name: "index_iwis_hapus_on_iwi_id"
  end

  create_table "iwis_maraes", id: false, force: :cascade do |t|
    t.bigint "iwi_id"
    t.bigint "marae_id"
    t.index ["iwi_id", "marae_id"], name: "index_iwis_maraes_on_iwi_id_and_marae_id", unique: true
    t.index ["iwi_id"], name: "index_iwis_maraes_on_iwi_id"
    t.index ["marae_id"], name: "index_iwis_maraes_on_marae_id"
  end

  create_table "iwis_organisations", id: false, force: :cascade do |t|
    t.bigint "iwi_id"
    t.bigint "organisation_id"
    t.index ["iwi_id", "organisation_id"], name: "index_iwis_organisations_on_iwi_id_and_organisation_id", unique: true
    t.index ["iwi_id"], name: "index_iwis_organisations_on_iwi_id"
    t.index ["organisation_id"], name: "index_iwis_organisations_on_organisation_id"
  end

  create_table "iwis_wharenuis", id: false, force: :cascade do |t|
    t.bigint "iwi_id"
    t.bigint "wharenui_id"
    t.index ["iwi_id", "wharenui_id"], name: "index_iwis_wharenuis_on_iwi_id_and_wharenui_id", unique: true
    t.index ["iwi_id"], name: "index_iwis_wharenuis_on_iwi_id"
    t.index ["wharenui_id"], name: "index_iwis_wharenuis_on_wharenui_id"
  end

  create_table "lineages", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "iwi_id"
    t.bigint "hapu_id"
    t.string "whanau"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hapu_id"], name: "index_lineages_on_hapu_id"
    t.index ["iwi_id"], name: "index_lineages_on_iwi_id"
    t.index ["user_id"], name: "index_lineages_on_user_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "locationable_type"
    t.bigint "locationable_id"
    t.text "address", null: false
    t.text "contact"
    t.float "latitude"
    t.float "longitude"
    t.string "postcode"
    t.string "suburb"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locationable_type", "locationable_id"], name: "index_locations_on_locationable_type_and_locationable_id"
  end

  create_table "maraes", force: :cascade do |t|
    t.string "name", null: false
    t.string "suburb"
    t.string "postcode"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.text "contact"
    t.text "abstract"
    t.text "address"
    t.string "slug", null: false
    t.index "ll_to_earth(latitude, longitude)", name: "maraes_earthdistance_ix", using: :gist
  end

  create_table "maraes_wharenuis", id: false, force: :cascade do |t|
    t.bigint "marae_id"
    t.bigint "wharenui_id"
    t.index ["marae_id", "wharenui_id"], name: "index_maraes_wharenuis_on_marae_id_and_wharenui_id", unique: true
    t.index ["marae_id"], name: "index_maraes_wharenuis_on_marae_id"
    t.index ["wharenui_id"], name: "index_maraes_wharenuis_on_wharenui_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "sender_notification_id"
    t.string "notifiable_type"
    t.bigint "notifiable_id"
    t.string "notifiable_status", null: false
    t.boolean "is_show", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id"
    t.index ["sender_notification_id"], name: "index_notifications_on_sender_notification_id"
  end

  create_table "organisations", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name", null: false
    t.string "nzbn"
    t.string "organisation_type", null: false
    t.text "abstract"
    t.string "slug", null: false
    t.text "hero_image_data"
    t.string "hero_image_filename"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index "ll_to_earth(latitude, longitude)", name: "organisations_earthdistance_ix", using: :gist
    t.index ["user_id"], name: "index_organisations_on_user_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.string "page_type"
    t.text "description"
    t.string "slug", null: false
    t.text "hero_image_data"
    t.string "hero_image_filename"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text "content"
    t.string "searchable_type"
    t.bigint "searchable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id"
  end

  create_table "post_images", force: :cascade do |t|
    t.text "image_data"
    t.string "image_filename"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title", null: false
    t.text "content", null: false
    t.string "post_type", null: false
    t.string "slug", null: false
    t.text "hero_image_data"
    t.string "hero_image_filename"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "region_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "receiver_notifications", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "notification_id"
    t.boolean "is_show", default: true, null: false
    t.boolean "is_read", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["notification_id"], name: "index_receiver_notifications_on_notification_id"
    t.index ["user_id"], name: "index_receiver_notifications_on_user_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name", null: false
    t.text "contact"
    t.text "abstract"
    t.string "slug", null: false
  end

  create_table "regions_organisations", id: false, force: :cascade do |t|
    t.bigint "region_id"
    t.bigint "organisation_id"
    t.index ["organisation_id"], name: "index_regions_organisations_on_organisation_id"
    t.index ["region_id", "organisation_id"], name: "index_regions_organisations_on_region_id_and_organisation_id", unique: true
    t.index ["region_id"], name: "index_regions_organisations_on_region_id"
  end

  create_table "regions_rohes", id: false, force: :cascade do |t|
    t.bigint "region_id"
    t.bigint "rohe_id"
    t.index ["region_id", "rohe_id"], name: "index_regions_rohes_on_region_id_and_rohe_id", unique: true
    t.index ["region_id"], name: "index_regions_rohes_on_region_id"
    t.index ["rohe_id"], name: "index_regions_rohes_on_rohe_id"
  end

  create_table "rohes", force: :cascade do |t|
    t.string "name", null: false
    t.text "contact"
    t.text "abstract"
    t.string "slug", null: false
  end

  create_table "rohes_hapus", id: false, force: :cascade do |t|
    t.bigint "rohe_id"
    t.bigint "hapu_id"
    t.index ["hapu_id"], name: "index_rohes_hapus_on_hapu_id"
    t.index ["rohe_id", "hapu_id"], name: "index_rohes_hapus_on_rohe_id_and_hapu_id", unique: true
    t.index ["rohe_id"], name: "index_rohes_hapus_on_rohe_id"
  end

  create_table "rohes_iwis", id: false, force: :cascade do |t|
    t.bigint "rohe_id"
    t.bigint "iwi_id"
    t.index ["iwi_id"], name: "index_rohes_iwis_on_iwi_id"
    t.index ["rohe_id", "iwi_id"], name: "index_rohes_iwis_on_rohe_id_and_iwi_id", unique: true
    t.index ["rohe_id"], name: "index_rohes_iwis_on_rohe_id"
  end

  create_table "rohes_maraes", id: false, force: :cascade do |t|
    t.bigint "rohe_id"
    t.bigint "marae_id"
    t.index ["marae_id"], name: "index_rohes_maraes_on_marae_id"
    t.index ["rohe_id", "marae_id"], name: "index_rohes_maraes_on_rohe_id_and_marae_id", unique: true
    t.index ["rohe_id"], name: "index_rohes_maraes_on_rohe_id"
  end

  create_table "rohes_organisations", id: false, force: :cascade do |t|
    t.bigint "rohe_id"
    t.bigint "organisation_id"
    t.index ["organisation_id"], name: "index_rohes_organisations_on_organisation_id"
    t.index ["rohe_id", "organisation_id"], name: "index_rohes_organisations_on_rohe_id_and_organisation_id", unique: true
    t.index ["rohe_id"], name: "index_rohes_organisations_on_rohe_id"
  end

  create_table "rohes_wharenuis", id: false, force: :cascade do |t|
    t.bigint "rohe_id"
    t.bigint "wharenui_id"
    t.index ["rohe_id", "wharenui_id"], name: "index_rohes_wharenuis_on_rohe_id_and_wharenui_id", unique: true
    t.index ["rohe_id"], name: "index_rohes_wharenuis_on_rohe_id"
    t.index ["wharenui_id"], name: "index_rohes_wharenuis_on_wharenui_id"
  end

  create_table "sender_notifications", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "is_show", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sender_notifications_on_user_id"
  end

  create_table "sms_receipts", force: :cascade do |t|
    t.string "msisdn"
    t.string "to"
    t.string "network_code"
    t.string "message_id", null: false
    t.string "price"
    t.string "status"
    t.string "scts"
    t.string "error_code"
    t.string "api_key"
    t.string "message_timestamp"
  end

  create_table "social_media", force: :cascade do |t|
    t.string "mediable_type"
    t.bigint "mediable_id"
    t.string "social_media_type", null: false
    t.string "name", null: false
    t.string "link", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mediable_type", "mediable_id"], name: "index_social_media_on_mediable_type_and_mediable_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.bigint "tag_id"
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.string "tagger_type"
    t.bigint "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type", "taggable_id"], name: "index_taggings_on_taggable_type_and_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
    t.index ["tagger_type", "tagger_id"], name: "index_taggings_on_tagger_type_and_tagger_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "user_invitation_friendships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "user_friend_id"
    t.bigint "request_by_id"
    t.string "invitation_status", default: "pending", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["request_by_id"], name: "index_user_invitation_friendships_on_request_by_id"
    t.index ["user_friend_id"], name: "index_user_invitation_friendships_on_user_friend_id"
    t.index ["user_id", "user_friend_id"], name: "index_user_invitation_friendships_on_user_id_and_user_friend_id", unique: true
    t.index ["user_id"], name: "index_user_invitation_friendships_on_user_id"
  end

  create_table "user_invitation_signups", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "user_invited_id"
    t.string "email"
    t.string "phone"
    t.string "name"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_invitation_signups_on_user_id"
    t.index ["user_invited_id"], name: "index_user_invitation_signups_on_user_invited_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "slug", null: false
    t.text "avatar_data"
    t.string "avatar_filename"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "second_factor_attempts_count", default: 0
    t.string "encrypted_otp_secret_key"
    t.string "encrypted_otp_secret_key_iv"
    t.string "encrypted_otp_secret_key_salt"
    t.string "direct_otp"
    t.datetime "direct_otp_sent_at"
    t.datetime "totp_timestamp"
    t.integer "role", default: 0, null: false
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.text "abstract"
    t.string "competency"
    t.float "latitude"
    t.float "longitude"
    t.text "address"
    t.string "city"
    t.string "suburb"
    t.string "postcode"
    t.text "contact"
    t.string "phone"
    t.text "interest"
    t.text "work"
    t.boolean "visibility", default: true, null: false
    t.integer "receiver_notifications_count", default: 0, null: false
    t.float "visible_latitude"
    t.float "visible_longitude"
    t.datetime "discarded_at"
    t.index ["discarded_at"], name: "index_users_on_discarded_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["encrypted_otp_secret_key"], name: "index_users_on_encrypted_otp_secret_key", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  create_table "users_favorite_posts", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "post_id"
    t.index ["post_id"], name: "index_users_favorite_posts_on_post_id"
    t.index ["user_id", "post_id"], name: "index_users_favorite_posts_on_user_id_and_post_id", unique: true
    t.index ["user_id"], name: "index_users_favorite_posts_on_user_id"
  end

  create_table "users_friendships", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "user_friend_id"
    t.index ["user_friend_id", "user_id"], name: "index_users_friendships_on_user_friend_id_and_user_id", unique: true
    t.index ["user_friend_id"], name: "index_users_friendships_on_user_friend_id"
    t.index ["user_id", "user_friend_id"], name: "index_users_friendships_on_user_id_and_user_friend_id", unique: true
    t.index ["user_id"], name: "index_users_friendships_on_user_id"
  end

  create_table "users_like_posts", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "post_id"
    t.index ["post_id"], name: "index_users_like_posts_on_post_id"
    t.index ["user_id", "post_id"], name: "index_users_like_posts_on_user_id_and_post_id", unique: true
    t.index ["user_id"], name: "index_users_like_posts_on_user_id"
  end

  create_table "wharenuis", force: :cascade do |t|
    t.string "name", null: false
    t.string "contact"
    t.string "abstract"
    t.string "slug", null: false
  end

  create_table "work_experiences", force: :cascade do |t|
    t.string "title", null: false
    t.string "company", null: false
    t.string "location"
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "appointments", "health_providers"
  add_foreign_key "appointments", "users"
  add_foreign_key "taggings", "tags"
  add_foreign_key "user_invitation_friendships", "users", column: "request_by_id"
  add_foreign_key "user_invitation_friendships", "users", column: "user_friend_id"
  add_foreign_key "user_invitation_signups", "users", column: "user_invited_id"
  add_foreign_key "users_friendships", "users", column: "user_friend_id"
end
