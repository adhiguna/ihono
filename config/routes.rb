require 'sidekiq/web'

Rails.application.routes.draw do
  authenticate :user, lambda { |u| u.super_admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end
  
  # shrine direct uploader https://github.com/shrinerb/shrine/wiki/Adding-Direct-S3-Uploads
  mount ImageCacheUploader.upload_endpoint(:store) => '/image_cache_uploader/upload'

  get '/service-worker.js', to: 'service_workers/workers#index'
  get '/manifest.json', to: 'service_workers/manifests#index'
  root 'pages#index'

  # Webhook URL
  resources :webhooks, except: :all do
    collection do
      post 'sms-receipt', to: 'webhooks#sms_receipt'
    end
  end

  resources :search, only: [:index] do
    collection do
      get 'view_results/:group/:keyword', as: :view_results, to: 'search#view_results'
    end
  end
  resources :notifications, only: [:index]
  resources :organisations, only: [:show]
  resources :posts do
    collection do
      get :my_posts
      get :favorites
    end

    member do
      put :toggle_favorite
      put :toggle_like
    end
  end
  resources :regions, path: 'region', only: [:show]
  resources :rohes, path: 'rohe', only: [:show]
  resources :iwis, path: 'iwi', only: [:show] do
    member do
      get :get_hapu
    end
  end
  resources :hapus, path: 'hapu', only: [:show]
  resources :maraes, path: 'marae', only: [:show]
  resources :wharenuis, path: 'wharenui', only: [:show]
  resources :maps, only: [:index]
  
  devise_for :users, controllers: {
    two_factor_authentication: 'users/two_factor_authentication',
    sessions: 'users/sessions'
  }, skip: %[registrations]
  devise_scope :user do
    resource :registration,
      only: %w[new create],
      path: 'users',
      controller: 'users/registrations'
  end

  namespace :users do
    resources :setup_account
    resources :user_invitation_friendships, path: 'friend-invitations', only: [:index] do
      collection do
        post '/:user_friend_id', to: 'user_invitation_friendships#create', as: :create
        delete '/:user_friend_id', to: 'user_invitation_friendships#destroy', as: :destroy
        delete ':user_friend_id/unfriend', to: 'user_invitation_friendships#unfriend', as: :unfriend
        put ':user_friend_id/:status', to: 'user_invitation_friendships#update', as: :update
      end
    end
    resources :profile, only: [:index] do
      collection do
        get :edit
        patch :update
        patch :update_location
      end
    end
  end

  namespace :admin do
    resources :health_providers, except: [:show]
    resources :appointments
    resources :organisations, except: [:show] do
      resources :locations, except: [:show]
    end
    resources :pages, only: [:index, :edit, :update]
    resources :users, except: [:show]
    resources :regions, path: 'region', except: [:show]
    resources :rohes, path: 'rohe', except: [:show]
    resources :wharenuis, path: 'wharenui', except: [:show]
    resources :iwis, path: 'iwi', except: [:show] do
      member do
        get ':id/show' => 'iwis#show'
      end
    end
    resources :hapus, path: 'hapu', except: [:show] do
      member do
        get ':id/show' => 'hapus#show'
      end
    end

    resources :maraes, path: 'marae', except: [:show] do
      member do
        get ':id/show' => 'marae#show'
      end
    end
  end

  namespace :health_admin, path: 'health-admin' do
    resources :health_providers, except: [:show]
    resources :appointments
  end

  namespace :corona, path: 'covid-19' do
    root :to => 'pages#index'

    get 'health-provider-maps' => 'health_providers#index'
    get 'health-provider-list' => 'health_providers#index_list'

    resources :posts, only: [:index, :show]

    get 'medical-checkup', to: 'pages#medical_checkup'
    resources :health_providers, path: 'health-providers', only: [:show] do
      get 'appointment', to: 'appointments#new'
      post 'appointment', to: 'appointments#create'
      get 'appointment-success', to: 'appointments#success'
    end

    resources :appointments, only: [:show, :index]

    get 'health-status', to: 'pages#health_status'

    # invitation signup
    get 'health-status/invitation-signup', to: 'user_invitation_signup#index'
    post 'health-status/invitation-signup', to: 'user_invitation_signup#create'
  end

  resources :users, only: [:show]

  get ':page', as: :page_show, to: 'pages#show'
end
