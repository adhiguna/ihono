FEELS_LIST = [
              'cough', 'sore throat', 'shortness of breath', 
              'head cold', 'loss of smell', 'fever', 'diarrhoea', 
              'muscle pain', 'headache', 'nausea', 'confusion/irritability'
             ].freeze.sort

HEALTH_CONDITION_LIST = [
                          'compromised immune system', 'liver disease', 'cancer', 
                          'kidney disease', 'heart disease', 'diabetes', 
                          'chronic lung disease', 'asthma', 'pregnancy', 'none'
                        ].freeze

AGE_LIST =  [
              'Under than 5 years', '5-10 years', '11-20 years', '21-30 years', 
              '31-40 years', '41-50 years', '51-60 years', '61-70 years', '71-80 years', 
              'more than 80 years'
            ].freeze
                        
QUARANTINE_LIST = ['No', 'Yes - self quarantine', 'Yes - under medical supervision'].freeze