require 'shrine'
require "shrine/storage/s3"
require "shrine/storage/file_system"

if Rails.env.test?
  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads"),
  }
else
  s3_options = {
    access_key_id:     ENV['AWS_ACCESS_KEY_ID'],
    secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    bucket:            ENV['AWS_BUCKET_NAME'],
    region:            ENV['AWS_REGION_NAME'],
  }

  Shrine.storages = {
    cache: Shrine::Storage::S3.new(public: true, prefix: "cache", **s3_options),
    store: Shrine::Storage::S3.new(
      public: true,
      host: "https://#{ENV['AWS_BUCKET_NAME']}",
      prefix: 'store', **s3_options,
      upload_options: {
        cache_control: 'public, max-age=2628000',
        expires: Time.now + 30.year
      }
    ),
  }
end

Shrine.plugin :activerecord
Shrine.plugin :backgrounding

Shrine::Attacher.promote { |data| ShrineUploadWorker.perform_async(data) }
Shrine::Attacher.delete { |data| ShrineDeleteWorker.perform_async(data) }
