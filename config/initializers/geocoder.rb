if ENV['GOOGLE_MAP_GEOCODER_API_KEY']
  configure = {
    :lookup     => :google,
    :api_key    => ENV['GOOGLE_MAP_GEOCODER_API_KEY']
  }

  Geocoder.configure(configure)
end