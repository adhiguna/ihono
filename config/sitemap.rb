# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'https://ihono.maori.nz/'
SitemapGenerator::Sitemap.sitemaps_path = 'shared/'
SitemapGenerator::Sitemap.compress = false

SitemapGenerator::Sitemap.create do
  add '/about', priority: 0.1, changefreq: 'daily'
  add '/terms-and-conditions', priority: 0.1, changefreq: 'daily'
  add '/privacy', priority: 0.1, changefreq: 'daily'
end
