require "application_system_test_case"

class HealthProvidersTest < ApplicationSystemTestCase
  setup do
    @health_provider = health_providers(:one)
  end

  test "visiting the index" do
    visit health_providers_url
    assert_selector "h1", text: "Health Providers"
  end

  test "creating a Health provider" do
    visit health_providers_url
    click_on "New Health Provider"

    fill_in "Abstract", with: @health_provider.abstract
    fill_in "Address", with: @health_provider.address
    fill_in "Contact", with: @health_provider.contact
    fill_in "Country", with: @health_provider.country
    fill_in "Latitude", with: @health_provider.latitude
    fill_in "Longitude", with: @health_provider.longitude
    fill_in "Name", with: @health_provider.name
    fill_in "Postcode", with: @health_provider.postcode
    fill_in "Slug", with: @health_provider.slug
    fill_in "Suburb", with: @health_provider.suburb
    click_on "Create Health provider"

    assert_text "Health provider was successfully created"
    click_on "Back"
  end

  test "updating a Health provider" do
    visit health_providers_url
    click_on "Edit", match: :first

    fill_in "Abstract", with: @health_provider.abstract
    fill_in "Address", with: @health_provider.address
    fill_in "Contact", with: @health_provider.contact
    fill_in "Country", with: @health_provider.country
    fill_in "Latitude", with: @health_provider.latitude
    fill_in "Longitude", with: @health_provider.longitude
    fill_in "Name", with: @health_provider.name
    fill_in "Postcode", with: @health_provider.postcode
    fill_in "Slug", with: @health_provider.slug
    fill_in "Suburb", with: @health_provider.suburb
    click_on "Update Health provider"

    assert_text "Health provider was successfully updated"
    click_on "Back"
  end

  test "destroying a Health provider" do
    visit health_providers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Health provider was successfully destroyed"
  end
end
