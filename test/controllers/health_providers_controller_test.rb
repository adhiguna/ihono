require 'test_helper'

class HealthProvidersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @health_provider = health_providers(:one)
  end

  test "should get index" do
    get health_providers_url
    assert_response :success
  end

  test "should get new" do
    get new_health_provider_url
    assert_response :success
  end

  test "should create health_provider" do
    assert_difference('HealthProvider.count') do
      post health_providers_url, params: { health_provider: { abstract: @health_provider.abstract, address: @health_provider.address, contact: @health_provider.contact, country: @health_provider.country, latitude: @health_provider.latitude, longitude: @health_provider.longitude, name: @health_provider.name, postcode: @health_provider.postcode, slug: @health_provider.slug, suburb: @health_provider.suburb } }
    end

    assert_redirected_to health_provider_url(HealthProvider.last)
  end

  test "should show health_provider" do
    get health_provider_url(@health_provider)
    assert_response :success
  end

  test "should get edit" do
    get edit_health_provider_url(@health_provider)
    assert_response :success
  end

  test "should update health_provider" do
    patch health_provider_url(@health_provider), params: { health_provider: { abstract: @health_provider.abstract, address: @health_provider.address, contact: @health_provider.contact, country: @health_provider.country, latitude: @health_provider.latitude, longitude: @health_provider.longitude, name: @health_provider.name, postcode: @health_provider.postcode, slug: @health_provider.slug, suburb: @health_provider.suburb } }
    assert_redirected_to health_provider_url(@health_provider)
  end

  test "should destroy health_provider" do
    assert_difference('HealthProvider.count', -1) do
      delete health_provider_url(@health_provider)
    end

    assert_redirected_to health_providers_url
  end
end
